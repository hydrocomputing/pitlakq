Command-line Options
--------------------

PITLAKQ provides command-line options.

Just typing `pitlakq` or `pitlakq -h` at the command line will bring up this
output::

    usage: pitlakq [-h] [-v] {init,run,create,introspect} ...

    positional arguments:
      {init,run,create,introspect}
        init                initialize PITLAKQ and create `.pitlakq`
        run                 start the calculation of a model
        create              creates a new project folder with empty sub folders
        introspect          find out internals of PITLAKQ or your projects

    optional arguments:
      -h, --help            show this help message and exit
      -v, --version         show version info and exit
  
The program is organized in subcommands such as ``init`` or ``run``.
For example, ``run``  takes the name of the project to be run as obligatory
argument::


    pitlakq run -h
    usage: run_pitlakq.py run [-h] project_name

    run a project

    positional arguments:
      project_name  name of project to be calculated

    
The subcommand ``introspect`` allows to explore internals::

    pitlakq introspect
    usage: run_pitlakq.py introspect [-h] [-w] [-p]

    find out internals

    optional arguments:
      -h, --help      show this help message and exit
      -w, --w2-names  show w2 names
      -p, --projects  show all projects

The option ``-w`` in short or ``w2-names`` in long form displays a long
for named used in CE-QUAL-W2 (see `CE-QUAL-W2 documentation`_) and
their corresponding names in PITLAKQ.

The option ``--projects``  lists all projects in the in model path.

.. _`CE-QUAL-W2 documentation`: http://www.ce.pdx.edu/w2/download/download/v20/manual/manualv20.zip