Making groundwater inflow a function of lake water level
--------------------------------------------------------

So far we have specified the groundwater inflow as a tributary inflow.
For this we needed to provide the inflow for each desired time step.
An alternative solution is to specify groundwater inflows as a function
of lake water level.

First, we create a new  project with  ``pitlakq create pitlakq_tut_gwh``.
We just copy the ``input`` folder from our former project
``pitlakq_tut_qual``. Introducing a new feature, it is always a good idea to
turn of water quality calculations for the first steps. This makes setting up
our new inflow simpler and test runs considerately faster.

To do this we just turn off the PHREEQC coupling. In ``pitlakq.yaml`` we set
this option for this to ``False``:

.. code-block:: yaml

  couplings:
      phreeqc_coupling_lake:
          value: False

Furthermore, for know we also turn of kinetic calculations in W2 setting
the corresponding option in ``pitlakq.yaml`` to ``False``:


.. code-block:: yaml

  lake:
      ...
      kinetics:
          value: False

Now we also turn off our groundwater inflow tributary. In ``w2.yaml`` we
set the number of tributaries to one.

.. code-block:: yaml

  bounds:
    ...
    number_of_tributaries:
        value: 1


And also change the tributary section to one tributary:


.. code-block:: yaml

  tributaries:
      number_of_tributaries:
          value: 1
      tributary_names:
          value:
              - mytribname
      tributary_segment:
          value:
              - 5
      tributary_inflow_placement:
          value:
              - 1 # density
      tributary_inflow_top_elevation:
          value:
              - 180
      tributary_inflow_bottom_elevation:
          value:
              - 155

Now we do a test run to see if the model still works:

``pitlakq run pitlakq_tut_gwh``

We turn on the option ``gwh``  in ``pitlakq.yaml``:

.. code-block:: yaml

  gw:
      gwh:
          value: True

The only thing missing now is the actual input. We create a template input
file with this script:


.. literalinclude:: ../tut/pitlakq_tut_gwh/preprocessing/do_gwh_xlsx.py
   :language: python

Running it:

``python do_gwh_xlsx.py``


creates a file ``gwh_template.xlsx`` in ``input/gwh``.
Rename this file to ``gwh.xlsx``. This renaming is a safety measure to avoid
accidental re-creation of the file and loosing the entered in input.

This file has three tabs. The first one represents the zones:

   .. image:: images/gwh_zones.png
       :scale: 50%

Only the white cells with the ``void`` inside are editable

   .. image:: images/gwh_zones_filled.png
       :scale: 50%

We added two zones. The color are optional and don't mean anything to PITLAKQ.
But the name is important. There is no limit on the number of zones as long as
there are at least two cells in two adjacent layers in one zone. The inflows
will be distributed over these cells according to there current water filled
volume.

Inflows will activate only if the water level specified in the column
``Level`` on the tab ``Flow`` will be reached by the lake water level. A soon
as the next level is reached, its inflow will be active and not the one below
anymore.
This way inflows can vary in many ways. Inflows can be negative which
results in groundwater outflow.

========= ===== ======
Zone name Level Flow
========= ===== ======
zone1     155   0.02
zone1     160   0.01
zone1     170   0.05
zone1     180   0.001
zone2     180   0.002
zone2     185   0.001
zone2     190   -0.0
========= ===== ======

Each zone has one concentration specified in the tab ``Conc``. We just use
the concentration from our groundwater tributary. This table shows the
first entries. Add all that from the original groundwater tributary file.

========= ================ ======== ===============
Zone name dissolved_oxygen ammonium nitrate_nitrite
========= ================ ======== ===============
zone1         8                   0         0.11
zone2         8                   0         0.11
========= ================ ======== ===============

Now run the model. Modify levels and flow values to see the effect on
the water level.

Finally, we can turn the water quality calculations back on.