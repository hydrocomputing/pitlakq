Create the main input file
--------------------------

Now we need to create input files. PITLAKQ has many hundreds of input
variables. Fortunately, for many applications sensible default will values can
be used. In the ``template`` directory (see your ``.pitlakq`` for its
location). You will find many templates of input files that contain default
values. The following input files contain only a minimal, site-specific data set.
Most of the input is taken from the defaults. Since we do only a very simple
model (hydrodynamics only, no inflow, no precipitation no evaporation) to show
the general principal how PITLAKQ works, this is approach is useful. For
real-world applications the amount of (different) input data is typically much
larger. We look at the many options that are involved when water quality
calculations that couple chemical and biological process or treatment, erosion or
sediment interactions come into play in later tutorials.

The input files are either in YAML_ or CSV (text in columns) format. Both
formats are easily readable by humans and computers. The YAML files use the
so called ``!File`` directive, which allows to include YAML into other YAML
by referencing them rather than copying their content. This means the same file
can be used from several other YAML files. Furthermore, the input can be spread
over as many or a as few input files as desired. This gives great flexibility how
to arrange the model best for each specific task.


This is a minimal PITLAKQ input file with information about the whole model.
For an overview of all available options see the file ``pitlakq.yaml`` in the
directory ``<install_dir>/pitlakq/templates``
(See :ref:`init-reference-label` for details about where this path is
located.):

.. literalinclude:: ../tut/pitlakq_tut/input/main/pitlakq.yaml
   :language: yaml

It is located in the directory where all your PITLAKQ projects are:
``<my_project_name>/input/main/pitlakq.yaml``. Change ``<my_project_name>``
to the name of your project.
This file has two groups ``general`` and ``lake``.
There are several more groups.
But since we can use the default values from ``<install_dir>\pitlakq\templates``
we don't need to repeat them here.
All values must be indented by four space, not tabs.
Best is to turn the option in your editor to display non-printing characters.
For example, with Notepad++ the menu ``View | Show Symbol | Show White Space and
TAB`` allows you to make them visible.
We always edit the attribute ``value`` there can be more attributes like
``unit`` but we don't use them here.

The value for ``start`` is ``01.01.1998``.
Note: Currently, dates always have to be in ``DD.MM.YYYY``-format. Other formats
would be possible but are not implemented yet. There is a second date for the
start called ``gw_start``. This is doe to historical reasons. Please use
``gw_start`` plus ``number_of_gw_time_steps`` to specify actual model run.
A ``gw_time_step`` is by default one calendar month long. So ``120`` month
will be exactly 10 years. Leap years are considered. The option ``dump_input``
make PITLAKQ to stop after reading all inputs to give you the possibility to
dump the input, which is combination of the defaults and the user settings
into one big YAML file. While this can take several seconds, it can be
interesting to see these data.

The group ``lake`` allows you to give more information about the lake model.
Here we only provide ``max_water_surface`` and ``deepest_segment``. The later
could be induced from the bathymetry data in ``bath.nc`` but for various
reasons it can be supplied here.