Pit Wall Loading
----------------

Needed information
++++++++++++++++++

The water quality of a pit lake can be significantly influenced by material
from the pit wall entering the lake. PITLAKQ offers a way to simulate these
loadings. There are several prerequisites that need to be satisfied for the
modeling approach to work:

1. You need to specify loading rates expressed in mass per area per time from
   the pit walls for all species that you would like to model.
   Such loading rates are typically measured in laboratory experiments.
   The rates need to be assigned to pit wall zones.
   There is no limit on the number of zones. There can be
   as few as one zone, covering all pit walls, or as many as dozens,
   hundreds, or thousands of zones if such detailed information is available.
2. You need to specify surface areas of the pit wall for each zone for
   different lake water levels. There is no limit on the number of lake water
   levels with different surface areas you can specify. However, there must be
   at least two levels. This surface areas will be used to calculate the
   effective loadings for each zone for the current water level.
3. You also need to specify planar areas in the fashion as surface areas.
   These planar areas will be used to calculate the run off from the pit walls
   using the precipitation specified in the model input for the CE-QUAL-W2
   part of PITLAKQ.


The procedure
+++++++++++++

PITLAKQ is calculating the following:

1. For each loading time step, typically one day but it can be set by the user,
   the model determines the surface area of each zone. Each area is multiplied
   by the given loading rate for this zone and the elapsed time (one day for
   our example case). These masses are accumulated to be added to the lake as
   soon as there is runoff.
2. The precipitation for the time period is calculated. If there is no
   precipitation, nothing happens. If there is precipitation, it will be
   multiplied with the planar area of each zone to yield the water volume
   supplied by each zone.
3. The water volume is added to a specified region of the lake.
   This region is determined by a user-defined number of layers counted from
   the lake surface and active lake segments per zone. In addition, the lake
   receives the accumulated masses from step 1. and an amount of heat provided
   by a given temperature multiplied by the volumes. After adding the masses to
   the lake, the accumulated masses are set to zero.
   This means the accumulation can begin anew with the next time step.

For example, if there is a 10-day period without precipitation, the model would
accumulate masses for all species for all zones according to the specified
loading rates and current surface areas for all zones. After 10 days the
calculated precipitation volumes for each zone would carry these masses
into the lake. For wet climates with frequent precipitation this results in
a steady loading from the pit walls. On the other hand, for dry climates with
infrequent precipitation this simulates longer periods without any loading
followed by short peaks of loading.

Model input
+++++++++++

The input for the loading is contained in the file ``loading.xlsx`` in the
directory ``input/loading``. The tab "Area" holds a table of surface areas
and planar areas in m\ :sup:`2` for each desired water table as well as start
and end segment of the lake in between which the load should be applied.
For one zone this table could look like this:

=========== ============ ============= ==================== =================
water_level zone1_planar zone1_surface  zone1_start_segment zone1_end_segment
=========== ============ ============= ==================== =================
152                    0             0                    4                 5
155              73278.2       97704.3                    4                 5
153              69492.0       92656.0                    6                 8
154              67777.0       90370.4                    6                 8
155              67048.0       89398.1                    6                 8
=========== ============ ============= ==================== =================

Typically there are more lines in a table, covering the whole expected range
of water table changes. Using a value of zero for areas effectively turns
them off for this water table. This allows to represent inactive areas for
certain areas. A table for three zones would have this header:

====================== ===
Header
====================== ===
water_level
zone1_planar
zone2_planar
zone3_planar
zone1_surface
zone2_surface
zone3_surface
zone1_start_segment
zone1_end_segment
zone2_start_segment
zone2_end_segment
zone3_start_segment
zone3_end_segment
====================== ===

The numbers for the areas need to be supplied by the user. Currently, there is
no extra tool for preprocessing that would generate the area information
from bathymetry information. Using GIS software, generation of the area
information should be straight forward.

The table under the tab "Conc" holds the loading data in g/m\ :sup:`2`/s.
Each zone name that was used in the table under tab "Area" must appear in the
column ``zone``. All species that are to be active for loading have to given in
the columns of the table as headings:

====== ========= ========= ========== ========= ==========
zone   cobalt    silver     zinc      tracer    calcium
====== ========= ========= ========== ========= ==========
zone1  1.00E-011 1.00E-010  1.00E-010 1.00E-010 1.00E-010
zone2  5.00E-011 1.00E-010  4.00E-010 1.00E-010 1.00E-010
zone3  0         1.00E-010  1.00E-011 1.00E-010 1.00E-010
====== ========= ========= ========== ========= ==========

In addition, all species in this table must be turned on using a ``1`` for
``active_constituents`` in the file ``input/w2/activeconst.txt``  in your
**model input**. Please **do not** turn them on as default in
``resources\activeconst.txt`` as this would affect all other models, which is
typically undesirable.
