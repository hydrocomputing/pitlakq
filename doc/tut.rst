PITLAKQ Tutorial
================

Preliminaries
-------------

This tutorial assumes that you have installed PITLAKQ properly and can run it
from the command line as explained in the section
:ref:`install-reference-label`.

Running Python Scripts
----------------------

Some of the task are done via Python scripts.
There are two options:

1. Run the script saved in a file from the command-line via
   ``python my_script.py``

2. Run from inside a `Jupyter notebook`_.

For the second option type::

    jupyter lab

A browser window will open.
Click on the drop-down menu ``New`` and select ``Python 3``.
This will open a new notebook.
Enter the Python commands into a cell and type ``<Shift><Enter>``.

.. _`Jupyter notebook`: http://jupyter.org/


.. include:: create_project.rst
.. include:: bathymetry.rst
.. include:: main_input.rst
.. include:: w2_input.rst
.. include:: results.rst
.. include:: precipitation.rst
.. include:: evaporation.rst
.. include:: inflows.rst
.. include:: charge_balancing.rst
.. include:: water_quality.rst
.. include:: gwh.rst
.. include:: extend_database.rst
.. include:: loading.rst
