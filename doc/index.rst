.. PITLAKQ documentation master file, created by
   sphinx-quickstart on Sat Sep 22 09:10:08 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PITLAKQ's documentation!
===================================

Contents:

.. toctree::
   :maxdepth: 2

   install
   tut
   cmdline
   support
   changelog

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

