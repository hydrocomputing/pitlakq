Charge balancing
----------------

The initial lake composition and all inflows need to be charge balanced.
This is important because the pH value is calculated via the charge.
PITLAKQ offers tools to make charge balancing of inflows easier, especially if
many inflow time steps need to be charged.

We supply measured concentrations for all inflows in an CSV
``sample_concentration.csv`` file:

.. include:: ../tut/pitlakq_tut/preprocessing/charge/sample_concentration.csv
   :literal:


Opened in a spreadsheet program, it looks like this:

.. image:: images/trib_conc.png

Now we create a script to start our charge balancing run:

.. literalinclude:: ../tut/pitlakq_tut/preprocessing/charge/do_trib_charge.py
   :language: python

and execute it:

``python do_trib_charge.py``

This will generate the files:

* ``Collie_River.txt``
* ``Groundwater-CRM-79.txt``
* ``Groundwater-CRM-85.txt``
* ``Groundwater-CRM-86.txt``
* ``Groundwater-CRM-89.txt``
* ``lake_start.txt``
* ``sample_concentration_.txt``
* ``surface_runoff.txt``


that contain this kind of information::

          date       time                        ph          dissolved_oxygen                  ammonium           nitrate_nitrite ...
    01.03.1997  00:00:00                   6.70000                   8.00000                   0.03000                   0.18000  ...
    05.01.2012  00:00:00                   6.70000                   8.00000                   0.03000                   0.18000  ...


Tributaries that are in contact with atmosphere my exhibit some deviations in
terms of carbon concentration. If the acid capacity is known, it can be used
as additional information. We run the script ``do_check_trib_charge.py``:

.. literalinclude:: ../tut/pitlakq_tut/preprocessing/charge/do_check_trib_charge.py
   :language: python

``python do_check_trib_charge.py``


It titrates the water toward a pH of 4.3 and compares the used amount acid
with of the measured value for this titration. It will output something like
this::

                date  calc  meas ratio
          01.03.1997  0.60  0.60  1.00
          05.01.2012  0.60  0.60  1.00
    min 0.0018992835095
    max 0.0018992835095
    average 0.0018992835095
    abs average 0.0018992835095
    run time 0.0929999351501

If the ratio is different from 1.0 you may adjust the value for ``factor`` in
``sample_concentration.csv`` for the Collie River water, re-run
``do_trib_charge.py`` and ``do_check_trib_charge.py``. Repeat until the
the ratio approaches 1.0. This excises has to be supported by fundamental
chemical understanding of the underlying process.