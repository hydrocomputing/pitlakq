Looking at results
------------------

Run your model with ``pitlakq run <project_name>``. The calculation should be
finished after a few minutes. Now we can visualize the temperature at one point
over time. Create a file called ``do_show_temp.py`` with this content:

.. literalinclude:: ../tut/pitlakq_tut/postprocessing/do_show_temp.py
   :language: python

This assumes the file is located in the ``postprocessing`` directory of your
project. Now run it:

``python do_show_temp.py``

You should see a picture like this:

.. image:: images/temperature.png