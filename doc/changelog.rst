Changelog
---------

Version 1.7.2
+++++++++++++

* No versions 1.7.0 and 1.7.1 due to upload errors
* Move Fortran code to own package `libpitlakq`_, which is now a dependency
* Make ``pitlakq`` a no-architecture package
* Build ``conda`` and ``pip`` packages
* Fix layer ordering (effect only on non-equidistant layer thickeness)
* Update deprecated NumPy calls

.. libpitlakq: https://gitlab.com/hydrocomputing/libpitlakq

Version 1.6.1
+++++++++++++

* Add gfortan DLLs

Version 1.6.0
+++++++++++++

* Update to Python 3.9
* Use `Loader` for reading `yaml` to avoid warning
* Add parallel PHREEQC calculations with processes on one machine
* Varies small typo and other fixes


Version 1.5.2
+++++++++++++

* Fix density calculation


Version 1.5.1
+++++++++++++

* Remove ``raw_input``

Version 1.5.0
+++++++++++++

* Add linear gridding for bathymetry data

Version 1.4.2
+++++++++++++

* Fix missing conversion from list to NumPy array

Version 1.4.1
+++++++++++++

* Fix indexing of generator the new version ``openpyxl`` returns
  instead of a list
* Add changelog to documentation


Version 1.4.0
+++++++++++++

* Update to Python 3.6
* Use ``m2w64-toolchain`` from the channel ``msys2`` on Windows for
  C and FORTRAN compiler
* First conda package for Linux


Version 1.3.2
+++++++++++++

* ``pip`` installation without dependencies


Version 1.3.1
+++++++++++++

* Fix indexing in ``phreeqc``
* Add ``w2.pyd`` to Windows release

Version 1.3.0
+++++++++++++

* Support Python 3.5
* Release as ``conda``  package
* Move output directory ``balance`` under ``output``
* Turn user warnings into exceptions.
* Corrected for missing cos2sat values in netCDF file for high pH values
* Deep copy only needed data not whole input
* Add print out information for exceptions with small or wrong numbers
* Add deletion of input data that is already written to W2 readable files
* Correct time slots and reverse color map for pH values
* Corrected bathymetry preprocessing. Bottom is no always the second grid layer
* Allow three successive PHREEQC errors
* Add automatic saving of figures
* Add more specie names
* Use LLNL database as default
* Add tutorial for loading
* Add script to start Pyro servers for distributed PHREEQC runs
* Add script for charge balancing loading input data
* Add script to find date and concentration for a given water level
* Add script to calculate waterlevel-volume relation from W2 results
* Add script to convert balance files, so that relative time in days is used
  instead of date
* Add README
* Add W2 names for precipitation concentration
* Add some default concentration names to
  `` pitlakq/templates/precipitation_concentration.txt``
  Eventually, there need to be all names here
* Add equilibrium species to charge for preprocessing of charge for loadings
* Add cut of data below given heights
* Add all additional species to TDS.
* Remove ``mx.DateTime`` from project and use only ``datetime``
* Add W2 manual for W2 version 2
* Add initialization script to automatically setup PITLAKQ
* Replace ``optparse`` with ``argparse``
* Add option to show current project names


Version 1.2.0
+++++++++++++

* Include last day of month in groundwater time step
* Add print of error information if specie balance produces ``inf`` or ``nan``
* Keep 10 old message files
* Ignore all sink/source terms for output per default
* Allow user-specified additional species and minerals
* Add check for defined constituents and mineral names
* Order of constituent flexible by for easier user input
* Add pit wall loading
* Constants are now input values
* Add more species to database
* PHREEQC database name is now an input parameter
* Keep 10 old copies of the PHREEQC output file for debugging
* Add Calcite and Siderite
* Enable parallel PHREEQC again
* Add PHREEQC executable for Mac OS X
* Add documentation for database extension and loading


Version 1.1.0
+++++++++++++

* Add help for command line options
* Support W2 compilation on Mac OS X
* Replace ``pynetcdf`` with ``netCDF4``
* Set default algorithm for ice calculation to detailed
* Add preprocessing tool to allow user input of groundwater zones
* Added tool to find ice-free and freeze-over dates
* Add sub-module ``gwh`` for groundwater inflow based on lake water levels
* Add check for consistent names in distribution, flow, and concentrations
* Add commandline option to create new project.
* Add graphing for lake volume and area as function of water level
* Automation of Surfer to calculate volumes and planar areas for given levels
* Script to create waterlevel volume and area data and write them into a file
* Add ``gwh`` documentation
