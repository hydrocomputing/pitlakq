Adding New Species to the Database
----------------------------------

PITLAKQ currently comes with 56 pre-defined solute species and 7 minerals.
It is also possible to add user-defined species.
There are several steps to incorporate own species in PITLAKQ.
These species will be transported in CE-QUAL-W2 as tracers.
All water quality processes will take place in PHREEQC. Furthermore, minerals
that are allowed to form and precipitate can also be added. As with other
minerals, they can be allowed to fully precipitate as soon as they
are over-saturated or can be allowed to settle with potential dissolution
on their way to the bottom if conditions change.

The next steps will modify files in the installation directory.
To avoid overwriting these files with defaults when updating ``pitlakq``,
it is recommend to copy the directories ``<install_dir>/pitlakq/templates``
and ``<install_dir>/pitlakq/resources`` into different locations.
These new locations need to be listed in your ```.pitlakq`` file.
(See :ref:`init-reference-label` for details.)

There are four steps involved in adding a new specie:

1. Add a new line in the file ``resources/const_names.txt`` for solute or in
   the file ``resources/mineral_names.txt`` for mineral species.
2. Add a corresponding entry in the file ``templates/w2.yaml``.
3. Add a corresponding entry in the file ``templates/activeconst.txt``.
4. Optional: Update the PHREEQC database if it **does not contain** the new
   specie and the corresponding reactions yet.

These steps may get a bit more stream-lined in the future. Due to the
fact that PITLAKQ was not designed from ground up for adding new species
by the user, consolidating the information from steps 1. to 3. in one place
would mean a major re-arrangement of the code base. Let's go over the steps in
detail.

Add a line to the files ``resources/const_names.txt`` or ``resources/mineral_names.txt``
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

The file ``resources/const_names.txt`` has this structure::

    key    name          w2ssp  w2ssgw  phreeqc_name  molar_weight  rate_name

    alkal  alkalinity    void   void    void          void          void
    mn     manganese_II  mnssp  mnssgw  Mn(+2)        54.938        Mn_di
    na     sodium        nassp  nassgw  Na            22.9898       void

There are more lines in this file. The three lines above are chosen to
exemplify how different options look like.

The file ``resources/mineral_names.txt`` has this structure::

    key     name            w2ssp     w2ssgw  phreeqc_name  molar_weight  rate_name

    feoh3   iron_hydroxide  feoh3ssp  void    Fe(OH)3       106.871       Fe(OH)3(a)r
    caco3   calcite         auto      void    Calcite       100.071       void

There are more lines in this file. The two lines above are chosen to exemplify
how different options look like.

The columns have these meanings:

key
    Internal key used for CE-QUAL-W2 and other purposes. It must be unique.
    It is likely that one-letter names or very short names are already taken.
    PITLAKQ will warn you when this is the case. In this case, please choose
    another name.
    It is recommended to use a name that is as close as possible to the
    chemical symbol. Technically, any name consisting of English lowercase
    letters, digits and the underscore (``_``) can be used. However, digits
    cannot appear at the beginning of a name.

name
    A descriptive name for the specie. The name must be unique among the
    names in this column. The same name rules as for ``key`` apply.

w2ssp
    Name used for exchange with PHREEQC. This is typically ``<name>ssp``.
    A specific name is only used for pre-defined species. As a user you can
    only choose between the option ``auto`` or ``void``. The entry ``auto``
    should be used for all species for which you want PHREEQC to use in its
    reactions. The actual name will be generated internally. Using ``void`` in
    this column prevents the specie to be used for PHREEQC calculations.
    This is useful for species that don't make sense in PHREEQC. For example,
    ``alkalinity`` in the example above is not used. It is the original
    alkalinity calculated in CE-QUAL-W2. It is contained only for technical
    reasons and serves no actual purpose for modeling. Due to the way
    the source code of CE-QUAL-W2 is structured, it would require a major
    re-write to turn off alkalinity with other means. Using ``void`` can be
    useful for other species such as tracers. This way you can add an unlimited
    number of tracers.

w2ssgw
    Name used for exchange with groundwater. This is typically ``<name>ssgw``.
    A specific name is only used for pre-defined species. As a user you can
    only choose between the option ``auto`` or ``void``. The entry ``auto``
    should be used for all species for which the groundwater should act as
    source and sink. The actual name will be generated internally. Using
    ``void`` in this column prevents any exchange with the groundwater for
    this specie.
    This is useful for minerals that can be transported in the lake but
    typically not in the groundwater due to velocities that are orders of
    magnitude smaller.

phreeqc_name
    This is the name of the specie used in PHREEQC. It must appear in the
    database in ``SOLUTION_MASTER_SPECIES`` for solutes or in ``PHASES``
    for minerals. See the PHREEQC manual for more details.

molar_weight
    The molar weight of the specie. This must be the same value as used in the
    PHREEQC database. So far there are no know use cases where this value
    differs from the one in PHREEQC. It you know of one, please tell the
    model developers.

rate_name
   This is an alternative name for the specie used in PHREEQC's rate reactions.
   You must name the specie matching the corresponding entry under ``KINETICS``
   in the PHREEQC database.

A typical entry in ``resources/const_names.txt`` would look like this::

    key name      w2ssp   w2ssgw  phreeqc_name    molar_weight    rate_name

    .... other lines here ....

    van vanadium  auto    auto    V               50.9415         void


Since ``v`` is already used in CE-QUAL-W2 (PITLAKQ will warn you about it),
we choose ``van`` for ``key``. The entry for ``name`` is just the lower case
version of the chemical name. The entry for ``w2ssp`` must be ``auto`` because
we want our species to react in PHREEQC. Likewise, the entry for ``w2ssgw`` is
also ``auto`` because we would like the exchange with groundwater to be possible.
It can be enabled and disabled for each model but must be ``auto``, otherwise
it cannot be enabled at all. The PHREEQC name is just the normal chemical name
and you must use the same name as in the PHREEQC database.
The molar weight is copied from the PHREEQC database. We don't want to use
kinetic rate reactions, therefore ``rate_name`` is ``void``.

Adding a new line at the end of the file ``resources/mineral_names.txt`` should
look like this::

    key     name            w2ssp     w2ssgw  phreeqc_name  molar_weight  rate_name

    .... other lines here ....

    caco3   calcite         auto      void    Calcite       100.071       void

The meanings are the same as above. We choose a unique name for the ``key``.
In our case the lowercase version of the chemical formula. The ``name`` is the
lowercased PHREEQC name. We want reactions in PHREEQC, hence ``auto`` for
``w2ssp``. Minerals typically are not transported in the groundwater, therefore
``void`` in ``w2ssgw``. The molar weight is again the value PHREEQC uses.
We also do not want rate reactions and set ``rate_name`` to ``void``.


Add an entry in ``templates/w2.yaml``
+++++++++++++++++++++++++++++++++++++

We need to tell CE-QUAL-W2 about the new specie. We need to add an entry
inside the group ``initial_concentrations``. Going with our example
for vanadium it would look like this:


.. code-block:: yaml

    initial_concentrations:
        # many more entries here
        vanadium:
            default: 0.0
            w2_code_name: van
            dimension:
                - number_of_layers
                - number_of_segments

Unless you want a default initial concentration different from zero, add
``default: 0.0``. The entry for ``w2_code_name`` must be the same as in the
column ``key`` in ``resources/const_names.txt`` or
``resources/mineral_names.txt``. Always use the same entries for ``dimension``.

Add an entry in ``templates/activeconst.txt``
+++++++++++++++++++++++++++++++++++++++++++++


The file ``templates/activeconst.txt`` looks like this::

    constituent_name active_constituents initial_concentration inflow_active_constituents tributary_active_constituents precipitation_active_constituents
    .... other lines here ....
    iron_II          0                   0                     0                          0                             0
    .... other lines here ....
    silver           0                   0                     0                          0                             0
    .... other lines here ....

Per default all species should be inactive. Using this file copied to your
model input, you can activate any specie you need for any if the columns.

Adding a new specie is simple. Just use the name you entered in the column
``name`` in ``resources/const_names.txt`` or ``resources/mineral_names.txt``
and copy all the zeros from the line above:

::

    constituent_name active_constituents initial_concentration inflow_active_constituents tributary_active_constituents precipitation_active_constituents
    .... other lines here ....
    vanadium           0                   0                     0                          0                             0


Update the PHREEQC database
+++++++++++++++++++++++++++

You may need to update your PHREEQC database to reflect your changes.
You don't need to do anything, if the database already contains all species you
added including all the reactions that are needed for your site.

You can also specify a different database. Just add this to your
``pitlakq.yaml``:

.. code-block:: yaml

    phreeqc:
        # more lines here
        phreeqc_database_name:
            value: my_database.dat ## default is phreeqc_w2.dat

You need to copy the database file ``my_database.dat`` into the directory
``resources`` of you PITLAKQ install. Refer to ``.pitlakq`` where this is
on your computer.

