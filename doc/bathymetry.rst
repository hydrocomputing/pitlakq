Preprocess the bathymetry data
------------------------------

The bathymetry of the lake needs to be discretized. CE-QUAL-W2 and therefore
PITLAKQ is two-dimensional along x and z.
The y-direction is parametrized as cell width.
Let's assume we have a lake that looks like the following picture.


.. image:: images/tut_lake.png

We save the gridded data that where used to generate this contour plot in a
a SURFER 6 text file named ``bath_asci.grd``
(this default name can be changed with configuration options if needed).
The file content look like this::

    DSAA
    100 51
    0 1000
    0 500
    150.03254732465 214.3417454463
    202.0000000052914 202.5142874143128 203.0243218381507

Currently, PITLAKQ only supports this file format.
Adding more formats is possible.

If there are many points with lake bottom elevation data, linear interpolation
may be accurate enough.
PITLAKQ provides a pre-processing tool for creating a SURFER 6 text file
with linear interpolation.

These are the data points that were used to create the above contour map:

.. image:: images/lake_bottom_points.png

You can create this plot with this code:

.. literalinclude:: ../tut/pitlakq_tut/preprocessing/do_show_points.py
   :language: python

The file named ``bath_data.txt`` with the data looks like this:

.. literalinclude:: ../tut/pitlakq_tut/preprocessing/input/bath_data.txt
   :encoding: utf-8
   :lines: 1-10

This Python program creates the SURFER file for the project ``pitlakq_tut``,
using 100 grid locations in x and 51 grid locations in y direction:

.. literalinclude:: ../tut/pitlakq_tut/preprocessing/do_make_bath.py
   :language: python

The resulting grid file allows to generate this contour plot:

.. image:: images/contour_linear_interpolation.png

You can create this plot with this code:

.. literalinclude:: ../tut/pitlakq_tut/preprocessing/do_show_contour.py
   :language: python

This looks similar to the SURFER-generated contour plot.
An analyses shows that the surface area differs by about 6% and the lake volume
by about 9%.
The differences will be smaller for denser data points.

This tool can also be used to apply a different griding algorithm or
program:

1. Save the griding result in x-y-z format such as in the file
   ``bath_data.txt`` with high resolution, i.e. many data points.

2.  Apply the tool as described above with many interpolation points
    in x and y direction.

Alternatively, you can save the griding results directly in the
SURFAER text format.

Now, we specify how our lake grid is going to look like.
PITLAKQ currently supports either East-West or North-South directed grid
layouts.
This due to the (here not covered coupling to regularly gridded groundwater
models.
We place a file called  ``reservoir.txt`` inside our directory
``preprocessing/input``.
This is the file content:

.. literalinclude:: ../tut/pitlakq_tut/preprocessing/input/reservoir.txt
   :encoding: utf-8

We have 9 columns running from East to West with a distance of 100 each.
We could vary the distance for each column (or segment). We also choose a
equi-distant discretization in the vertical with 1 m per layer for 50 layers.

We supply more information in the file ``preprocessing.yaml``:

.. literalinclude:: ../tut/pitlakq_tut/preprocessing/input/preprocessing.yaml
   :language: yaml

This file is in the YAML_ format. The entry ``orientations`` here says that our
first (sub)lake (we only have one sub-lake, which takes up the total lake) is in
East-West direction.

Now we start our preprocessing program in a similar fashion as setting up a new
project. Create file called ``do_preprocessing.py`` with this content:

.. literalinclude:: ../tut/pitlakq_tut/preprocessing/do_preprocessing.py
   :language: python

Replace the name ``pitlakq_tut``  with your project name. e.g.
``my_project``.

Run it: ``python do_preprocessing.py``. If everything goes well you
will have a file named ``bath.nc`` in the ``output`` directory under
``preprocessing``. This file ``bath.nc`` will be used as input for our model
run. It is in netCDF_ format.

.. _netCDF: http://www.unidata.ucar.edu/software/netcdf/

The file ``bath.nc`` can be converted into a human readable format with
``ncdump bath.nc``::

    netcdf bath {
    dimensions:
            segment_lenght = 10 ;
            layer_height = 52 ;
    variables:
            double segment_lenght(segment_lenght) ;
            double starting_water_level(segment_lenght) ;
            double segment_orientation(segment_lenght) ;
            double layer_height(layer_height) ;
            double cell_width(layer_height, segment_lenght) ;
    data:

     segment_lenght = 100, 100, 100, 100, 100, 100, 100, 100, 100, 100 ;

     starting_water_level = 200, 200, 200, 200, 200, 200, 200, 200, 200, 200 ;

     segment_orientation = 9.99, 1.57, 1.57, 1.57, 1.57, 1.57, 1.57, 1.57, 1.57,
        9.99 ;

     layer_height = 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1 ;

     cell_width =
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 75.6157364131951, 145.089953029845, 222.101772341138, 279.100893272702,
        279.22978559541, 222.26566587868, 141.54165990374, 27.8009873136653, 0,
      0, 64.9264248803291, 139.74267671092, 215.582246846721, 274.371310775368,
        274.382373960408, 216.115799371682, 134.929618517385, 23.2367196470327, 0,
    ...

We can visualize our lake grid. Create a file called ``do_show_bath.py`` with
this content:

.. literalinclude:: ../tut/pitlakq_tut/preprocessing/do_show_bath.py
   :language: python


Run it: ``python do_show_bath.py``. You should see a picture like
this:

.. _bath-figure-label:

.. figure:: images/bath_grid.png

   Lake Bathymetry

Copy the file ``bath.nc`` into the directory ``/tut/pitlakq_tut/input/w2/``.

.. _YAML: http://en.wikipedia.org/wiki/YAML
