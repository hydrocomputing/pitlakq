Adding evaporation
------------------

Now we also turn on evaporation:

.. code-block:: yaml

    calculations:
        evaporation_included_in_water_budget:
            value: True

Now re-run your model and look at the water level that printed to the screen.
It should drop with each time step. Now we also us measured water level data
to compare to our calculations:

.. literalinclude:: ../tut/pitlakq_tut/postprocessing/measured_wl.txt
   :encoding: utf-8

We modify our script a bit:

.. literalinclude:: ../tut/pitlakq_tut/postprocessing/do_show_wl_measured.py
   :language: python

and  run it:

``python do_show_wl_measured.py``

You should see a picture like this:

.. image:: images/wl_precip_evp.png