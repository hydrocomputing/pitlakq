Turning on precipitation
------------------------

You can see that the water level does not change over the whole runtime of the
model. This is due to fact that there no sources or sinks to our lake. Let's add
some precipitation. Add this to your ``w2.yaml`` file:

.. code-block:: yaml

    calculations:
        precipitation_included:
            value: True

Now re-run your model and look at the water level that is printed to the screen.
It should drop with each time step. We can visualize the lake water level
with this script:

.. literalinclude:: ../tut/pitlakq_tut/postprocessing/do_show_wl.py
   :language: python

This assumes the file is located in the ``postprocessing`` directory of your
project. Now run it:

``python do_show_wl.py``

You should see a picture like this:

.. image:: images/wl_precip.png