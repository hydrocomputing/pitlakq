Installing PITLAKQ
------------------

.. _install-reference-label:

A Python Program
****************

PITLAKQ is written in Python and FORTRAN.
It includes CE-QUAL-W2_, a public domain code for hydrodynamics and water
quality of lakes and reservoirs version 2.0.
The latter is implemented in FORTRAN.
Therefore, you need to install Python.
You don't need to know how to program in Python in order to use PITLAKQ.
However, it can be very useful to have some basic Python knowledge.
There is no need to understand FORTRAN because the FORTRAN source code is
complied into Python extension.


Installing of Miniconda or Anaconda
***********************************

PITLAKQ relies on many external packages.
Therefore, you need to install either Miniconda_ or Anaconda_.
Anaconda is a Python distribution with about 200 Python packages.
It is a download of about 400 to 500 MB and will take about 2 GB on your
hard drive.
Miniconda is much much smaller and is therefore the preferred option.
Both allow the comfortable installation of further packages as well as creating
of so called environments.
We will take advantage of both features.

PITLAKQ works on Microsoft Windows, Mac OS X, and Linux.
It is packaged as a conda_ package.

Please download Miniconda_.
Please choose the right version for your operating system and take
Python 3.9 (or higher, if available.)
Install, following the instructions on the download web site.
Please install only for you, with your user rights,
**not** for all, which would require administrator rights.
You can check if the installation was successful by opening a new terminal
window.
On Windows this can be achieved by typing ``Anaconda Prompt`` while hovering
with the cursor over the Windows start button (lower left on a standard install)
and clicking on the entry ``Anaconda Prompt``.
On Mac OS X and Linux a normal terminal window should work.

Now type::

    python

You should see a response and ``>>>`` with a blinking curses.

Type::

    >>> exit()

to terminate this Python session.
You can also identify the installed Python version with::

    python --version


Installing PITLAKQ
******************

In a terminal window type::

    conda config --add channels conda-forge
    conda config --add channels msys2
    conda config --add channels hydrocomputing

Currently, PITLAKQ is available for Python 3.9.
If you have an installed Python with the same version
(see above for finding out the current Python version),
you may skipp the following steps that create a new environment.
However, creating a new environment can be useful even if you already have the
right Python version, because it allows to separate the Python installation
for PITLAKQ from potential other Python applications.
There are many more interesting details about `conda environments`_.

Creating a `conda` environment
++++++++++++++++++++++++++++++

Type::

    conda create -n pitlakq39 python=3.9

Answer "Yes" to the question(s) and type enter.

Next, type::

    activate pitlakq39

On Linux or Mac this needs to be::

    source activate pitlakq39



.. note::

    There might be ``pitlakq`` packages for different Python versions.
    To find out, type::

        conda search pitlakq

    You should see an answer like this::

        pitlakq   1.6    py39_0  hydrocomputing/win-64

    This means version 1.6 of ``pitlakq`` is available for Python 3.9.
    If it would say ``py36_x``, you should create an environment for Python 3.6.
    For example::

        conda create -n pitlakq36 python=3.6

    and activate accordingly::

        activate pitlakq36


You can choose a different name but you need to be consistent.
Here we use ``pitlakq39``.

The prompt should change to ``(pitlakq39)``.

Installing PITLAKQ
++++++++++++++++++


Type::

    conda install pitlakq

Answer "Yes" to the question(s) and type enter.

This will take a while.
After this is finished, you can check if ``pitlakq`` is properly installed by
typing::

    pitlakq

You should see something like this::

    usage: run_pitlakq.py [-h] [-v] {init,run,create,introspect} ...

    positional arguments:
      {init,run,create,introspect}
        init                initialize PITLAKQ and create `.pitlakq`
        run                 start the calculation of a model
        create              creates a new project folder with empty sub folders
        introspect          find out internals of PITLAKQ or your projects

    optional arguments:
      -h, --help            show this help message and exit
      -v, --version         show version info and exit


.. _init-reference-label:

Initializing PITLAKQ
*********************

PITLAKQ needs to know about two directories:

1. A path for the models
2. A temporary path to copy internal files during model execution

The easiest way to do let PITLAKQ know is to run::

    pitlakq init

This will create a file named ``.pitlakq`` in your home directory.
It will contain content like this::

    model_path:
        C:\Users\<user_name>\pitlakq_work\models

    ram_path:
        C:\Users\<user_name>\pitlakq_work\RAM

    resources_path:
        <install_dir>\pitlakq\resources

    template_path:
        <install_dir>\pitlakq\templates

Where ``<user_name>`` is your user name and ``<install_dir>`` is the
directory ``pitlakq`` is installed in.
You can change these paths, forcing PITLAKQ to look into a different directory
for models (``model_path``)  of using a different directory for temporary files
(``ram_path``).
This is an advanced technique and not needed for normal use.

You can place ``.pitlakq`` somewhere else.
Just choose a directory and set the `environmental variable`_ ``PITLAKQHOME``
pointing this path.

Check if it runs
****************

You can find out what projects are available with::

    pitlakq introspect --projects

If the are no projects, you get an according message::

    Projects:
    Currently no projects.

Copy one or more projects, i.e. directories, from the
`provided tutorial projects`_ into this path.

.. _`provided tutorial projects`: tut.zip

Now ::

    pitlakq introspect --projects

should show you the projects you can run::

    Projects:
         pitlakq_tut
         pitlakq_tut_gwh
         pitlakq_tut_loadin
         pitlakq_tut_qual

Choose a name from this list and run the model with::

    pitlakq run pitlakq_tut

This should produce a lot of screen output.
The model might run several minutes or longer.
You can always stop a model run with the key combination ``<Ctrl><C>``.

.. _Anaconda: https://www.continuum.io/downloads
.. _conda: https://conda.io/docs/
.. _`conda environments`: https://conda.io/docs/user-guide/tasks/manage-environments.html
.. _Miniconda: https://conda.io/miniconda.html
.. _CE-QUAL-W2: http://www.cee.pdx.edu/w2/
.. _`home directory`: http://en.wikipedia.org/wiki/Home_directory
.. _`environmental variable`: http://en.wikipedia.org/wiki/Environment_variable