Adding inflow
-------------

``mytribname_inflow.txt``

We add some inflow:

.. code-block:: yaml

    date            time  tributary_inflow
    01.01.1997     00:00      0.005
    01.01.2011     00:00      0.0

Re-run the model and see how the water level in the lake changes.