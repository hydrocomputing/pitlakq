
Create a W2 input file
----------------------

The file ``<my_project_name>/input/w2/w2.yaml`` has more entries:

.. literalinclude:: ../tut/pitlakq_tut/input/w2/w2.yaml
   :language: yaml

Most of the information in this file are handed down to CE-QUAL-W2.
In fact, the file  ``<install_dir>/pitlakq/templates/w2.yaml`` has an attribute
``w2_code_name`` for most of the entries. This corresponds to variable name
in CE-QUAL-W2. Therefore, more information about each of this entries can be
found in the CE-QUAL-W2 manual or in FORTRAN source code.

We provide a model title and detailed information about the model bounds. Here
we just repeat the 10 by 52 setup from our ``bath.nc``. These dimensions will be
used to check if other input values we will specify later are of the correct
shape.

Then we start a new stream with ``---`` and include the file ``bath.nc`` with
help of the directive ``!File``- This file is in netCDF format and we would
like it to be in the group ``bathymetry``. The specification of this group
allows to override parts of the data in ``bath.nc``. We do this by providing
an different value for the starting water label and set ``starting_water_level``
in the group ``bathymetry`` to 195.0 (meters). No need to go into the netCDF
file and change data there.

The group ``branch_geometry`` provides information for each branch. We only
have one branch in this case. Multi-branch setups are possible but more
complicated and only recommend for complex cases such as complex geometries
or large difference in lake water levels. The ``branch_upstream_segments`` is
typically ``2`` for the first branch. The value for
``branch_downstream_segments`` is ``8`` instead of ``9``, which would be the
last active segment. Looking at the lake cells  (:ref:`bath-figure-label`),
we can see that the last segment in only one meter deep. This means setting
``branch_upstream_segments`` to ``9`` works only if the water level is at
200 m. Below this segment is dry and CE-QUAL-W2 terminates with an error
message.

The ``waterbody_coordinates`` are in degrees of latitude and longitude, where
the southern hemisphere gets negative values for latitude.  Even though, we don't
have any tributary inflow yet, wen need (for input reasons in CE-QUAL-W2)
provide on tributary, which we do in group ``tributaries``. As we will see,
we just put a value of zero for inflow to turn it off. We don't want to
calculate the ice cover and therefore set ``allow_ice_calculations`` in the
group ``ice_cover`` to ``False``.

We use very simple initial conditions. Setting ``initial_temperature`` to
positive value uses this value for isothermal conditions throughout the entire
lake. Using a ``-1``  or ``-2``, we can specify a one-dimensional or
two-dimensional temperature distribution in ``temperature``. That is also the
reason we need to repeat this ``temperature``. Similarly, even
though we turned ice calculations of, we need to set ``ice_thickness`` because
the default value has different dimensions. Similarly, we set the
``initial_concentrations`` of a specie ``tracer`` to ``10.00`` even though, we
don't calculate species yet.

Now we include more files using the directive ``!File``. Let's have a look at
these files.

The meteorology file provides input data for:

* air temperature,
* dew point temperature,
* wind speed,
* wind direction and
* cloud cover

The time resolution can be as fine as desired up to one minute. Finer resolution
would be possible but the input format does not support it yet.

.. include:: ../tut/pitlakq_tut/input/w2/meteorology.txt
   :literal:
   :end-line: 10


The precipitation file is simpler. It only has the entry for precipitation in
m/s:

.. include:: ../tut/pitlakq_tut/input/w2/precipitation.txt
   :literal:
   :end-line: 10

Likewise, the precipitation temperature:

.. include:: ../tut/pitlakq_tut/input/w2/precipitation_temperature.txt
   :literal:
   :end-line: 10

and the concentration:

.. include:: ../tut/pitlakq_tut/input/w2/precipitation_concentration.txt
   :literal:
   :end-line: 10

For each entry in ``tributary_names``, we need to specify:

* <mytribname>_inflow.txt
* <mytribname>_temperature.txt
* <mytribname>_concentration.txt

Replace ``<mytribname>`` with name(s) you used.

We don't have any inflow:

.. include:: ../tut/pitlakq_tut/input/w2/mytribname_inflow.txt
   :literal:
   :end-line: 10

Therefore, data in the temperate file:

.. include:: ../tut/pitlakq_tut/input/w2/mytribname_temperature.txt
   :literal:
   :end-line: 10

and the concentration file:


.. include:: ../tut/pitlakq_tut/input/w2/mytribname_concentration.txt
   :literal:
   :end-line: 10

are ignore.

The specification of not-used values can be a bit misleading. PITLAKQ follows
CE-QUAL-W2 as close as possible to allow the of the CE-QUAL-W2's manual as much as
possible.