
Create a new project
--------------------

The first thing we need to do is to create new, still empty, project. 
Create a new project named ``my_project`` by typing::

    pitlakq create my_project


Now you should have a directory inside your ``models`` directory with a layout 
that looks like this::

    pitlakq_tut
    |
    +---balance
    +---input
    |   +---main
    |   +---w2
    +---output
    |   +---sediment
    |   +---w2
    +---postprocessing
    +---preprocessing
        +---input
        +---output
        +---tmp