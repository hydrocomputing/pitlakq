
Water quality calculations
-------------------------------

To do water quality calculations, we need to add more entries to our
``pitlakq.yaml``:

.. literalinclude:: ../tut/pitlakq_tut_qual/input/main/pitlakq.yaml
   :language: yaml

We set the options ``lake_calculations`` and ``kinetic`` in the group ``lake``
to ``True``. We want to couple with PHREEQC. Therefore, we set
``phreeqc_coupling_lake`` in the group ``couplings`` to ``True``. In addition,
we add the group ``phreeqc``, in which we specify the active constituents as
list of names under ``lake_active_const_names``. We like to have some
of constituents to be calculated with rate equations rather than an equilibrium
approach. We list these names under ``rates``.

The mapping of different names is defined in ``resources/const_names.txt``
This is an expert from this file with the constituents we calculate::


    key   name           w2ssp   w2ssgw   phreeqc_name  molar_weight   rate_name
    fe    iron_II        fessp   fessgw   Fe(+2)        55.847         Fe_di
    fe3   iron_III       fe3ssp  fe3ssgw  Fe(+3)        55.847         Fe_tri
    so4   sulfate        so4ssp  so4ssgw  Sulfat        96.064         Sulfat
    mn    manganese_II   mnssp   mnssgw   Mn(+2)        54.938         Mn_di
    mn3   manganese_III  mn3ssp  mn3ssgw  Mn(+3)        54.938         Mn_tri

Important here are the columns ``key``, ``phreeqc_name`` and ``rate_name``.
The rates themselves are defined in ``resources/phreeqc_w2.dat``

Analogous we allow the minerals given in ``lake_active_minerals_names``
to form. We specify with ``mineral_rates`` what minerals are formed
from rate constituent. The relevant part from ``resources/mineral_names.txt``
looks like this::

    key   name             w2ssp     w2ssgw   phreeqc_name   molar_weight   rate_name
    feoh3 iron_hydroxide   feoh3ssp  noGW     Fe(OH)3(a)     106.871        Fe(OH)3(a)r

We need to start with a charge balanced water. Therefore we choose one
constituent to be used for charging. We choose ``Cl`` for ``charge`` because
we don't need to evaluate chloride.

PITLAKQ supports parallel processing of PHREEQC calculations. This an advanced
feature and we turn ``parallel_phreeqc``  off. All our carbon concentrations are
given in terms of ``C``, therefore we set ``c_as_c`` to true. This will run
all PHREEQC calculation with the option ``as C``. We want daily PHREEQC time
steps. Actually they are the default.

Iron precipitation co-precipitates phosphorus. We specify the ratio for this
process with ``fe_po4_ratio`` a typical value is ``10``.



We also modify our ``w2.yaml``  file:


.. literalinclude:: ../tut/pitlakq_tut_qual/input/w2/w2.yaml
   :language: yaml

We add another tributary and specify initial concentrations. Add a second
tributary requires to add some more information how we would like tributary
inflows to placed. We choose by density. Due to logic of the input, we go
have to specify top and bottom elevations, even though they are not used.
This could be improved in PITLAKQ but would require to modify the input
system.

After we start our run with ``pitlakq run pitlakq_tut_qual`` and let it run
for a while. In the mean create this script:

.. literalinclude:: ../tut/pitlakq_tut_qual/postprocessing/do_show_ph.py
   :language: python

Running it ``pythont do_show_ph.py`` (remember the calculations
should have run for few minutes), you should see a figure like this:

.. image:: images/ph.png

Depending on how long you model run the figure might look considerably
different.

This script:

.. literalinclude:: ../tut/pitlakq_tut_qual/postprocessing/do_show_ca.py
   :language: python

after run ``pythont do_show_ca.py`` displays this figure

.. image:: images/ca.png

We can pout several constituents in one script:

.. literalinclude:: ../tut/pitlakq_tut_qual/postprocessing/do_show_many.py
   :language: python

After running it with ``python do_show_many.py`` you should see the
windows popping up with these figures. After you close one, the next will up.

.. image:: images/al.png

.. image:: images/cl.png

.. image:: images/dox.png

.. image:: images/fe2.png

.. image:: images/fe3.png

.. image:: images/ka.png

.. image:: images/mg.png

.. image:: images/po4.png

.. image:: images/so4.png
