# PITLAKQ - The Pit Lake Hydrodynamic and Water Quality Model

[PITLAKQ](http://www.pitlakq.com/) couples the models
[CE-QUAL-W2](http://www.cee.pdx.edu/w2/) and
[PHREEQC](https://water.usgs.gov/water-resources/software/PHREEQC/index.html)
and adds  new functionality to account for the pit lake requirements.
It includes the most important processes in pit lakes.
For example, several sources of acidity such as erosion or release from
submerged sediments and spatially distributed groundwater inflow help to better
represent pit lake conditions.
Furthermore, PITLAKQ can account for the effects of water treatment on water
quality.
The two-dimensional model setup with one vertical and one horizontal dimension
allows having sinks and sources with defined spatial locations.

PITLAKQ is Open Source (BSD license).
You can find the [source on GitLab](https://gitlab.com/hydrocomputing/pitlakq).
Please sign up for the [email list](http://groups.google.com/group/pitlakq-users)
to stay current with the latest news.
Please subscribe (pitlakq-users+subscribe@googlegroups.com) to get your
questions about PITLAKQ answered.

Please [contact](http://www.pitlakq.com/contact.html) us for more information.
