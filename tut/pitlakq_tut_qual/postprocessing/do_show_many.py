from pitlakq.postprocessing import depth_time_contour

for name in ["na", "cl", "dox", "po4", "ca", "so4", "mg",
             "al", "fe3", "fe"]:
    species=[{"name": name, "levels": "auto"}]
    depth_time_contour.main("../output/w2/out.nc", species=species, location=5)

