      date                       cum_lake cum_tributaries_trib_mytribname                 cum_atmosphere              cum_balance_error
31.01.1998                     0.00276269                     0.00000000                     0.00000000                     0.00004598
28.02.1998                     0.00280118                     0.00000000                     0.00000000                     0.00008447
31.03.1998                     0.00284231                     0.00000000                     0.00000000                     0.00012560
30.04.1998                     0.00289094                     0.00000000                     0.00000000                     0.00017423
31.05.1998                     0.00294130                     0.00000000                     0.00000000                     0.00022459
30.06.1998                     0.00299157                     0.00000000                     0.00000000                     0.00027486
31.07.1998                     0.00305376                     0.00000000                     0.00000000                     0.00033705
31.08.1998                     0.00312736                     0.00000000                     0.00000000                     0.00041065
30.09.1998                     0.00318086                     0.00000000                     0.00000000                     0.00046415
31.10.1998                     0.00320945                     0.00000000                     0.00000000                     0.00049273
30.11.1998                     0.00319982                     0.00000000                     0.00000000                     0.00048311
31.12.1998                     0.00318523                     0.00000000                     0.00000000                     0.00046852
31.01.1999                     0.00317018                     0.00000000                     0.00000000                     0.00045347
28.02.1999                     0.00315571                     0.00000000                     0.00000000                     0.00043900
31.03.1999                     0.00314131                     0.00000000                     0.00000000                     0.00042460
30.04.1999                     0.00313794                     0.00000000                     0.00000000                     0.00042123
31.05.1999                     0.00314073                     0.00000000                     0.00000000                     0.00042402
30.06.1999                     0.00314621                     0.00000000                     0.00000000                     0.00042950
31.07.1999                     0.00316947                     0.00000000                     0.00000000                     0.00045276
31.08.1999                     0.00318583                     0.00000000                     0.00000000                     0.00046912
30.09.1999                     0.00319364                     0.00000000                     0.00000000                     0.00047693
31.10.1999                     0.00319328                     0.00000000                     0.00000000                     0.00047657
30.11.1999                     0.00318190                     0.00000000                     0.00000000                     0.00046519
31.12.1999                     0.00316995                     0.00000000                     0.00000000                     0.00045324
31.01.2000                     0.00315404                     0.00000000                     0.00000000                     0.00043733
29.02.2000                     0.00313825                     0.00000000                     0.00000000                     0.00042154
31.03.2000                     0.00312379                     0.00000000                     0.00000000                     0.00040708
30.04.2000                     0.00313375                     0.00000000                     0.00000000                     0.00041704
31.05.2000                     0.00313148                     0.00000000                     0.00000000                     0.00041476
30.06.2000                     0.00312702                     0.00000000                     0.00000000                     0.00041031
31.07.2000                     0.00313853                     0.00000000                     0.00000000                     0.00042182
31.08.2000                     0.00313417                     0.00000000                     0.00000000                     0.00041746
30.09.2000                     0.00313780                     0.00000000                     0.00000000                     0.00042109
31.10.2000                     0.00313438                     0.00000000                     0.00000000                     0.00041767
30.11.2000                     0.00312581                     0.00000000                     0.00000000                     0.00040910
31.12.2000                     0.00311561                     0.00000000                     0.00000000                     0.00039890
31.01.2001                     0.00309979                     0.00000000                     0.00000000                     0.00038308
28.02.2001                     0.00308539                     0.00000000                     0.00000000                     0.00036868
31.03.2001                     0.00307236                     0.00000000                     0.00000000                     0.00035565
30.04.2001                     0.00306131                     0.00000000                     0.00000000                     0.00034460
31.05.2001                     0.00306358                     0.00000000                     0.00000000                     0.00034687
30.06.2001                     0.00309053                     0.00000000                     0.00000000                     0.00037382
31.07.2001                     0.00310235                     0.00000000                     0.00000000                     0.00038564
31.08.2001                     0.00311226                     0.00000000                     0.00000000                     0.00039555
30.09.2001                     0.00312404                     0.00000000                     0.00000000                     0.00040733
31.10.2001                     0.00311793                     0.00000000                     0.00000000                     0.00040122
30.11.2001                     0.00311289                     0.00000000                     0.00000000                     0.00039618
31.12.2001                     0.00309962                     0.00000000                     0.00000000                     0.00038291
31.01.2002                     0.00308364                     0.00000000                     0.00000000                     0.00036693
28.02.2002                     0.00307118                     0.00000000                     0.00000000                     0.00035447
31.03.2002                     0.00305852                     0.00000000                     0.00000000                     0.00034181
30.04.2002                     0.00305509                     0.00000000                     0.00000000                     0.00033838
31.05.2002                     0.00305323                     0.00000000                     0.00000000                     0.00033652
30.06.2002                     0.00306537                     0.00000000                     0.00000000                     0.00034866
31.07.2002                     0.00307907                     0.00000000                     0.00000000                     0.00036236
31.08.2002                     0.00308653                     0.00000000                     0.00000000                     0.00036982
30.09.2002                     0.00309576                     0.00000000                     0.00000000                     0.00037905
31.10.2002                     0.00309478                     0.00000000                     0.00000000                     0.00037807
30.11.2002                     0.00308740                     0.00000000                     0.00000000                     0.00037069
31.12.2002                     0.00307382                     0.00000000                     0.00000000                     0.00035711
31.01.2003                     0.00305823                     0.00000000                     0.00000000                     0.00034152
28.02.2003                     0.00304583                     0.00000000                     0.00000000                     0.00032912
31.03.2003                     0.00303315                     0.00000000                     0.00000000                     0.00031644
30.04.2003                     0.00302975                     0.00000000                     0.00000000                     0.00031304
31.05.2003                     0.00302805                     0.00000000                     0.00000000                     0.00031134
30.06.2003                     0.00304024                     0.00000000                     0.00000000                     0.00032353
31.07.2003                     0.00305399                     0.00000000                     0.00000000                     0.00033728
31.08.2003                     0.00306161                     0.00000000                     0.00000000                     0.00034490
30.09.2003                     0.00307056                     0.00000000                     0.00000000                     0.00035385
31.10.2003                     0.00306947                     0.00000000                     0.00000000                     0.00035276
30.11.2003                     0.00306203                     0.00000000                     0.00000000                     0.00034532
31.12.2003                     0.00304941                     0.00000000                     0.00000000                     0.00033270
31.01.2004                     0.00303852                     0.00000000                     0.00000000                     0.00032181
29.02.2004                     0.00302390                     0.00000000                     0.00000000                     0.00030719
31.03.2004                     0.00301053                     0.00000000                     0.00000000                     0.00029382
30.04.2004                     0.00300531                     0.00000000                     0.00000000                     0.00028860
31.05.2004                     0.00300848                     0.00000000                     0.00000000                     0.00029177
30.06.2004                     0.00302579                     0.00000000                     0.00000000                     0.00030908
31.07.2004                     0.00303158                     0.00000000                     0.00000000                     0.00031487
31.08.2004                     0.00304183                     0.00000000                     0.00000000                     0.00032512
30.09.2004                     0.00303873                     0.00000000                     0.00000000                     0.00032202
31.10.2004                     0.00303589                     0.00000000                     0.00000000                     0.00031918
30.11.2004                     0.00303263                     0.00000000                     0.00000000                     0.00031592
31.12.2004                     0.00302032                     0.00000000                     0.00000000                     0.00030361
31.01.2005                     0.00300549                     0.00000000                     0.00000000                     0.00028878
28.02.2005                     0.00299323                     0.00000000                     0.00000000                     0.00027652
31.03.2005                     0.00298347                     0.00000000                     0.00000000                     0.00026676
30.04.2005                     0.00298614                     0.00000000                     0.00000000                     0.00026943
31.05.2005                     0.00300672                     0.00000000                     0.00000000                     0.00029001
30.06.2005                     0.00301651                     0.00000000                     0.00000000                     0.00029980
31.07.2005                     0.00301779                     0.00000000                     0.00000000                     0.00030108
31.08.2005                     0.00302608                     0.00000000                     0.00000000                     0.00030936
30.09.2005                     0.00303566                     0.00000000                     0.00000000                     0.00031895
31.10.2005                     0.00304000                     0.00000000                     0.00000000                     0.00032329
30.11.2005                     0.00303668                     0.00000000                     0.00000000                     0.00031997
31.12.2005                     0.00302850                     0.00000000                     0.00000000                     0.00031179
31.01.2006                     0.00301956                     0.00000000                     0.00000000                     0.00030285
28.02.2006                     0.00300814                     0.00000000                     0.00000000                     0.00029143
31.03.2006                     0.00299460                     0.00000000                     0.00000000                     0.00027789
30.04.2006                     0.00299033                     0.00000000                     0.00000000                     0.00027362
31.05.2006                     0.00298642                     0.00000000                     0.00000000                     0.00026971
30.06.2006                     0.00298420                     0.00000000                     0.00000000                     0.00026749
31.07.2006                     0.00299232                     0.00000000                     0.00000000                     0.00027561
31.08.2006                     0.00301203                     0.00000000                     0.00000000                     0.00029532
30.09.2006                     0.00301368                     0.00000000                     0.00000000                     0.00029697
31.10.2006                     0.00300948                     0.00000000                     0.00000000                     0.00029277
30.11.2006                     0.00300051                     0.00000000                     0.00000000                     0.00028380
31.12.2006                     0.00298689                     0.00000000                     0.00000000                     0.00027018
31.01.2007                     0.00297297                     0.00000000                     0.00000000                     0.00025626
28.02.2007                     0.00295945                     0.00000000                     0.00000000                     0.00024274
31.03.2007                     0.00294587                     0.00000000                     0.00000000                     0.00022916
30.04.2007                     0.00294208                     0.00000000                     0.00000000                     0.00022537
31.05.2007                     0.00294462                     0.00000000                     0.00000000                     0.00022791
30.06.2007                     0.00294960                     0.00000000                     0.00000000                     0.00023289
31.07.2007                     0.00297182                     0.00000000                     0.00000000                     0.00025511
31.08.2007                     0.00298799                     0.00000000                     0.00000000                     0.00027128
30.09.2007                     0.00299583                     0.00000000                     0.00000000                     0.00027912
31.10.2007                     0.00299609                     0.00000000                     0.00000000                     0.00027938
30.11.2007                     0.00298562                     0.00000000                     0.00000000                     0.00026891
31.12.2007                     0.00297441                     0.00000000                     0.00000000                     0.00025770
