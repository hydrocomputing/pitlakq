from pitlakq.postprocessing import depth_time_contour

species=[{"name": "t2", "levels": "auto"}]
depth_time_contour.main("../output/w2/out.nc", species=species, location=5)
