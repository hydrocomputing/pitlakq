import os
from pitlakq.preprocessing import check_kb_balance

# Get current working directory.
base = os.getcwd()
# Build absolute path.
in_file_name = os.path.join(base, "Collie_River.txt")
# Specify project name.
project_name  = "pitlakq_tut"
target_ph = 4.3
# Start charging.
check_kb_balance.main(project_name,in_file_name, target_ph)
