import datetime
import os

from pitlakq.preprocessing import charge_tributaries

# Get current working directory.
base = os.getcwd()
# Build absolute path.
in_file_name = os.path.join(base, 'sample_concentration.csv')
# Start charging.
charge_tributaries.main(project_name='pitlakq_tut',
                        in_file_name=in_file_name,
                        out_dir=base,
                        charging_species='Cl',
                        last_date=datetime.datetime(2012, 1, 5))
