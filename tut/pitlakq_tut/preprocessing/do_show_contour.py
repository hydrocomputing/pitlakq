from pitlakq.preprocessing.create_bathgrid import show_contours

show_contours(project_name='pitlakq_tut',
              bath_data_file_name='bath_data.txt',
              num_x=100, num_y=51, show_plot=True)

