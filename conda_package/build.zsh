#!/bin/zsh

export PITLAKQ_VERSION=$(python -m hatch version)
echo $PITLAKQ_VERSION
conda build --python=3.10 --numpy=1.24 --user=hydrocomputing pitlakq
