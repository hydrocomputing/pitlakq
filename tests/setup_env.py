
import os
import subprocess

def create_dot_pitlakq(base=os.getcwd(), dst='.pitlakq'):
    indent = ' '  * 4
    with open(dst, 'w') as fobj:
        for name, path in [('model_path', 'models'),
                           ('ram_path', 'RAM'),
                           ('resources_path', '../../pitlakq/resources'),
                           ('template_path', '../../pitlakq/templates')]:
            fobj.write('{0}:\n'.format(name))
            fobj.write('{0}{1}\n'.format(indent, os.path.join(base, path)))

def set_env():
    cmd = 'set PITLAKQHOME={0}\n'.format(os.getcwd())
    dirn = os.path.dirname
    cmd += 'set PYTHONPATH={0}\n'.format(dirn(dirn((os.getcwd()))))
    fname = 'set_env.bat'
    with open(fname, 'w') as fobj:
        fobj.write(cmd)

if __name__ == '__main__':
    create_dot_pitlakq()
    set_env()

