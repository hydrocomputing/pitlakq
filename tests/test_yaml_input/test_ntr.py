"""Test consistent number of tributaries.
"""

import pytest

import os
import pitlakq.commontools.input.yamlinput as yamlinput
from pitlakq.numericalmodels.existingmodels.w2 import w2

template_path = os.path.abspath('../../pitlakq/templates/w2.yaml')


def test_ntr_tribs_2():
    yinput = yamlinput.YamlInput()
    input_path = os.path.abspath('w2_tribs_2.yaml')
    yinput.load(template_path, input_path)
    w2.W2._check_number_of_tributaries(yinput)
    print(yinput.data['tributaries'])
    assert yinput.data['bounds']['number_of_tributaries']['value'] == 2


def test_ntr_ok():
    yinput = yamlinput.YamlInput()
    input_path = os.path.abspath('w2.yaml')
    yinput.load(template_path, input_path)
    w2.W2._check_number_of_tributaries(yinput)


def test_ntr_bounds_2():
    yinput = yamlinput.YamlInput()
    input_path = os.path.abspath('w2_bounds_2.yaml')
    yinput.load(template_path, input_path)
    with pytest.raises(ValueError) as err:
        w2.W2._check_number_of_tributaries(yinput)



