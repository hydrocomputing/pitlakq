"""Test the date converter.
"""

import datetime

import mx.DateTime as mx_dt

import pitlakq.commontools.timing.timeconverter as converter


def test_to_datetime():
    """Check if dates get converted in both directions.
    """
    mx_d = mx_dt.DateTime(2002, 3, 4, 2, 1, 30.05)
    dt_d = datetime.datetime(2002, 3, 4, 2, 1, 30, 50000)
    assert converter.mx_to_datetime(mx_d) == dt_d
    assert converter.datetime_to_mx(dt_d) == mx_d

