"""Test the conversion from mixedCase to under_score.
"""

import itertools
import os

from pitlakq.preprocessing.input_converter import InputConverter


def test_make_underscore():
    """Test the conversion from mixedCase to under_Score.
    """
    make_underscore = InputConverter.make_underscore
    assert make_underscore('mixedCase') == 'mixed_case'
    assert make_underscore('mixedCAse') == 'mixed_case'
    assert make_underscore('mixedCASe') == 'mixed_case'
    assert make_underscore('mixedCASE') == 'mixed_case'
    assert make_underscore('mixedCaSe') == 'mixed_ca_se'
    assert make_underscore('mixedCase1D') == 'mixed_case_1d'
    assert make_underscore('mixed1DCase') == 'mixed_1d_case'
    assert make_underscore('w2Cells') == 'w2_cells'



class TestInputConverter(object):
    """Test the input conversion.
    """

    def setup_class(self):
        """Setup the test.
        """
        # This acts like an __init__.
        # pylint: disable-msg=W0201
        self.converter = InputConverter()
        self.base = os.path.dirname(__file__)

    def test_yaml(self):
        """YAML files.
        """
        for name in ['w2const', 'w2']:
            in_file = os.path.join(self.base, '%s_mixed.yaml' % name)
            out_file = os.path.join(self.base, '%s_converted.yaml' % name)
            check_file = os.path.join(self.base, '%s.yaml' % name)
            self.converter.convert_yaml(in_file, out_file)
            print in_file, out_file
            print name
            for out_line, check_line in itertools.izip(open(out_file),
                                                       open(check_file)):
                check_line = check_line.replace('aluminium:', 'aluminum:')
                assert out_line == check_line



    def test_txt(self):
        """Text files.
        """
        for name in ['activeconst', 'meteorology', 'precipitation']:
            in_file = os.path.join(self.base, '%s_mixed.txt' % name)
            out_file = os.path.join(self.base, '%s_converted.txt' % name)
            check_file = os.path.join(self.base, '%s.txt' % name)
            self.converter.convert_txt(in_file, out_file)
            print name
            for out_line, check_line in itertools.izip(open(out_file),
                                                       open(check_file)):
                assert out_line == check_line

    def test_bath(self):
        """Test bathymetry in netCDF file.
        """
        in_file = os.path.join(self.base, 'bath_mixed.nc')
        out_file = os.path.join(self.base, 'bath_converted.nc')
        check_file = os.path.join(self.base, 'bath.nc')
        self.converter.convert_bath(in_file, out_file)
        assert open(out_file, 'rb').read() == open(check_file, 'rb').read()
