"""Testing of input.
"""

from openpyxl.reader.excel import load_workbook
import pytest

from pitlakq.submodels.loading.loading import Loading

from conftest import make_abs_path


@pytest.fixture
def xlsx_file_name():
    return make_abs_path(__file__, 'loading.xlsx')


def make_loading(xlsx_file_name):
    loading = Loading.__new__(Loading)
    loading.workbook = load_workbook(filename=xlsx_file_name)
    loading.xlsx_file_name = xlsx_file_name
    return loading

def test_area(xlsx_file_name):
    loading = make_loading(xlsx_file_name)
    area = loading.read_area()
    assert area['water_level'][0] == 152
    assert area['zone1_start_segment'][4] == 4

def test_conc(xlsx_file_name):
    loading = make_loading(xlsx_file_name)
    conc = loading.read_conc()
    assert conc['zone'][0] == 'zone1'
    assert conc['silver'][1] == 3e-11


