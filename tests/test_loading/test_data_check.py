"""Testing correct checking of input data.
"""

import random
import textwrap

import pytest

from pitlakq.submodels.loading.loading import Loading


def test_check_levels_correct():
    loading = Loading.__new__(Loading)
    loading.area = {'water_level': [10, 20, 30]}
    loading.check_levels()


def test_check_levels_not_ordered():
    loading = Loading.__new__(Loading)
    loading.area = {'water_level': [20, 10, 30]}
    with pytest.raises(ValueError) as excinfo:
                loading.check_levels()
    msg = 'Please sort water level values in acsending order.'
    assert excinfo.value.message == msg


def test_check_levels_too_close():
    loading = Loading.__new__(Loading)
    loading.area = {'water_level': [10, 20, 20, 25, 25.01, 30, 30.05]}
    with pytest.raises(ValueError) as excinfo:
        loading.check_levels()
    msg = textwrap.dedent("""
    There must be a minium difference between specified lake water levels of 0.1 m.
    The following levels are too close:
    -  20.000 and  20.000
    -  25.000 and  25.010
    -  30.000 and  30.050""")[1:]
    assert excinfo.value.message == msg


def test_areas():
    loading = Loading.__new__(Loading)
    loading.area = {'zone1_planar': [100.5, -12, -0.4]}
    with pytest.raises(ValueError) as excinfo:
        loading.check_areas()
    msg = textwrap.dedent("""
        Found negative values in area zone1_planar.
        Please provide non-negative values for:
        -     -12
        -    -0.4""")[1:]
    assert excinfo.value.message == msg


def test_segments():
    loading = Loading.__new__(Loading)
    loading.area = {'zone1_start_segment': [2, 5, 8, 5],
                    'zone1_end_segment': [3, 4, 8, 2]}
    with pytest.raises(ValueError) as excinfo:
        loading.check_segments()
    msg = textwrap.dedent("""
        Found invalid segment definitions for zone "zone1".
        Make sure start segments are lower than or equal to end segments.
        The following segments are incorrect:
        line:   2 start:   5 end:   4
        line:   3 start:   5 end:   2""")[1:]
    assert excinfo.value.message == msg




