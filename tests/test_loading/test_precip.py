"""Testing of reading of precipitation values.
"""

import os
import random

import pytest
from pitlakq.submodels.loading.loading import Loading


from conftest import make_abs_path


@pytest.fixture
def precip_file_name():
    return make_abs_path(__file__, 'pre_br1.npt')


@pytest.fixture
def long_step_precip_file_name():
    return make_abs_path(__file__, 'pre_br1_monthly.npt')

TOTAL = 8.911288728
CONST_RATE = 0.000000019
LONG_STEP_TOTAL = CONST_RATE * 86400 * 1127

def get_precip(precip_file_name):
    """Helper to create coroutine and advance it.
    """
    load = Loading.__new__(Loading)
    precip = load.get_precipitation(open(precip_file_name))
    next(precip)
    return precip


def test_precip_total(precip_file_name, total=TOTAL):
    """Test the total amount of precipitation.
    """

    for factor in [0.01, 0.1, 1, 10, 100, 1000, 10000, 100000000000]:
        precip = get_precip(precip_file_name)
        precip_sum = 0
        jday = 0
        while True:
            jday += factor * random.random()
            try:
                precip_sum += precip.send(jday)
            except StopIteration:
                break
        #print '%18.2f' % factor, precip_sum, total
        assert abs(precip_sum - total) < 1e-1


def test_precip_interval(precip_file_name):
    """Numbers calculated with LibreOffice spreadsheet.
    """
    precip = get_precip(precip_file_name)
    assert abs(precip.send(3.5) - 0.00979992) < 1e-6
    assert precip.send(3.6) == 0.0
    assert abs(precip.send(3.8) - 0.000200016) < 1e-6
    assert precip.send(3.801) == 0.0


def test_raise_wrong_jday(precip_file_name):
    """Using the same jday twice or an older jday will raise an ValueError.
    """
    precip = get_precip(precip_file_name)
    precip.send(3.5)
    pytest.raises(ValueError, lambda: precip.send(3.5))
    precip = get_precip(precip_file_name)
    precip.send(3.5)
    pytest.raises(ValueError, lambda: precip.send(3.49))


def test_long_step_single(long_step_precip_file_name):
    """Check that long precipitation time steps work..
    """
    precip = get_precip(long_step_precip_file_name)
    jdays = [0, 1, 2, 3, 5, 7.5, 30, 68, 100, 200, 500, 1000, 1127]
    jdays = [0,  1127]
    for start, end in zip(jdays[:-1], jdays[1:]):
        ammount_got = precip.send(end)
        ammount_expected = (end - start) * 86400 * CONST_RATE
        #print end, ammount_got, ammount_expected
        assert abs(ammount_got - ammount_expected) < 1e-12


def test_long_step_sum(long_step_precip_file_name):
    """Check that long precipitation time steps work.
    """
    test_precip_total(long_step_precip_file_name, LONG_STEP_TOTAL)

