"""Test calculation of areas, masses etc.
"""


import textwrap

import pytest

from pitlakq.submodels.loading.loading import Loading

@pytest.fixture
def loading():
    class Config:
        def __init__(self):
            self.loading_file = 'loading.xlsx'
    loading_inst = Loading.__new__(Loading)
    loading_inst.config = Config()
    return loading_inst

def test_find_pos():
    water_levels = list(range(10, 101, 10))
    water_level = 50
    loading = Loading.__new__(Loading)
    assert loading._find_level_pos(water_level, water_levels) == (4, 5)


def test_pos_too_high(loading):
    water_levels = list(range(10, 101, 10))
    water_level = 150
    with pytest.raises(ValueError) as excinfo:
        loading._find_level_pos(water_level, water_levels)
    msg = textwrap.dedent("""
        Water level above highest specifed water level.
        The water level is 150 but highest specified water level is 100.
        Please correct {0}.
        """.format(loading.config.loading_file))
    assert excinfo.value.message == msg


def test_pos_too_low(loading):
    water_levels = list(range(10, 101, 10))
    water_level = 3
    with pytest.raises(ValueError) as excinfo:
        loading._find_level_pos(water_level, water_levels)
    msg = textwrap.dedent("""
        Water level below lowest specifed water level.
        The water level is 3 but lowest specified water level is 10.
        Please correct {0}.
        """.format(loading.config.loading_file))
    assert excinfo.value.message == msg

