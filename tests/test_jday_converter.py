"""Test the conversion of jdays to dates and back.
"""

import datetime

import pitlakq.numericalmodels.existingmodels.w2.jday_converter as jday_conv


class TestJdayConverter(object):
    """Test the jday conversion.
    """

    def setup_class(self):
        """Setup the test.
        """
        # This acts like an __init__.
        # pylint: disable-msg=W0201
        start = datetime.datetime(1991, 3, 15, 12, 45, 7)
        end = datetime.datetime(2007, 8, 21, 23, 7, 32)
        self.conv = jday_conv.JdayConverter(start)
        self.conv_complex = jday_conv.JdayConverterComplex(start, end)
        self.conv_complex.make_cumulative_days_in_years()

    def test_date2jday(self):
        """Date2Jday.
        """
        dates = [datetime.datetime(2001, 6, 2, 20, 17, 3, 500),
                 datetime.datetime(2003, 1, 22),
                 datetime.datetime(1998, 12, 17, 12, 30)]
        for date in dates:
            assert (self.conv_complex.make_jday_from_date(date) ==
                    self.conv.make_jday_from_date(date))

    def test_jday2date(self):
        """JDay2Date.
        """
        jdays = [10, 20.567, 300.5, 900.123345, 3000.5678]
        for jday in jdays:
            assert (self.conv_complex.make_date_from_jday(jday) ==
                    self.conv.make_date_from_jday(jday))
