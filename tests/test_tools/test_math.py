"""Test some mathematical functions.
"""

from pitlakq.commontools.tools import interpolate


def test_interpolate():
    for x_0, x_1, y_0, y_1, x_value, y_value in [
        (10, 20, 90, 100, 15, 95),
        (-20, -10, -100, -90, -15, -95),
        (-10, 10, -20, 20, 0, 0)]:
        assert interpolate(x_0, x_1, y_0, y_1, x_value) == y_value