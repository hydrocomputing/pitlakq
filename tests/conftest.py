
import os

import pytest

def make_abs_path(module_name, file_name):
    """Helper to make absolute path name.
    """
    own_dir = os.path.abspath(os.path.dirname(module_name))
    return os.path.join(own_dir, file_name)