import os

from pitlakq.metamodel.configuration.getconfig import get_dot_pitlakq_path

from conftest import make_abs_path

def test_get_dot_pitlakq_path_home():
    """Test use of .pitlakq from home directory.
    Assumes ~/.pitlakq exists.
    """
    path = get_dot_pitlakq_path(verbose=True)
    assert path == os.path.join(os.path.expanduser('~'), '.pitlakq')


def test_get_dot_pitlakq_path_no_pitlakqhome():
    """Test if PITLAKQHOME is set but has no .pitlakq inside it defaults
    to .piztakq in HOME.
    Assumes ~/.pitlakq exists.
    """
    os.environ['PITLAKQHOME'] = 'dummy'
    path = get_dot_pitlakq_path(verbose=True)
    assert path == os.path.join(os.path.expanduser('~'), '.pitlakq')


def test_get_dot_pitlakq_path_pitlakqhome():
    """Test if PITLAKQHOME is set and has .pitlakq inside, it must be used.
    """
    mock = make_abs_path(__file__, 'mock_pitlake_home')
    os.environ['PITLAKQHOME'] = mock
    print os.getenv('PITLAKQHOME')
    print os.getcwd()
    path = get_dot_pitlakq_path(verbose=True)
    assert path == os.path.join(os.getenv('PITLAKQHOME'), '.pitlakq')