subroutine update_kb() 
        use shared_data
        implicit none
        integer :: kb_new = 0
!
!

if (all(kb == kb_real)) then
    write(*,*) 'kb is at end position'
    return
end if

!**** bottom layers
      kbmax = 0.0
      do jb=1,nbp
        do i=us(jb)-1,ds(jb)+1
          if (kb(i) > kb_real(i)) then
              kb_new = max(kb_real(i), kti(i) + 2)
              kb(i) = (max(kb_new, kb(i) - 1)) !allow only change of 1
          end if
          kbmax = max(kbmax,kb(i))
        end do
        kb(us(jb)-1) = kb(us(jb))
        kb(ds(jb)+1) = kb(ds(jb))
      end do
end subroutine update_kb    