subroutine    close_files
     use shared_data
     implicit none
     integer :: ioerr
     
       if (time_series)  close (tsr)
       if (vector)       close (vpl)
       if (profile)      close (prf)
       if (spreadsheet)  close (spr)
       if (contour)      close (cpl)
       if (.not.warning_open) then
         close (wrn,status='delete')
       else
         close (wrn)
       end if
       close (err,status='delete')
       close(met)
       if (withdrawals) then
         close (wdq)
       end if
       if (tributaries) then
         do jt=1,ntr
           close (trq(jt), iostat=ioerr)
           if (ioerr /= 0) then
               write(*,*) ioerr
           end if
           close (trt(jt))
           if (constituents) then
             close (tribconc(jt))
           end if
         end do
       end if
       do jb=1,nbp
         if (up_flow(jb)) then
            close (inq(jb))
           close (intemp(jb))
           if (constituents) then
             close (inc(jb))
           end if
         end if
         if (dn_flow(jb)) then
           close (otq(jb))
         end if
         if (precipitation) then
           close (pre(jb))
           close (prt(jb))
           if (constituents) then
              close(prc(jb))
           end if
         end if
         if (dist_tribs(jb)) then
           close (dtq(jb))
           close (dtt(jb))
           if (constituents) then
             close (dtc(jb))
           end if
         end if
         if (uh_external(jb)) then
           close (uhe(jb))
           close (uht(jb))
           if (constituents) then
             close (uhc(jb))
           end if
         end if
         if (dh_external(jb)) then
           close (dhe(jb))
           close (dht(jb))
           if (constituents) then
             close (dhc(jb))
           end if
         end if 
       end do
       
       end subroutine close_files