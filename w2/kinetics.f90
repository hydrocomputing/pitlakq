!************************************************************************
!**                         k i n e t i c s                            **
!************************************************************************


!***********************************************************************
!*                   r a t e   m u l t i p l i e r s                  **
!***********************************************************************

subroutine rate_multipliers
        use shared_data
        use shared_data_kinetics
        implicit none
        double precision :: fr, tt2, tt1, tt, ff, tt4, tt3
        double precision :: k3, k4
        double precision :: lam1, lam2

        !****** rising and falling temperature rate functions

        fr(tt,tt1,tt2,k1,k2) = k1*exp(log(k2*(1.0-k1)/(k1*(1.0-k2)))    &
     &                         /(tt2-tt1)*(tt-tt1))
        ff(tt,tt3,tt4,k3,k4) = k4*exp(log(k3*(1.0-k4)/(k4*(1.0-k3)))    &
     &                         /(tt4-tt3)*(tt4-tt))

        do i=iu,id
          do k=kt,kb(i)
            lam1       = fr(t1(k,i),nh4t1,nh4t2,nh4k1,nh4k2)
            nh4rm(k,i) = lam1/(1.0+lam1-nh4k1)
            lam1       = fr(t1(k,i),no3t1,no3t2,no3k1,no3k2)
            no3rm(k,i) = lam1/(1.0+lam1-no3k1)
            lam1       = fr(t1(k,i),omt1,omt2,omk1,omk2)
            omrm(k,i)  = lam1/(1.0+lam1-omk1)
            lam1       = fr(t1(k,i),at1,at2,ak1,ak2)
            lam2       = ff(t1(k,i),at3,at4,ak3,ak4)
            armr(k,i)  = lam1/(1.0+lam1-ak1)
            armf(k,i)  = lam2/(1.0+lam2-ak4)
          end do
        end do
      end subroutine

!***********************************************************************
!*                   d e c a y   c o n s t a n t s                    **
!***********************************************************************

      subroutine decay_constants
         use shared_data
         implicit none

        do i=iu,id
          do k=kt,kb(i)
            a1(k,i)    = (1.0d0+sign(1.0d0,dox(k,i)-o2lim))*0.5d0
            a2(k,i)    = (1.0d0+sign(1.0d0,o2lim-dox(k,i)))*0.5d0
            a3(k,i)    = (1.0d0+sign(1.0d0,dox(k,i)-1.0d-10))*0.5d0
            domd(k,i)  = omrm(k,i)*(ldomdk*ldom(k,i)+rdomdk                &
     &                   *rdom(k,i))*a3(k,i)
            nh4d(k,i)  = nh4dk*nh4rm(k,i)*nh4(k,i)*a1(k,i)
            no3d(k,i)  = no3dk*no3rm(k,i)*no3(k,i)*a2(k,i)
            cbodd(k,i) = kbod*tbod**(t1(k,i)-20.0d0)*a3(k,i)
            lpomd(k,i) = lpomdk*omrm(k,i)*lpom(k,i)*a3(k,i)
            sedd(k,i)  = sdk*omrm(k,i)*sed(k,i)*a3(k,i)
          end do
        end do
! changed fe to feoh3 and fes to 0.0 for p precipitation
! mmueller
        do i=iu,id
          fpss(kt,i)   = partp*ss(kt,i)/(partp*(ss(kt,i)+feoh3(kt,i))+1.0)
          fpfe(kt,i)   = partp*feoh3(kt,i)/(partp*(ss(kt,i)+feoh3(kt,i))+1.0)
          setout(kt,i) = (sss*fpss(kt,i)+0.0*fpfe(kt,i))/hkt2(i)*a1(kt,i)
          do k=kt+1,kb(i)
            fpss(k,i)   = partp*ss(k,i)/(partp*(ss(k,i)+feoh3(k,i))+1.0)
            fpfe(k,i)   = partp*feoh3(k,i)/(partp*(ss(k,i)+feoh3(k,i))+1.0)
            setin(k,i)  = setout(k-1,i)
            setout(k,i) = (sss*fpss(k,i)+0.0*fpfe(k,i))/h(k)*a1(k,i)
          end do
        end do
        do i=iu,id
          sodd(kt,i) = sod(i)/bhkt2(i)*omrm(kt,i)*(b(kti(i),i)-b(kt+1,i))
          do k=kt+1,kb(i)-1
            sodd(k,i) = sod(i)/bh(k,i)*omrm(k,i)*(b(k,i)-b(k+1,i))
          end do
          sodd(kb(i),i) = sod(i)/bh(kb(i),i)*omrm(kb(i),i)*b(kb(i),i)
        end do
      end subroutine

!***********************************************************************
!*                  s u s p e n d e d   s o l i d s                   **
!***********************************************************************

      subroutine suspended_solids
        use shared_data
        implicit none

        do i=iu,id
          ssss(kt,i) = -sss*ss(kt,i)/hkt2(i)
          do k=kt+1,kb(i)
            ssss(k,i) = sss*(ss(k-1,i)-ss(k,i))/h(k)
          end do
        end do
        do i=iu,id
          k=kb(i)
          csssed(2,jb) = csssed(2,jb)+sss*ss(k,i)/h(k)*dlt
          do k=kt,kb(i)
            cssw2(2,jb) = cssw2(2,jb)+ssss(k,i)*dlt
          end do
        end do
      end subroutine

!***********************************************************************
!*                           c o l i f o r m                          **
!***********************************************************************

      subroutine coliform
        use shared_data
        implicit none

        do i=iu,id
          do k=kt,kb(i)
            colss(k,i) = -coldk*colq10**(t1(k,i)-20.0)*col(k,i)
            cssw2(3,jb) = cssw2(3,jb)+colss(k,i)*dlt
          end do
        end do
        end subroutine

!***********************************************************************
!*                        l a b i l e   d o m                         **
!***********************************************************************

      subroutine labile_dom
        use shared_data
        use shared_data_kinetics
        implicit none
        double precision :: decay

        do i=iu,id
          do k=kt,kb(i)
            decay       = omrm(k,i)*a3(k,i)*(ldomdk+lrddk)*ldom(k,i)
            apr         = (aer(k,i)+(1.0-apom)*amr(k,i))*algae(k,i)
            ldomss(k,i) = apr-decay
            cssw2(5,jb) = cssw2(5,jb)+ldomss(k,i)*dlt
          end do
        end do
      end subroutine

!***********************************************************************
!*                     r e f r a c t o r y   d o m                    **
!***********************************************************************

      subroutine refractory_dom
        use shared_data
        implicit none

        do i=iu,id
          do k=kt,kb(i)
            rdomss(k,i) = omrm(k,i)*(lrddk*ldom(k,i)-rdomdk                &
     &                    *rdom(k,i))*a3(k,i)
            cssw2(6,jb) = cssw2(6,jb)+rdomss(k,i)*dlt
          end do
        end do
      end subroutine

!***********************************************************************
!*                      p h y t o p l a n k t o n                     **
!***********************************************************************

      subroutine phytoplankton
        use shared_data
        use shared_data_kinetics
        implicit none
        double precision :: ltcoef, hourOfDay
        double precision :: lam1, lam2
        double precision :: llim, nlim, limit, ticlim    !mueller tic limiting
        double precision :: fdpo4, plim, growth
        character lf*3

        ltcoef = (1.0-beta)*sro*4.186e6/asat
        do i=iu,id

!******** limiting factor

          gamma = exh2o+exss*ss(kt,i)+exom*(algae(kt,i)+lpom(kt,i))
          lam1  = ltcoef
          lam2  = ltcoef*exp(-gamma*depthm(kt))
          if (noPhoto) then
            llim = 1.0d-30
          else
            llim  = 2.718282*(exp(-lam2)-exp(-lam1))/(gamma*hkt2(i))
          end if
          fdpo4 = 1.0-fpss(kt,i)-fpfe(kt,i)
          plim  = fdpo4*po4(kt,i)/(fdpo4*po4(kt,i)+ahsp)
          nlim  = (nh4(kt,i)+no3(kt,i))/(nh4(kt,i)+no3(kt,i)+ahsn)
!<mueller>
!    <date>2001/02/06></date>
!    <reason>carbon limite photosynthesis</reason>
          ticlim= tic(kt,i)/(tic(kt,i) + ahstic)
            hourOfDay     = (jday-jdayg)*24.0d0

!</mueller>
          limit = min(plim,nlim,llim,ticlim) !mueller ticlim
          if (.not. limitingCauseWritten .and. hourOfDay > 12.0) then
            if (limit.eq.plim) then
                limitingCause(kt,i) = 1
                limitingFactor(kt,i) = plim
                write (lfac(kt,i),'(f4.3)') plim
                lf         = ' p '
                lfpr(kt,i) = lf//lfac(kt,i)
            else if (limit.eq.nlim) then
                limitingCause(kt,i) = 2
                limitingFactor(kt,i) = nlim
                write (lfac(kt,i),'(f4.3)') nlim
			    lf		   = ' n '
			    lfpr(kt,i) = lf//lfac(kt,i)
		    else if (limit.eq.llim) then
			    limitingCause(kt,i) = 3
			    limitingFactor(kt,i) = llim
			    write (lfac(kt,i),'(f4.3)') llim
			    lf		   = ' l '
			    lfpr(kt,i) = lf//lfac(kt,i)

!<mueller>
!	 <date>2001/02/06></date>
!	 <reason>if carbon is limiting value</reason>
		    else if (limit.eq.ticlim) then
			    limitingCause(kt,i) = 4
			    limitingFactor(kt,i) = ticlim
			    write (lfac(kt,i),'(f4.3)') ticlim
			    lf		   = ' c '
			    lfpr(kt,i) = lf//lfac(kt,i)
!</mueller>
		    end if
          end if
!******** sources/sinks

          arr(kt,i)   = armr(kt,i)*armf(kt,i)*ar*a3(kt,i)
          amr(kt,i)   = (armr(kt,i)+1.0-armf(kt,i))*am
          agr(kt,i)   = armr(kt,i)*armf(kt,i)*ag*limit
          agr(kt,i)   = min(agr(kt,i),po4(kt,i)/(biop*dlt*algae(kt,i)    &
     &                  +nonzero),(nh4(kt,i)+no3(kt,i))/(bion*dlt        &
     &                  *algae(kt,i)+nonzero))
          aer(kt,i)   = min((1.0-llim)*ae,agr(kt,i))
          growth      = (agr(kt,i)-arr(kt,i)-aer(kt,i)-amr(kt,i))        &
     &                  *algae(kt,i)
          nets        = -as*algae(kt,i)/hkt2(i)
          algss(kt,i) = growth+nets

!<mueller>
!    <date>2001/02/06></date>
!    <reason>algae due to growth (and decay)
!            no settling
!    </reason>
        if (phreeqc_coupling) then
            algaenosettlingss(kt,i) = growth    !
            algaewithsettlingss(kt,i) = nets
        end if
!</mueller>
          do k=kt+1,kb(i)

!********** limiting factor

            gamma = exh2o+exss*ss(k,i)+exom*(algae(k,i)+lpom(k,i))
            lam1  = ltcoef*exp(-gamma*depthm(k))
            lam2  = ltcoef*exp(-gamma*depthm(k+1))
            llim  = 2.718282*(exp(-lam2)-exp(-lam1))/(gamma*h(k))
            fdpo4 = 1.0-fpss(k,i)-fpfe(k,i)
            plim  = fdpo4*po4(k,i)/(fdpo4*po4(k,i)+ahsp)
            nlim  = (nh4(k,i)+no3(k,i))/(nh4(k,i)+no3(k,i)+ahsn)
!<mueller>
!    <date>2001/02/06></date>
!    <reason>carbon limite photosynthesis</reason>
            ticlim= tic(k,i)/(tic(k,i) + ahstic)
!</mueller>
            limit = min(plim,nlim,llim,ticlim) !mueller ticlim

            if (.not. limitingCauseWritten .and. hourOfDay > 12.0) then
               if (limit.eq.plim) then
                limitingCause(k,i) = 1
                limitingFactor(k,i) = plim
                write (lfac(k,i),'(f4.3)') plim
                lf        = ' p '
                lfpr(k,i) = lf//lfac(k,i)
              else if (limit.eq.nlim) then
                limitingCause(k,i) = 2
                limitingFactor(k,i) = nlim
                write (lfac(k,i),'(f4.3)') nlim
                 lf        = ' n '
                lfpr(k,i) = lf//lfac(k,i)
             else if (limit.eq.llim) then
                limitingCause(k,i) = 3
                limitingFactor(k,i) = llim
                write (lfac(k,i),'(f4.3)') llim
                lf        = ' l '
                lfpr(k,i) = lf//lfac(k,i)
!<mueller>
!    <date>2001/02/06></date>
!    <reason>if carbon is limiting take its value</reason>
              else if (limit.eq.ticlim) then
                limitingCause(k,i) = 4
                limitingFactor(k,i) = ticlim
                write (lfac(kt,i),'(f4.3)') ticlim
                lf         = ' c '
                lfpr(kt,i) = lf//lfac(kt,i)
!</mueller>
                end if
            end if

!********** sources/sinks

            arr(k,i)   = armr(k,i)*armf(k,i)*ar*a3(k,i)
            amr(k,i)   = (armr(k,i)+1.0-armf(k,i))*am
            agr(k,i)   = armr(k,i)*armf(k,i)*ag*limit
            agr(k,i)   = min(agr(k,i),po4(k,i)/(biop*dlt*algae(k,i)        &
     &                   +nonzero),(nh4(k,i)+no3(k,i))/(bion*dlt        &
     &                   *algae(k,i)+nonzero))
            aer(k,i)   = min((1.0-llim)*ae,agr(k,i))
            growth     = (agr(k,i)-arr(k,i)-aer(k,i)-amr(k,i))            &
     &                   *algae(k,i)
            nets       = as*(algae(k-1,i)-algae(k,i))/h(k)
            algss(k,i) = growth+nets
!<mueller>
!    <date>2001/02/06></date>
!    <reason>alagae due to growth (and decay)
!            no settling
!    </reason>
        if (phreeqc_coupling) then
            algaenosettlingss(kt,i) = growth    !
            algaewithsettlingss(kt,i) = nets
        end if
!</mueller>
          end do
        end do
        do i=iu,id
          k=kb(i)
          csssed(7,jb) = csssed(7,jb)+as*algae(k,i)/h(k)*dlt
          do k=kt,kb(i)
            cssw2(7,jb) = cssw2(7,jb)+algss(k,i)*dlt
          end do
        end do
    if (.not. limitingCauseWritten .and. hourOfDay > 12.0) then
        limitingCauseWritten = .true.
    else if (limitingCauseWritten .and. hourOfDay < 12.0) then
        limitingCauseWritten = .false.
    end if
      end subroutine

!***********************************************************************
!*                         l a b i l e   p o m                        **
!***********************************************************************

      subroutine labile_pom
        use shared_data
        use shared_data_kinetics
        implicit none

        do i=iu,id
          apr          = apom*amr(kt,i)*algae(kt,i)
          nets         = -poms*lpom(kt,i)/hkt2(i)
          lpomss(kt,i) = apr-lpomd(kt,i)+nets
          do k=kt+1,kb(i)
            apr         = apom*amr(k,i)*algae(k,i)
            nets        = poms*(lpom(k-1,i)-lpom(k,i))/h(k)
            lpomss(k,i) = apr-lpomd(k,i)+nets
          end do
        end do
        do i=iu,id
          k=kb(i)
          csssed(8,jb) = csssed(8,jb)+poms*lpom(k,i)/h(k)*dlt
          do k=kt,kb(i)
            cssw2(8,jb) = cssw2(8,jb)+lpomss(k,i)*dlt
          end do
        end do
      end subroutine

!***********************************************************************
!*                         p h o s p h o r u s                        **
!***********************************************************************

      subroutine phosphorus
        use shared_data
        use shared_data_kinetics
        implicit none

        do i=iu,id
          do k=kt,kb(i)
            apr        = (arr(k,i)-agr(k,i))*algae(k,i)
            po4ss(k,i) = biop*(apr+lpomd(k,i)+domd(k,i)+sedd(k,i))        &
     &                   +po4r*sodd(k,i)*a2(k,i)+setin(k,i)                &
     &                   *po4(k-1,i)-setout(k,i)*po4(k,i)
            cssw2(9,jb) = cssw2(9,jb)+po4ss(k,i)*dlt
          end do
          k=kb(i)
          csssed(9,jb) = csssed(9,jb)+setout(k,i)*po4(k,i)*dlt
        end do
      end subroutine

!***********************************************************************
!*                          a m m o n i u m                           **
!***********************************************************************

      subroutine ammonium
        use shared_data
        use shared_data_kinetics
        implicit none

        do i=iu,id
          do k=kt,kb(i)
            apr        = (arr(k,i)-agr(k,i)*nh4(k,i)/(nh4(k,i)            &
     &                   +no3(k,i)+nonzero))*algae(k,i)
            nh4ss(k,i) = bion*(apr+lpomd(k,i)+domd(k,i)+sedd(k,i))        &
     &                   +nh4r*sodd(k,i)*a2(k,i)-nh4d(k,i)
            cssw2(10,jb) = cssw2(10,jb)+nh4ss(k,i)*dlt
          end do
        end do
      end subroutine

!***********************************************************************
!*                            n i t r a t e                           **
!***********************************************************************

      subroutine nitrate
        use shared_data
        implicit none
        double precision :: algc

        do i=iu,id
          do k=kt,kb(i)
            algc       = bion*(1.0-nh4(k,i)/(nh4(k,i)+                &
     &                     no3(k,i)+nonzero))*agr(k,i)*algae(k,i)
            no3ss(k,i) = nh4d(k,i)-no3d(k,i)-algc
            cssw2(11,jb) = cssw2(11,jb)+no3ss(k,i)*dlt
            cssatm(11,jb) = cssatm(11,jb)+no3d(k,i)*dlt
          end do
        end do
      end subroutine

!***********************************************************************
!*                   d i s s o l v e d   o x y g e n                  **
!***********************************************************************

      subroutine dissolved_oxygen
        use shared_data
        use shared_data_kinetics
        implicit none
        double precision :: sato, x
        double precision :: o2ex, satdo


        !****** dissolved oxygen saturation

        sato(x) = exp(7.7117-1.31403*(log(x+45.93)))*palt

        o2ex = (0.5+0.05*wind*wind)/86400.0
        do i=iu,id
          doss(kt,i) = 0.0d0
          do k=kt,kb(i)
            apr       = (o2ag*agr(k,i)-o2ar*arr(k,i))*algae(k,i)
            doss(k,i) = apr-o2nh4*nh4d(k,i)-o2om*(lpomd(k,i)        &
     &                 +sedd(k,i))-sodd(k,i)*a3(k,i)-o2om*domd(k,i) &
     &                  -cbodd(k,i)*cbod(k,i)*rbod
            cssw2(12,jb) = cssw2(12,jb)+doss(k,i)*dlt
          end do
          satdo = sato(t1(kt,i))
          if (.not.ice(i).and.allowAtmosphericExchange) then
            doss(kt,i) = doss(kt,i)+(satdo-dox(kt,i))*o2ex/hkt2(i)
            cssatm(12,jb) = cssatm(12,jb)+((satdo-dox(kt,i))*o2ex/hkt2(i))*dlt*vactive(kt,i)
          endif
        end do
      end subroutine

!***********************************************************************
!*                           s e d i m e n t                          **
!***********************************************************************

      subroutine sediment
        use shared_data
        implicit none
        double precision :: settle

        do i=iu,id
          settle    = (as*algae(kt,i)+poms*lpom(kt,i))*dlt                &
     &                /hkt2(i)*(1.0-b(kt+1,i)/bkt(i))
          sed(kt,i) = max(sed(kt,i)+settle-sedd(kt,i)*dlt,0.0)
          do k=kt+1,kb(i)-1
            settle   = (as*algae(k,i)+poms*lpom(k,i))*dlt/h(k)            &
     &                 *(1.0-b(k+1,i)/b(k,i))
            sed(k,i) = max(sed(k,i)+settle-sedd(k,i)*dlt,0.0)
          end do
          settle       = (as*algae(kb(i),i)+poms*lpom(kb(i),i))            &
     &                   *dlt/h(kb(i))
          sed(kb(i),i) = max(sed(kb(i),i)+settle-sedd(kb(i),i)            &
     &                   *dlt,0.0)
        end do
      end subroutine

!***********************************************************************
!*                   i n o r g a n i c   c a r b o n                  **
!***********************************************************************

      subroutine inorganic_carbon
        use shared_data
        use shared_data_kinetics
        implicit none
        double precision :: co2ex

        do i=iu,id
          ticss(kt,i) = 0.0d0
          ! co2ex seems too high --> corrected to 10% for ph >= 7
          if (ph(kt,i) < 4.0) then
              co2ex_factor = 1.0
          else if (ph(kt,i) >= 4.0 .and. ph(kt,i) <= 7.0) then
              co2ex_factor = 1.0 - (((ph(kt,i)-4.0)/(7.0-4.0))*(1.0-0.1))
          else
            co2ex_factor = 0.1
          end if
          co2ex = co2ex_factor*(0.5+0.05*wind*wind)/86400.0
          do k=kt,kb(i)
            apr        = (arr(k,i)-agr(k,i))*algae(k,i)
            ticss(k,i) = bioc*(apr+domd(k,i)+lpomd(k,i)+sedd(k,i))        &
    &                   +co2r*sodd(k,i)*a3(k,i)
            cssw2(14,jb) = cssw2(14,jb)+ticss(k,i)*dlt
          end do
          if (.not.ice(i) .and.allowAtmosphericExchange) then
           !co2sat(i) = co2satANN(0.00035d0, t2(kt,i), ph(kt,i)) !pCO2 = 0.00035    fixed
           ticss(kt,i) = ticss(kt,i) + (co2sat(i) - tic(kt,i))* co2ex/hkt2(i)
           cssatm(14,jb) = cssatm(14,jb)+((co2sat(i) - tic(kt,i))* co2ex/hkt2(i))*dlt*vactive(kt,i)
          endif
        end do
      end subroutine

!***********************************************************************
!*                             p h   c o 2                            **
!***********************************************************************

      subroutine ph_co2
        use shared_data
        use shared_data_kinetics
        implicit none
        double precision :: carb, alk, t1k,  s2, hco3t, co3t, h2co3t
        double precision :: ph_right, hion, bicarb, f_left, ph_start, ph_diff, ph_mid
        double precision :: ph_left, fmid
        double precision :: kw, sqrs2

        do i=iu,id
          do k=kt,kb(i)
            carb = tic(k,i)/1.2e4
            alk  = alkal(k,i)/5.0e4
            t1k  = t1(k,i)+273.15

!********** activity equilibrium constants

            kw = 10.0**(35.3944-5242.39/t1k-0.00835*t1k-11.8261            &
     &           *log10(t1k))
            k1 = 10.0**(14.8435-3404.71/t1k-0.032786*t1k)
            k2 = 10.0**(6.498-2902.39/t1k-0.02379*t1k)

!********** ionic strength

            if (fresh_water) then
              s2 = 2.5e-05*tds(k,i)
            else
              s2 = 0.00147+0.019885*tds(k,i)+0.000038*tds(k,i)**2
            end if

!********** debye-huckel terms and activity coefficients

            sqrs2  = sqrt(s2)
            hco3t  = 10.0**(-0.5085*sqrs2/(1.0+1.3124*sqrs2)            &
     &                 +4.745694e-03+4.160762e-02*s2-9.284843e-03*s2**2)
            co3t   = 10.0**(-2.0340*sqrs2/(1.0+1.4765*sqrs2)            &
     &                 +1.205665e-02+9.715745e-02*s2-2.067746e-02*s2**2)
            h2co3t = 0.0755*s2
            kw     = kw/hco3t
            k1     = k1*h2co3t/hco3t
            k2     = k2*hco3t/co3t

!********** ph evaluation

            ph_left  = -14.0d0
            ph_right = 0.0d0
            hion     = 10.0**ph_left
            bicarb   = carb*k1*hion/(k1*hion+k1*k2+hion**2)
            f_left   = bicarb*(hion+2.0*k2)/hion+kw/hion-alk-hion
            if (f_left.lt.0) then
             ph_start = ph_left
             ph_diff  = ph_right-ph_left
            else
             ph_start = ph_right
             ph_diff  = ph_left-ph_right
            endif
            j = 0
            do while (j.lt.50)
              j       = j+1
              ph_diff = ph_diff*0.5
              ph_mid  = ph_start+ph_diff
              hion    = 10.0**ph_mid
              bicarb  = carb*k1*hion/(k1*hion+k1*k2+hion**2)
              fmid    = bicarb*(hion+2.0*k2)/hion+kw/hion-alk-hion
              if (fmid.lt.0) ph_start = ph_mid
              if (abs(ph_diff).lt.0.01.or.fmid.eq.0.) j = 51
            enddo

!********** ph, carbon dioxide, bicarbonate, and carbonate concentrations

            ph(k,i)   = -ph_mid
            co2(k,i)  = tic(k,i)/(1.0+k1/hion+k1*k2/hion**2)
            hco3(k,i) = tic(k,i)/(1.0+hion/k1+k2/hion)
            co3(k,i)  = tic(k,i)/((hion*hion)/(k1*k2)+hion/k2+1.0)
          end do
        end do
      end subroutine

!***********************************************************************
!*                               i r o n                              **
!***********************************************************************

      subroutine iron
        use shared_data
        use shared_data_kinetics
        implicit none
        double precision :: sedr

        do i=iu,id
          nets       = -fes*fe(kt,i)*a1(kt,i)/hkt2(i)
          sedr       = fer*sodd(kt,i)*a2(kt,i)
          fess(kt,i) = sedr+nets
          do k=kt+1,kb(i)
            nets      = fes*(fe(k-1,i)*a1(k-1,i)-fe(k,i)                &
     &                  *a1(k,i))/h(k)
            sedr      = fer*sodd(k,i)*a2(k,i)
            fess(k,i) = sedr+nets
          end do
        end do
        do i=iu,id
          k=kb(i)
          csssed(20,jb) = csssed(20,jb)+fes*fe(k,i)/h(k)*dlt
          do k=kt,kb(i)
            cssw2(20,jb) = cssw2(20,jb)+ssss(k,i)*dlt
          end do
        end do
      end subroutine

!***********************************************************************
!*               b i o c h e m i c a l   o 2   d e m a n d            **
!***********************************************************************

      subroutine biochemical_o2_demand
        use shared_data
        implicit none

        do i=iu,id
          do k=kt,kb(i)
            cbodss(k,i) = -cbodd(k,i)*cbod(k,i)
          end do
        end do
      end subroutine
