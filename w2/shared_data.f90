!$message:'    module shared_data'
module shared_data
        implicit none
        save
!***********************************************************************
!*                                                                    **
!*                         CE-QUAL-W2-INC                             **
!*                                                                    **
!*                 The INCLUDE file for CE-QUAL-W2                    **
!*                                                                    **
!*                          Version 2.11                              **
!*                         January, 1998                              **
!*                                                                    **
!*                  Developed by: Thomas M. Cole                      **
!*                  Water Quality Modeling Group                      **
!*                  U.S. Army Corps of Engineers                      **
!*                  Waterways Experiment Station                      **
!*                  Vicksburg, Mississippi 39180                      **
!*                  Phone number: (601) 634-3283                      **
!*                                                                    **
!***********************************************************************


!***********************************************************************
!*                                                                    **
!*   This file contains the PARAMETER specifications for CE-QUAL-W2   **
!*   and the EQUIVALENCEs between the constituent array and the var-  **
!*   iables used in the kinetics subroutines.                         **
!*                                                                    **
!*   The parameters are defined as follows:                           **
!*                                                                    **
!*     confn - control filename                                       **
!*     imp   - total number of segments                               **
!*     imc   - total number of segments                               **
!*     kmp   - total number of layers                                 **
!*     kmc   - total number of layers                                 **
!*     nbp   - total number of branches                               **
!*     nsp   - total number of outlet structures                      **
!*     ntp   - total number of tributaries                            **
!*     nwp   - total number of withdrawals                            **
!*     ncp   - total number of constituents                           **
!*     ndp   - maximum number of dates for i/o parameters             **
!*             (see manual, page __)                                  **
!*                                                                    **
!*   Notes:                                                           **
!*                                                                    **
!*   1. The variables IMC and KMC may be set to 1 in order to save    **
!*      memory if constituents are not being modeled.  If consti-     **
!*      tuents are modeled, then IMC and KMC should be set equal      **
!*      to IMP and KMP.                                               **
!*   2. Do not change the value of NCP unless more water quality      **
!*      state variables have been added to the model.                 **
!*   3. No parameters may be set to zero.  They must be set to at     **
!*      least 1.                                                      **
!*                                                                    **
!***********************************************************************


!**** parameter declarations
 
      character(len=72) :: confn = ' '
      character(len=9) ::  month = ' '
      integer :: imp = 0, kmp = 0, imc = 0, kmc = 0
      integer :: nbp = 0, nsp = 0, ntp = 0, nwp = 0
      integer :: ndp = 0
      integer :: ncp = 0, ncp_fixed = 41, ncp_additional_minerals = 0
                


    
      integer :: snp = 0
      integer :: jb = 0, jc = 0, iu = 0, id = 0, kt = 0, oldkt = 0
      integer :: iepr = 0,    kbpr = 0
      integer :: nac = 0
      integer :: nacin = 0,  nactr = 0,  nacpr = 0,  nacdt = 0, nacgw = 0
      integer :: nacload = 0
      integer :: wscdp = 0
      integer :: jdayg = 0
      integer :: nit = 0, nv = 0, kmin = 0, imin = 0, kloc = 0, iloc = 0      
      integer :: year = 0,    gday = 0
      integer :: nwd = 0,    ntr = 0
      integer ::   m = 0
!unit number for time_varying_data
      integer :: wdq = 0
      integer :: met = 0
      integer :: moved = 2
      integer :: error = 1
      !integer :: debug = 3


      logical :: gw_coupling = .false., phreeqc_coupling = .false. 
      logical :: loading = .false.
      logical :: ice_calc = .false.
      logical :: limiting_factor = .false., oxygen_demand = .false.
      logical :: fresh_water = .false., salt_water = .false., susp_solids = .false.
      logical :: constituents = .false.
      logical :: interp_inflow = .false.,  interp_outflow = .false., interp_met = .false.,  &
     &                interp_dtribs = .false., interp_tribs = .false.,         &
     &                interp_head = .false., interp_withdrwl = .false.
      logical :: no_wind = .false., no_inflow = .false.,      no_outflow = .false.,        &
     &                no_heat = .false.
      logical :: precipitation = .false.,  withdrawals = .false.,    tributaries = .false.
      logical :: open_files = .false.,     term_by_term = .false.
      logical :: leap_year = .false.
      logical :: noPhoto = .false.
      logical :: allowAtmosphericExchange = .false.
      logical :: limitingCauseWritten = .false.
      logical :: distributedGwTemperature = .false.

      double precision :: thin_cell_limit = 0.1d0, thin_cell_reduction = 1.0d6  !mmueller slow down flow in very thin cells  
      double precision :: ahstic = 0d0   !mueller algae half sat. conc. tic 
      double precision :: nonzero = 0d0
      double precision :: pcgt = 0d0
      double precision :: elkt = 0d0,     dlt = 0d0
      double precision :: tair = 0d0, tdew = 0d0, cloud = 0d0, phi = 0d0, et = 0d0,  cshe = 0d0,        &
     &                sro = 0d0,    sron = 0d0,   lat = 0d0,    longitude = 0d0,   windh = 0d0,  rhowcp = 0d0
     double precision :: omt1 = 0d0,   omt2 = 0d0,   nh4t1 = 0d0,  nh4t2 = 0d0,  no3t1 = 0d0,  no3t2 = 0d0,    &
     &                at1 = 0d0,    at2 = 0d0,    at3 = 0d0,    at4 = 0d0,    omk1 = 0d0,   omk2 = 0d0,        &
     &                nh4k1,  nh4k2,  no3k1,  no3k2,  ak1,    ak2,        &
     &                ak3,    ak4
      double precision :: palt = 0d0,   apom = 0d0,   o2lim = 0d0,  wind = 0d0
      double precision :: colq10 = 0d0, coldk = 0d0
      double precision :: sdk = 0d0,    lpomdk = 0d0, ldomdk = 0d0, rdomdk = 0d0, lrddk = 0d0
      double precision :: sss = 0d0,    poms = 0d0,   as = 0d0,     fes = 0d0
      double precision :: ae = 0d0, am = 0d0, ag = 0d0, ar = 0d0, asat = 0d0, ahsn = 0d0,   &
     &                ahsp = 0d0
      double precision :: beta = 0d0,   exh2o = 0d0,  exss = 0d0,   exom = 0d0
      double precision :: po4r = 0d0,   biop = 0d0,   partp = 0d0
      double precision :: bion = 0d0,   nh4dk = 0d0,  nh4r = 0d0,   no3dk = 0d0
      double precision :: o2om = 0d0,   o2ag = 0d0,   o2nh4 = 0d0,  o2ar = 0d0
      double precision :: co2r = 0d0,   bioc = 0d0
      double precision :: fer = 0d0
      double precision :: kbod = 0d0,   tbod = 0d0,   rbod = 0d0
      double precision :: daym = 0d0
      double precision :: jday = 0d0,   dlts = 0d0, mindlt = 0d0, jdmin = 0d0, dltav = 0d0, eltmjd = 0d0
      double precision :: non_zero = 0d0
      double precision :: nxmet1 = 0d0,        nxqwd1 = 0d0,      nxmet2 = 0d0,        nxqwd2 = 0d0
          
      double precision :: zmin = 0d0, co2ex_factor = 0d0, naTotalSum = 0d0
      double precision :: cart = 0d0, calf = 0d0, rats = 0d0, curs3 = 0d0, curs2 = 0d0, curs1 = 0d0
      double precision :: ratdi = 0d0, delc  = 0d0, adelc = 0d0, acurv = 0d0
      double precision :: flux = 0d0, ftemp= 0d0, cref = 0d0, cmin1 = 0d0, cmax1 = 0d0
      double precision :: ht  = 0d0, hm = 0d0, hb = 0d0, ratvi = 0d0






      character(len=3), dimension(4)  :: hprc = ' '
      character(len=24), dimension(4) :: hname = ' '
      character(len=72), dimension(7) :: title  = ' '

      character(len=3),  allocatable, dimension(:) :: cprc
      character(len=6),  allocatable, dimension(:) :: cunit1
      character(len=24), allocatable, dimension(:) :: cname2
      character(len=256), allocatable, dimension(:) :: qtrfn, ttrfn,   ctrfn
      character(len=256), allocatable, dimension(:) :: qinfn,  tinfn, cinfn, qotfn,     &    
     &             qdtfn, tdtfn, cdtfn, prefn, tprfn, cprfn,            &
     &             euhfn,  tuhfn, cuhfn, edhfn, tdhfn, cdhfn
      character(len=256):: qwdfn = ' ', metfn = ' '
      character(len=4), allocatable, dimension(:,:) :: lfac
      character(len=7), allocatable, dimension(:,:) :: lfpr
      character(len=7), allocatable, dimension(:,:) :: conv


       
      integer, allocatable, dimension(:) :: kb, kti, kb_real  
      integer, allocatable, dimension(:) :: ipr
      integer, allocatable, dimension(:) ::  cn 
      integer, allocatable, dimension(:) :: trcn, dtcn, prcn, uhcn, dhcn, gwcn
      integer, allocatable, dimension(:) :: loadcn
      integer, allocatable, dimension(:) :: incn
      integer, allocatable, dimension(:) :: nout
      integer, allocatable, dimension(:) :: nstr
      integer, allocatable, dimension(:) :: cus,   ds
      integer, allocatable, dimension(:,:) :: kbsw 
      integer, allocatable, dimension(:,:) :: kout
!<mmueller>
      integer, allocatable, dimension(:,:) :: limitingFactor
!</mmueller>
!unit numbers for time_varying_data      
      integer, allocatable, dimension(:) :: otq, trq, trt, tribconc,    &
     &                                        dtq, dtt, dtc, pre, prt,    &
     &                                        prc, uhe, uht, uhc,    dhe,    &
     &                                        dht, dhc, inq, intemp, inc  




        
      logical, allocatable, dimension(:) :: ice
      logical, allocatable, dimension(:) :: dist_tribs
      logical, allocatable, dimension(:) :: up_flow, dn_flow, uh_internal,  &
     &                     dh_internal, uh_external, dh_external
      logical, allocatable, dimension(:) :: sel_withdrawal
      logical, allocatable, dimension(:,:) :: point_sink
      double precision, allocatable, dimension (:,:) :: algaenosettlingss, algaewithsettlingss, tds
      !<date>2013/08/20</date> ssload
      double precision, allocatable, dimension(:,:,:), target :: cssk, c1, c2, ssgw, ssp, ssgw_out, ssload
     
      


      double precision, dimension(:,:), pointer ::     ss, col, ldom, rdom, algae
      double precision, dimension(:,:), pointer ::     lpom, po4, nh4, no3, dox, sed
      double precision, dimension(:,:), pointer ::     tic, alkal, ph, co2, hco3, co3
      double precision, dimension(:,:), pointer ::     fe, cbod, tra, al, so4, ca
      double precision, dimension(:,:), pointer ::       mg, na, ka, mn, cl, fe3
      double precision, dimension(:,:), pointer ::     mn3, sid, ch4
      double precision, dimension(:,:), pointer ::       feoh3, aloh3, pyrite, po4precip
      double precision, dimension(:,:), pointer ::       cd, pb, zn, ars

      double precision, dimension(:,:), pointer ::     ssss, colss, ldomss, rdomss, algss
      double precision, dimension(:,:), pointer ::     lpomss, po4ss, nh4ss, no3ss, doss
      double precision, dimension(:,:), pointer ::     ticss, fess, cbodss, trass, alss
      double precision, dimension(:,:), pointer ::       so4ss, cass, mgss, nass, kass 
      double precision, dimension(:,:), pointer ::     mnss, clss, fe3ss, co3ss  
      double precision, dimension(:,:), pointer ::     mn3ss, sidss, ch4ss
      double precision, dimension(:,:), pointer ::     feoh3ss, aloh3ss, pyritess, po4precipss
      double precision, dimension(:,:), pointer ::       cdss, pbss, znss, arsss

!<mueller>
!    <date>2001/04/12</date>
!    <reason>source sinks terms for exchange
!            with pcg and phreeqc
!    </reason>
!
      double precision, allocatable, dimension(:,:) :: limitingCause

      double precision, dimension(:,:), pointer :: ssssp, colssp, ldomssp, rdomssp,        &
     &                              algssp, lpomssp, po4ssp, nh4ssp,        &
     &                              no3ssp, dossp, ticssp, fessp, co3ssp,   &
     &                              cbodssp, trassp, alssp, so4ssp,         &
     &                              cassp, mgssp, nassp, kassp,             &
     &                              mnssp, clssp, fe3ssp, mn3ssp, sidssp,   &
     &                              ch4ssp, feoh3ssp, aloh3ssp,             &
     &                              cdssp, pbssp, znssp, arsssp , pyritessp, po4precipssp
      

      double precision, dimension(:,:), pointer :: ssssgw, colssgw, ldomssgw, rdomssgw,    &
     &                              algssgw, lpomssgw, po4ssgw, nh4ssgw,    &
     &                              no3ssgw, dossgw, ticssgw, fessgw,        &
     &                              cbodssgw, trassgw, alssgw, so4ssgw,    &
     &                              cassgw, mgssgw, nassgw, kassgw,        &
     &                              mnssgw, clssgw, mn3ssgw, sidssgw,      &
     &                              ch4ssgw, fe3ssgw,                        &
     &                              cdssgw, pbssgw, znssgw, arsssgw
!     &                               
     double precision, dimension(:,:), pointer :: ssssgwout, colssgwout, ldomssgwout, rdomssgwout, &
!     &                               algssgwout, lpomssgwout,
     &                              po4ssgwout, nh4ssgwout, no3ssgwout, dossgwout, &
     &                              ticssgwout, fessgwout, &
!    &                              cbodssgwout, &
     &                              trassgwout, &
     &                              alssgwout, so4ssgwout, cassgwout, mgssgwout, &
     &                              nassgwout, kassgwout, mnssgwout, clssgwout, &
     &                              fe3ssgwout, mn3ssgwout, sidssgwout, ch4ssgwout, &
     &                              cdssgwout, pbssgwout, znssgwout, arsssgwout
      double precision, allocatable, dimension(:,:) :: pcgq

      double precision, allocatable, dimension(:) :: el,    h
      double precision, allocatable, dimension(:) :: hkt1, hkt2, bkt, bhkt1, bhkt2, bhrkt1
      double precision, allocatable, dimension(:) :: iceth
      double precision, allocatable, dimension(:) :: sod
      double precision, allocatable, dimension(:) :: depthm
      double precision, allocatable, dimension(:) :: wsc
      double precision, allocatable, dimension(:) :: eluh, eldh
      double precision, allocatable, dimension(:) :: qin, pr, qdtr
      double precision, allocatable, dimension(:) :: qsum
      double precision, allocatable, dimension(:) :: tin, tpr, tdtr
      double precision, allocatable, dimension(:) :: qwd
      double precision, allocatable, dimension(:) :: qtr, ttr
      ! mmueller: cumulativ volumes of all tributaries as one value per trib 
      double precision, allocatable, dimension(:) :: vol_trib_single
      double precision, allocatable, dimension(:) :: co2sat  

      double precision, allocatable, dimension(:) :: ratd, curl3, curl2, curl1
      double precision, allocatable, dimension(:) :: ratv, curv3, curv2, curv1

      double precision, allocatable, dimension(:,:) :: lpomd, sedd, domd, sodd, nh4d, no3d, &
     &                              cbodd
      double precision, allocatable, dimension(:,:) :: omrm, nh4rm, no3rm
      double precision, allocatable, dimension(:,:) :: armr, armf
      double precision, allocatable, dimension(:,:) :: setin, setout
      double precision, allocatable, dimension(:,:) :: a1, a2, a3
      double precision, allocatable, dimension(:,:) :: agr, arr, amr, aer
      double precision, allocatable, dimension(:,:) :: fpss, fpfe
      double precision, allocatable, dimension(:,:) ::  t1,    t2
      double precision, allocatable, dimension(:,:) :: az
      double precision, allocatable, dimension(:,:) :: u, w 
      double precision, allocatable, dimension(:,:) :: rho
      double precision, allocatable, dimension(:,:) :: ndlt
      double precision, allocatable, dimension(:,:) :: b, bh
      double precision, allocatable, dimension(:,:) :: qout 
      double precision, allocatable, dimension(:,:) ::  tuh,    tdh
      double precision, allocatable, dimension(:,:) :: cin, cdtr, cpr
      double precision, allocatable, dimension(:,:) :: ctr
      double precision, allocatable, dimension(:,:) :: qstr, estr, wstr
      double precision, allocatable, dimension(:,:,:) :: cuh, cdh

      double precision, allocatable, dimension(:,:) :: adl
      double precision, allocatable, dimension(:,:) :: trib_specie_balance

    
      
      
!end in common block from main.f90

!incommon block from time_varying_data.f90


        double precision, allocatable, dimension(:,:) :: ctrnx,      cinnx,    &
     &                  qoutnx,     cdtrnx,            &
     &                  cprnx,      tuhnx,            &
     &                  tdhnx,      qstrnx
       double precision, allocatable, dimension(:,:,:) ::  cuhnx,  cdhnx
       double precision, allocatable, dimension(:) ::  qdtrnx,  tdtrnx,    &
     &                  prnx,           tprnx,                &
     &                  eluhnx,         eldhnx,                &
     &                  qwdnx,          qtrnx,                &    
     &                  ttrnx,          qinnx,                &
     &                  tinnx
        double precision, allocatable, dimension(:,:) :: ctro,       cino,            &
     &                  qouto,      cdtro,            &
     &                  tuho,       tdho, qstro

        double precision, allocatable, dimension(:,:,:) :: cuho,    cdho
        double precision, allocatable, dimension(:) :: qdtro,                &
     &                  tdtro,          eluho,                &
     &                  eldho,          qwdo,                    &
     &                  qtro,           ttro,                    &
     &                  qino,           tino                
        double precision, allocatable, dimension(:) :: nxqtr1,   nxttr1,   nxctr1,    &
     &                  nxqin1,      nxtin1,   nxcin1,    &
     &                  nxqdt1,      nxtdt1,   nxcdt1,    &
     &                  nxpr1,       nxtpr1,   nxcpr1,    &
     &                  nxeuh1,      nxtuh1,   nxcuh1,    &
     &                  nxedh1,      nxtdh1,   nxcdh1,    &
     &                  nxqot1
        double precision, allocatable, dimension(:) :: nxqtr2, nxttr2,   nxctr2,    &
     &                  nxqin2,      nxtin2,   nxcin2,    &
     &                  nxqdt2,      nxtdt2,   nxcdt2,    &
     &                  nxpr2,       nxtpr2,   nxcpr2,    &
     &                  nxeuh2,      nxtuh2,   nxcuh2,    &
     &                  nxedh2,      nxtdh2,   nxcdh2,    &
     &                  nxqot2

    double precision, allocatable , dimension(:) :: z


!**** type declaration originally from shared_data_main

! flags as integers
    integer :: vbc = 0 , ebc = 0, mbc = 0, pqc = 0, evc = 0, precip = 0
    integer :: infic = 0,  tric = 0,   dtric = 0,  hdic = 0,   outic = 0, wdic = 0, metic = 0
    integer :: qinc = 0,  qoutc = 0, windc = 0, heatc = 0
    integer :: icec = 0, slice = 0, slheat = 0
    integer :: sltr = 0
    integer ::  ccc = 0, limc = 0, sdc = 0

    character :: restartflag = ' '
    character :: dummy_character = ' '
    character(len=1) :: ext1 = ' ',   seg1 = ' ',  esc = ' '
    character(len=2) :: ext2 = ' ',   seg2 = ' ',  deg = ' '
    character(len=3) :: gdch = ' ',  ext3 = ' ',  scrc = ' '
    character(len=3) :: snpc = ' ',  vplc = ' ', tsrc = ' ', prfc = ' ', cplc = ' ', rsoc = ' ',        &
     &                      rsic = ' ', sprc = ' '
 


      character(len=3) ::  mon = ' ',    dum = ' '
      character(len=4) ::   ext4 = ' '
      character(len=5) ::  ljpc = ' ',   wtype = 'fresh', ext5 = ' '
      character(len=7) ::  blank='       '
      character(len=8) ::  cdate = ' ',  ctime = ' '                                        !fortran
      character(len=9) ::  blank1='    m    '
      character(len=12) :: rsofn = ' '
      character(len=17) :: dum1 = ' '




    integer :: nwsc = 0, j = 0, js = 0, jo = 0,  jw = 0, jt = 0,  nscr = 0, nspn = 0, nisnp = 0 
    integer :: i = 0, nprf = 0, niprf = 0, nb = 0, ndt = 0, nsnp = 0,  nspr = 0, nispr = 0, ntsr = 0, nvpl = 0
    integer :: ncpl = 0, nrso = 0, k = 0,  nxqgw1 = 0
    integer :: nsprf = 0, ntac = 0, kbmax = 0
    integer :: jdaynx = 0, nac0 = 0,  ktmax = 0,  iut = 0, idt = 0
    integer :: ntacmx, ntacmn, jac, jdayts, n, jdaypr, jprf, jdaysp 
    integer :: nrs = 0, incr = 0, kbt = 0, is = 0, ie = 0 !int
    integer :: klim = 0, ilim = 0, idiff = 0, iday = 0 
      integer :: count(3) = 0, start(3) = 0 
      integer :: con=5310, bth=5311, rsi=5312, rso=5321, tsr=5322, prf=5323,        &
     &             lpr =5314, vpl=5324, cpl=5325, vpr=5313, err=5327,  wrn=5326,        &
     &             spr=5328, config=5315, input=5316, path_file=5317
      integer :: tsrdp = 0,  snpdp = 0,   vpldp = 0,  cpldp = 0,  prfdp = 0,  rsodp = 0,  &
     &           dltdp = 0,  scrdp = 0,   sprdp = 0
      integer :: frequk = 0
      integer(2) :: cmdlinestatus = 0
 

      logical :: new_run = .false.        !helper switch for creating new netcdf or not
      logical :: gw_update = .false.     !flag to make waiting function take only 1 keystroke
      logical :: snapshot = .false.,  profile = .false., time_series = .false.,            &
     &        vector = .false., contour = .false.,  restart_in = .false.,                &
     &        restart_out = .false.,  spreadsheet = .false.,  screen_output = .false.,   &
     &        restart = .false.
      logical :: iso_temp = .false.,  vert_temp = .false.,    long_temp = .false.


      logical :: evaporation = .false.
      logical :: head_boundary = .false.
      logical :: add_layer = .false.,       sub_layer = .false.
      logical :: open_vpr = .false.,        open_lpr = .false.
      logical :: winter = .false.,          detailed_ice = .false.
      logical :: interpolate = .false.
      logical :: volume_balance = .false.,  energy_balance = .false.,  mass_balance = .false.
      logical :: place_qin = .false.
      logical :: warning_open = .false.,    volume_warning = .false.
      logical :: update_kinetics = .false., update_rates = .false.
      logical :: end_run = .false.,  branch_found = .false.,    upwind = .false.,      &
     &        shift_demand = .false.,  limiting_dlt = .false., ultimate = .false.
      logical :: laserjet_ii = .false.,     laserjet_iii = .false.,    laserjet_iv = .false.
  
      double precision :: xstart = 0.0d0, zstart = 0.0d0, dlxsum = 0.0d0, hsum = 0.0d0
      double precision :: nxtvd = 0.0d0,  nxtmsn = 0.0d0,  nxtmts = 0.0d0, nxtmpr = 0.0d0
      double precision :: nxtmcp = 0.0d0, nxtmvp = 0.0d0, nxtmrs = 0.0d0
      double precision :: nxtmsc = 0.0d0, nxtmsp = 0.0d0
      
      double precision :: icethu = 0.0d0, iceth1 = 0.0d0, iceth2 = 0.0d0, icemin = 0.0d0
      double precision :: icet2 = 0.0d0, icethi = 0.0d0, ice_tol=0.005d0    
      double precision :: rhoa=1.25d0, rhow=1000.0d0, rhoi=916.0d0, rk1=2.12d0
      double precision ::  rl1=333507.0d0,   rimt=0.0d0
      double precision :: g=9.8d0, vtol=1.0d3, cp=4186.0d0, frazdz=0.14d0, azmin=1.4d-6
      double precision :: cpi=2.108d3, latf=333507.0d0
     ! double precision :: dzmin=1.4d-7,    azmax=1.0d-4, dzmax=1.0d3
      double precision :: dzmin=1.4d-10,    azmax=1.0d-4, dzmax=1.0d3

      !after implicit none is introduced
      double precision :: betai = 0.0d0, thtai = 0.0d0, ax = 0.0d0, dxi = 0.0d0, tmstrt = 0.0d0
      double precision :: tmend = 0.0d0, dltmin = 0.0d0, elbot = 0.0d0    
      double precision :: t2i = 0.0d0, albedo = 0.0d0,  hwi = 0.0d0, gammai = 0.0d0, thetai = 0.0d0
      double precision :: chezy = 0.0d0, tsed= 0.0d0
      double precision :: fsod = 0.0d0, cbhe = 0.0d0 , eltm = 0.0d0,  curmax = 0.0d0,  dlmr = 0.0d0
      double precision :: dlvr = 0.0d0, hmax = 0.0d0, dlxmax = 0.0d0, hmin = 0.0d0, dlxmin = 0.0d0
      double precision :: dlxt = 0.0d0, dlxm = 0.0d0, htop = 0.0d0, hmid = 0.0d0
      double precision :: hbot = 0.0d0,  rhoicp = 0.0d0, rhorl1 = 0.0d0, dummy = 0.0d0
      double precision :: dlttvds = 0.0d0
      double precision :: dlttvd = 0.0d0, fw = 0.0d0,  tm = 0.0d0,  vptg = 0.0d0, rhotr = 0.0d0
      double precision :: vqtr = 0.0d0, vqtri = 0.0d0, qtrfr = 0.0d0
      double precision :: vol = 0.0d0, bhsum = 0.0d0, bht = 0.0d0, cz = 0.0d0, ssc = 0.0d0
      double precision :: fetch = 0.0d0, ssccos = 0.0d0,  wwt = 0.0d0, dfc = 0.0d0, expdf = 0.0d0
      double precision :: shears = 0.0d0, az0 = 0.0d0, riaz0 = 0.0d0, buoy = 0.0d0, ri = 0.0d0
      double precision :: riaz1 = 0.0d0, sscsin = 0.0d0, expraz = 0.0d0, gc2 = 0.0d0, udr = 0.0d0
      double precision :: udl = 0.0d0, ud = 0.0d0, rhoin = 0.0d0, vqin = 0.0d0, vqini = 0.0d0
      double precision :: qinfr = 0.0d0, bhrt = 0.0d0, bhrsum = 0.0d0, wt1 = 0.0d0, wt2 = 0.0d0, tau1 = 0.0d0
      double precision :: tau2 = 0.0d0, celrty = 0.0d0, qtot = 0.0d0, dltcal = 0.0d0, limdlt = 0.0d0
      double precision :: rs = 0.0d0,  rsn = 0.0d0, rb = 0.0d0, re = 0.0d0, rc = 0.0d0
      double precision :: tflux = 0.0d0,  gamma = 0.0d0,  hia = 0.0d0, heat = 0.0d0, tice = 0.0d0
      double precision ::  del = 0.0d0, hice = 0.0d0
      double precision :: cour = 0.0d0, t1v = 0.0d0, t2v = 0.0d0, t3v = 0.0d0, theta = 0.0d0
      double precision :: ran = 0.0d0,  c1v = 0.0d0, c2v = 0.0d0, c3v = 0.0d0
      double precision :: dle = 0.0d0, avtout = 0.0d0

      !double precision :: density !function
             
      double precision :: alfa = 0.0d0
      double precision :: t1l = 0.0d0, t2l = 0.0d0, t3l = 0.0d0, c1l = 0.0d0, c2l = 0.0d0, c3l = 0.0d0

      double precision :: deltaT = 0.0d0
      double precision :: balance_error_limit = -0.1d0




      character(len=2), allocatable, dimension(:) :: cunit3

      character(len=3), allocatable, dimension(:) :: dtrc, swc 
      character(len=6), allocatable,  dimension(:) :: cunit2
      character(len=17), allocatable, dimension(:) :: cname1
      character(len=6), allocatable,  dimension(:) ::  segment
      character(len=72):: rsifn,  vprfn, tsrfn, prffn, &
     &                                       vplfn, cplfn, snpfn,    &
     &             lprfn,  bthfn, sprfn
      character(len=8), allocatable,  dimension(:,:) :: sink
      character(len=9), allocatable,  dimension(:,:) :: conv1      


!      integer, allocatable, dimension(:) :: cn0
      integer, allocatable, dimension(:) :: skti, kbmin
      integer, allocatable, dimension(:) :: iprf, ispr    
      integer, allocatable, dimension(:) :: ipri
      integer, allocatable, dimension(:) :: iwd, kwd, jbwd
      integer, allocatable, dimension(:) :: jbtr, itr, kttr, kbtr 
      integer, allocatable, dimension(:) :: ktqin, kbqin
      integer, allocatable, dimension(:) :: jbuh, jbdh
      integer, allocatable, dimension(:) :: uhs, dhs, us, nl

      integer, allocatable,  dimension(:) :: trc, acc, inacc, tracc, dtacc
      integer, allocatable,  dimension(:) :: pracc
      integer, allocatable, dimension (:,:) :: balance_error 


      logical, allocatable, dimension(:) :: dn_head,up_head
      logical, allocatable, dimension(:) ::  ice_in 
      logical, allocatable, dimension(:) :: allow_ice,    one_layer
      logical, allocatable, dimension(:) :: iso_conc, vert_conc, long_conc                    
      logical, allocatable, dimension(:) :: transport
      logical, allocatable, dimension(:) :: place_qtr, specify_qtr


!**** dimension statements

      
      double precision, allocatable, dimension(:) :: icesw
      double precision, allocatable, dimension(:) ::  bhrkt2, avhkt,  dlx, dlxr, elws      
      double precision, allocatable, dimension(:) :: dlxrho
      double precision, allocatable, dimension(:) :: ev, qdt, qpr
      double precision, allocatable, dimension(:) :: q, qc, qssum
      double precision, allocatable, dimension(:) :: sods
      double precision, allocatable, dimension(:) :: rn, phi0
      double precision, allocatable, dimension(:) :: avh
      !<mueller>
!    <date>2001/02/06></date>
!    <reason>void</reason>    
      double precision, allocatable, dimension(:) :: w2z
!</mueller>
      double precision, allocatable, dimension(:) :: depthb
      double precision, allocatable, dimension(:) :: tvp
      double precision, allocatable, dimension(:) :: sf1v
      double precision, allocatable, dimension(:) :: twd 
      double precision, allocatable, dimension(:) :: eltrt, eltrb
      double precision, allocatable, dimension(:) :: c2i, avcout
      double precision, allocatable, dimension(:) ::  qprbr,  evbr    
      double precision, allocatable, dimension(:) :: snpd, vpld, prfd, cpld, rsod, tsrd, dltd, &
     &                          wscd, sprd, scrd
      double precision, allocatable, dimension(:) :: snpf, vplf, prff, cplf, rsof, tsrf,  &
     &                          dltf, scrf, sprf
      double precision, allocatable, dimension(:) :: dltmax  
!<mueller>
!    <date>2001/02/06></date>
!    <reason>void</reason>
      double precision, allocatable, dimension(:) :: w2x
      
      double precision, allocatable, dimension(:,:) :: sf2v, sf3v, sf4v, sf5v, sf6v,            &
     &          sf7v, sf8v, sf9v, sf10v
      double precision, allocatable, dimension(:,:) :: xdummy
!</mueller>    

      double precision, allocatable, dimension (:,:) :: vactive 
      double precision, allocatable, dimension (:,:) :: hactive 
      double precision :: surfacearea = 0.0d0

!<mueller>
!    <date>2001/02/06</date>
!    <reason>gw-inflow for all cells</reason>
      double precision, allocatable, dimension(:,:) :: qgw    
      double precision, allocatable, dimension(:,:) :: qgwin
      double precision, allocatable, dimension(:,:) :: qgwout
!    <date>2005/05/13</date>
!    <reason>gw temperature for all cells</reason>
      double precision, allocatable, dimension(:,:) :: tgw
!    <reason>lake temperature for all cells</reason>
      double precision, allocatable, dimension(:,:) :: tlake
!    <reason>cumulative gw outflow volume since last exchange with gw model</reason>
      double precision, allocatable, dimension(:,:) :: currentGwOutVolume
      
      !<date>2013/08/20</date>
      ! loading water
      double precision, allocatable, dimension(:,:) :: qload
      ! temperature of loading water
      double precision, allocatable, dimension(:,:) :: tload
!</mueller>    

      double precision, allocatable, dimension(:,:) :: admx, admz, dz, dx, dm, p,    &
     &                              hpg, hdg, sb, st,    dzq
      double precision, allocatable, dimension(:,:) :: su, sw, saz
      double precision, allocatable, dimension(:,:) :: tss, qss
      double precision, allocatable, dimension(:,:) :: bb, br, bhr, hseg 
      double precision, allocatable, dimension(:,:) :: sf1l
      double precision, allocatable, dimension(:,:) :: cwd
      double precision, allocatable, dimension(:,:) :: quh1, quh2, qdh1, qdh2
      double precision, allocatable, dimension(:,:) :: quh1_cum_plus, quh2_cum_plus, qdh1_cum_plus, qdh2_cum_plus
      double precision, allocatable, dimension(:,:) :: quh1_cum_minus, quh2_cum_minus, qdh1_cum_minus, qdh2_cum_minus
      double precision, allocatable, dimension(:,:) :: tssuh1, tssuh2, tssdh1, tssdh2
      double precision, allocatable, dimension(:,:) :: qinf    
      double precision, allocatable, dimension(:,:) :: qtrf
      double precision, allocatable, dimension(:,:) :: akbr
      double precision, allocatable, dimension(:,:) :: fetchu, fetchd
      double precision, allocatable, dimension(:,:) :: cvp
      double precision, allocatable, dimension(:,:,:) :: sf2l, sf3l, sf4l, sf5l, sf6l,        &
     &          sf7l, sf8l, sf9l, sf10l, sf11l, sf12l, sf13l
      double precision, allocatable, dimension(:,:,:) :: cssuh1, cssuh2, cssdh1, cssdh2
      double precision, allocatable, dimension(:,:,:) :: c1s,  cssb

     

!<mueller>
!    <date>2001/02/06</date>
!    <reason> keeping track of gw volume</reason>
      double precision, allocatable, dimension(:) :: volgwin, volgwout, tssgwin, tssgwout  
      double precision, allocatable, dimension(:) :: tssev,  tsspr,  tsstr,        &
                       tssdt,  tsswd,  tssuh,    &
     &                 tssdh,  tssin,  tssout, tsss, tssb,   tssice,    &
     &                  esbr,  etbr,   eibr
      double precision, allocatable, dimension(:) :: voltr, volsbr, voltbr, volev, volpr
      double precision, allocatable, dimension(:) :: voldt, volwd,  voluh,  voldh, volin,  volout, dlvol, volibr
      double precision, allocatable, dimension(:) :: bhrho, gma, bta, a, c, d,        &
     &                                       v, f
      double precision, allocatable, dimension(:) ::  sz
      double precision, allocatable, dimension(:) ::     gmat,   dt,     vt
      double precision, allocatable, dimension(:,:) :: btat,    ct,  at
      double precision, allocatable, dimension(:,:) :: tadl,   tadv
      double precision, allocatable, dimension(:,:) :: ad1l,   ad2l,   ad3l,        &
     &                                          ad1v,   ad2v,   ad3v
      double precision, allocatable, dimension(:,:) :: dx1,    dx2,    dx3
      double precision, allocatable, dimension(:,:) :: cmbrs,  cmbrt
      double precision, allocatable, dimension(:,:,:) :: cadl,  cadv
      !balance terms
      double precision, allocatable, dimension(:,:) :: csstr, cssdt, csswd, csspr,     &
                                                       cssin, cssout, cssuh, cssdh,    &
                                                       cssphc, cssw2, cssgwin, cssgwout, &
                                                       cssatm, csssed, cssload


       double precision, allocatable, dimension(:) :: volload, tssload
end module shared_data
