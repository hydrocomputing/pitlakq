! readInput
    subroutine readinput()
        
        use shared_data
        implicit none
        character(len=5) selector


    open (path_file, file='pathnames.txt', status = 'old')
!**** read input filenames
      read (path_file, '(a)') selector
      selector = trim(selector)
      do while (selector .ne. 'end')
         if (selector == 'metfn') then
            read (path_file, '(a)')  metfn
         else if (selector == 'qwdfn') then
            read (path_file, '(a)')  qwdfn
         else if (selector == 'qinfn') then
            read (path_file, '(a)') (qinfn(jb),jb=1,nbp)
         else if (selector == 'tinfn') then
            read (path_file, '(a)') (tinfn(jb),jb=1,nbp)
         else if (selector == 'cinfn') then
            read (path_file, '(a)') (cinfn(jb),jb=1,nbp)
         else if (selector == 'qotfn') then
            read (path_file, '(a)') (qotfn(jb),jb=1,nbp)
         else if (selector == 'qtrfn') then   
            read (path_file, '(a)') (qtrfn(jt),jt=1,ntp)
         else if (selector == 'ttrfn') then   
            read (path_file, '(a)') (ttrfn(jt),jt=1,ntp)
         else if (selector == 'ctrfn') then   
            read (path_file, '(a)') (ctrfn(jt),jt=1,ntp)
         else if (selector == 'qdtfn') then   
            read (path_file, '(a)') (qdtfn(jb),jb=1,nbp)
         else if (selector == 'tdtfn') then   
            read (path_file, '(a)') (tdtfn(jb),jb=1,nbp)
         else if (selector == 'cdtfn') then   
            read (path_file, '(a)') (cdtfn(jb),jb=1,nbp)
         else if (selector == 'prefn') then   
            read (path_file, '(a)') (prefn(jb),jb=1,nbp)
         else if (selector == 'tprfn') then   
            read (path_file, '(a)') (tprfn(jb),jb=1,nbp)
         else if (selector == 'cprfn') then   
            read (path_file, '(a)') (cprfn(jb),jb=1,nbp)
         else if (selector == 'euhfn') then   
            read (path_file, '(a)') (euhfn(jb),jb=1,nbp)
         else if (selector == 'tuhfn') then   
            read (path_file, '(a)') (tuhfn(jb),jb=1,nbp)
         else if (selector == 'cuhfn') then   
            read (path_file, '(a)') (cuhfn(jb),jb=1,nbp)
         else if (selector == 'edhfn') then   
            read (path_file, '(a)') (edhfn(jb),jb=1,nbp)
         else if (selector == 'tdhfn') then   
            read (path_file, '(a)') (tdhfn(jb),jb=1,nbp)
         else if (selector == 'cdhfn') then   
            read (path_file, '(a)') (cdhfn(jb),jb=1,nbp)
         endif
         read (path_file, '(a)') selector
         selector = trim(selector)
      end do

    close(path_file)


end subroutine
