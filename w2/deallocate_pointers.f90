subroutine    deallocate_pointers
     use shared_data
     implicit none

      deallocate (cprc)
      deallocate (cunit1)
      deallocate (cname2) 
      deallocate (qtrfn, ttrfn, ctrfn)
      deallocate (qinfn,  tinfn, cinfn, qotfn,               &
     &             qdtfn, tdtfn, cdtfn, prefn, tprfn, cprfn, &
     &             euhfn, tuhfn, cuhfn, edhfn, tdhfn, cdhfn)
      deallocate (lfac)
      deallocate (lfpr)
!<mmueller>
      deallocate (limitingCause)
      deallocate (limitingFactor)
!</mmueller>
      deallocate (conv)

       
      deallocate (kb, kti, kb_real)  
      deallocate (ipr)
      deallocate (cn) 
      deallocate (trcn, dtcn, prcn, uhcn, dhcn, gwcn, loadcn)
      deallocate (incn)
      deallocate (nout)
      deallocate (nstr)
      deallocate (cus,   ds)
      deallocate (kbsw) 
      deallocate (kout)  

        
      deallocate (ice)
      deallocate (dist_tribs)
      deallocate (up_flow, dn_flow, uh_internal,            &
     &          dh_internal, uh_external, dh_external)
      deallocate (sel_withdrawal)
      deallocate (point_sink)


      deallocate (algaenosettlingss, algaewithsettlingss)

      deallocate (cssk, c1, c2)
      deallocate (ssgw, ssp, ssgw_out, ssload)
      
!<mueller>
!    <date>2001/04/12</date>
!    <reason>source sinks terms for exchange
!            with pcg and phreeqc
!    </reason>
!      deallocate (ssssp(kmc,imc), colssp(kmc,imc), ldomssp(kmc,imc), rdomssp(kmc,imc),       &
!     &            algssp(kmc,imc), lpomssp(kmc,imc), po4ssp(kmc,imc), nh4ssp(kmc,imc),       &
!     &            no3ssp(kmc,imc), dossp(kmc,imc), ticssp(kmc,imc), fessp(kmc,imc),          &
!     &            cbodssp(kmc,imc), trassp(kmc,imc), alssp(kmc,imc), so4ssp(kmc,imc),        &
!     &            cassp(kmc,imc), mgssp(kmc,imc), nassp(kmc,imc), kassp(kmc,imc),            &
!     &            mnssp(kmc,imc), clssp(kmc,imc), fe3ssp(kmc,imc), mn3ssp(kmc,imc),          &
!     &            sidssp(kmc,imc), ch4ssp(kmc,imc), feoh3ssp(kmc,imc),    aloh3ssp(kmc,imc), &
!     &            cdssp(kmc,imc), pbssp(kmc,imc), znssp(kmc,imc), arsssp(kmc,imc),           &
!     &            pyritessp(kmc,imc), po4precipssp(kmc,imc), co3ssp(kmc,imc)) 
!      
!
!      deallocate (ssssgw(kmc,imc), colssgw(kmc,imc), ldomssgw(kmc,imc), rdomssgw(kmc,imc), &
!     &            algssgw(kmc,imc), lpomssgw(kmc,imc), po4ssgw(kmc,imc), nh4ssgw(kmc,imc), &
!     &            no3ssgw(kmc,imc), dossgw(kmc,imc), ticssgw(kmc,imc), fessgw(kmc,imc),    &
!     &            cbodssgw(kmc,imc), trassgw(kmc,imc), alssgw(kmc,imc), so4ssgw(kmc,imc),  &
!     &            cassgw(kmc,imc), mgssgw(kmc,imc), nassgw(kmc,imc), kassgw(kmc,imc),      &
!     &            mnssgw(kmc,imc), clssgw(kmc,imc), fe3ssgw(kmc,imc), mn3ssgw(kmc,imc),    &
!     &            sidssgw(kmc,imc), ch4ssgw(kmc,imc),                                      &
!     &            cdssgw(kmc,imc), pbssgw(kmc,imc), znssgw(kmc,imc), arsssgw(kmc,imc),     &
!       deallocate  (ssssgwout, &
!     &            colssgwout, ldomssgwout, rdomssgwout, &
!    &            algssgwout, lpomssgwout, &
!     &            po4ssgwout, nh4ssgwout, no3ssgwout, &
!     &            dossgwout, ticssgwout, fessgwout, &
!     &            cbodssgwout, &
!     &            trassgwout, alssgwout, &
!     &            so4ssgwout, cassgwout, mgssgwout, &
!     &            nassgwout, kassgwout, mnssgwout, &
!     &            clssgwout, fe3ssgwout, mn3ssgwout, &
!     &            sidssgwout, ch4ssgwout, cdssgwout, &
!     &            pbssgwout, znssgwout, arsssgwout)
      deallocate (el, h)
      deallocate (hkt1, hkt2, bkt, bhkt1, bhkt2, bhrkt1)
      deallocate (iceth)
      deallocate (sod)
      deallocate (depthm)
      deallocate (wsc)
      deallocate (eluh, eldh)
      deallocate (qin, pr, qdtr)
      deallocate (qsum)
      deallocate (tin, tpr, tdtr)
      deallocate (qwd)
      deallocate (qtr, ttr, vol_trib_single)
      deallocate (trib_specie_balance)

      deallocate (co2sat)

      deallocate (ratd)
      deallocate (curl3)
      deallocate (curl2)
      deallocate (curl1)

      deallocate (ratv)
      deallocate (curv3)
      deallocate (curv2)
      deallocate (curv1)
      
      deallocate (lpomd, sedd, domd, sodd, &
     &            nh4d, no3d, cbodd)
      deallocate (omrm, nh4rm, no3rm)
      deallocate (armr, armf)
      deallocate (setin, setout)
      deallocate (a1, a2, a3)
      deallocate (agr, arr, amr, aer)
      deallocate (fpss, fpfe)
      deallocate (t1, t2)
      deallocate (az)
      deallocate (u, w) 
      deallocate (rho)
      deallocate (ndlt)
      deallocate (b, bh)
      deallocate (qout) 
      deallocate (tuh, tdh)
      deallocate (cin, cdtr, cpr)
      deallocate (ctr)
      deallocate (qstr, estr, wstr)
      deallocate (cuh, cdh)

    
      
      
!end in common block from main.f90

!incommon block from time_varying_data.f90


        deallocate (ctrnx, cinnx,   &
     &              qoutnx, cdtrnx, &
     &              cprnx, tuhnx,   &
     &              tdhnx, qstrnx,  &
     &              cuhnx, cdhnx,   &
     &              qdtrnx, tdtrnx, &
     &              prnx, tprnx,    &
     &              eluhnx, eldhnx, &
     &              qwdnx, qtrnx,   &
     &              ttrnx, qinnx,   &
     &              tinnx)
        deallocate (ctro, cino,     &
     &              qouto, cdtro,   &
     &              tuho,  tdho,    &
     &              qstro, cuho,    &
     &              cdho, qdtro,    &
     &              tdtro, eluho,   &
     &              eldho, qwdo,    &
     &              qtro, ttro,     &
     &              qino, tino)
        deallocate (nxqtr1, nxttr1, nxctr1,    &
     &              nxqin1, nxtin1, nxcin1,    &
     &              nxqdt1, nxtdt1, nxcdt1,    &
     &              nxpr1, nxtpr1,  nxcpr1,    &
     &              nxeuh1, nxtuh1, nxcuh1,    &
     &              nxedh1, nxtdh1, nxcdh1,    &
     &              nxqot1)
        deallocate (nxqtr2, nxttr2, nxctr2,    &
     &              nxqin2, nxtin2, nxcin2,    &
     &              nxqdt2, nxtdt2, nxcdt2,    &
     &              nxpr2, nxtpr2,  nxcpr2,    &
     &              nxeuh2, nxtuh2, nxcuh2,    &
     &              nxedh2, nxtdh2, nxcdh2,    &
     &              nxqot2)

    deallocate (z)

! unit numbers for time_varying_data

        deallocate (trq, trt, tribconc)
        deallocate (inq, dtq, pre, uhe, dhe)
        deallocate (intemp, dtt, prt, uht, dht)
        deallocate (inc, dtc, prc, uhc, dhc)
        deallocate (otq)

        deallocate (pcgq)
        deallocate (adl)

 end subroutine deallocate_pointers