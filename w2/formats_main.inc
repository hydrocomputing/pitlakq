!**** input formats
  integer :: linefiller
  linefiller = 1 !dummy to help include firpt line properly
 1000 format(//(8x,a72))
 1010 format(//(8x,2f8.0,i8))
 1020 format(//8x,i8,8f8.0)
 1030 format(//(8x,9f8.0))
 1040 format(//(8x,5i8))
 1050 format(//8x,2f8.0,3x,a5,6(5x,a3))
 1060 format(//(8x,9(5x,a3)))
 1070 format(//13x,a3,2a8,6f8.0)
 1080 format(//8x,a8,f8.0,2a8)
 1090 format(//(8x,9i8))
 1100 format(//(8x,9a8))
 1110 format(:8x,9a8)
 1120 format(:8x,9f8.0)
 1130 format(:8x,9i8)
 1140 format(//11x,a5,8(5x,a3))
 1150 format(//13x,a3,i8,5x,a3)
 1160 format(//13x,a3,8i8)
 1170 format(//8x,3(5x,a3),i8)
 1180 format(//(10f8.0))
 1190 format(//(8x,9f8.0))

 2500 format(a72)
 2510 format(20(1x,a3))
 2520 format(3(1x,a17,1x,a6))
 2530 format(l2,(:/20i4))
 2540 format(20i4)
 2550 format(10f8.2)
 2560 format(2i4/(8(1pe10.2e2)))
 2570 format(a17,2f10.3,17a9)
 2580 format(17x,f10.3)
 2590 format(f8.3,1x,a3,i3,', ',2i4,f8.4,i4)
 2595 format(f8.3,1x,a3,i3,2x,2i4,f8.4,i4)
 2600 format('constituent      julian_day     depth',17(2x,a7))
 2610 format(37x,17(2x,a7))

!**** snapshot formats

 3000 format('1','ce-qual-w2 v2.10 - june, 1995'/7(1x,a72/))
 3010 format(1x,a15/                                                    &
     &       '+',4('_'),1x,10('_')//                                    &
     &       3x,'gregorian date      [gday] =',a16,i3,',',i5/            &
     &       3x,'julian date         [jday] =',i8,' days',f6.2,            &
     &         ' hours'/                                                &
     &       3x,'elapsed time      [eltmjd] =',i8,' days',f6.2,            &
     &         ' hours'/                                                &
     &       3x,'timestep             [dlt] =',i8,' sec'/                &
     &       5x,'at location  [kloc,iloc] = (',i2,',',i2,')'/            &
     &       3x,'minimum timestep  [mindlt] =',i8,' sec '/                &    
     &       5x,'at julian day    [jdmin] =',i8,' days',f6.2,' hours'/    &
     &       5x,'at location  [kmin,imin] = (',i2,',',i2,')')            
 3020 format(3x,'limiting timestep'/                                    &
     &       5x,'at location  [klim,ilim] = (',i2,',',i2,')')
 3030 format(3x,'average timestep   [dltav] =',i8,' sec'/                &
     &       3x,'number of iterations [nit] =',i8/                        &
     &       3x,'number of violations  [nv] =',i8/)
 3040 format(1x,a24/                                                    &
     &       '+',13('_'),1x,10('_')//                                    &
     &       3x,'input'/                                                &
     &       5x,'air temperature       [tair] =',f9.2,1x,a2/            &
     &       5x,'dewpoint temperature  [tdew] =',f9.2,1x,a2/            &
     &       5x,'wind speed            [wind] =',f9.2,' m/sec'/            &
     &       5x,'wind direction         [phi] =',f9.2,' rad'/            &
     &       5x,'wind sheltering        [wsc] =',f9.2/                    &
     &       5x,'cloud cover          [cloud] =',f9.2/                    &
     &       3x,'calculated'/                                            &
     &       5x,'equilibrium temperature [et] =',f9.2,1x,a2/            &
     &       5x,'surface heat exchange [cshe] =',1pe9.2,' m/sec'/        &
     &       5x,'short wave radiation  [sro] =',1pe9.2,1x,a2,' m/sec'/)     
 3050 format(1x,a7/                                                        &
     &       '+',7('_')//                                                &
     &       3x,a16)        
 3060 format(5x,'branch',i3/                                            &
     &       7x,'layer       [kqin] =',i5,'-',i2/                        &
     &       7x,'inflow       [qin] =',f8.2,' m^3/sec'/                    &
     &       7x,'temperature  [tin] =',f8.2,1x,a2)
 3070 format(/3x,'distributed tributaries'/)
 3080 format(5x,'branch',i3/                                            &
     &       7x,'inflow      [qdtr] =',f8.2,' m^3/sec'/                    &    
     &       7x,'temperature [tdtr] =',f8.2,1x,a2)    
 3090 format('+'://3x,'tributaries'/                                    &
     &       5x,'segment     [itr] =',8i8/(t25,8i8))
 3100 format('+':/5x,'layer       [ktr] =',8(i5,'-',i2)/                &
     &      (t25,8(i5,'-',i2)))
 3110 format('+':/5x,'inflow      [qtr] =',8f8.1/                        &
     &      (t25,8f8.1))
 3120 format('+':/5x,'temperature [ttr] =',8f8.1/                        &
     &      (t25,8f8.1))
 3130 format(/1x,a8/                                                    &
     &      '+',8('_'))
 3140 format(/3x,'structure outflows [qstr]'/                            &
     &       5x,'branch ',i2,' = ',8f8.2/                                &
     &      (t16,8f8.2))
 3150 format('+'://                                                        &
     &       3x,'total outflow [qout] =',f8.2,' m^3/s'//                &
     &       3x,'outlets'/                                                &
     &       5x,'layer             [kout] =',12i7/(31x,12i7))
 3160 format('+':/                                                        &
     &       5x,'outflow (m^3/sec) [qout] =',12f7.1/(31x,12f7.1))
 3170 format('+'://                                                        &
     &       3x,'withdrawals'/                                            &
     &       '+',2x,11('_')/                                            &
     &       5x,'segment           [iwd] =',12i7/(30x,12i7))
 3180 format(:5x,'layer             [kwd] =',12i7/(30x,12i7))
 3190 format(:5x,'outflow (m^3/sec) [qwd] =',12f7.1/(30x,12f7.1))
 3200 format('1',a33/                                                    &
     &       '+',11('_'),1x,6('_'),1x,14('_')/)
 3210 format(3x,'branch',i3,' [cin]'/                                    &
     &      (5x,a17,t24,'=',f8.3,1x,a6))
 3220 format(3x,'tributary',i3,' [ctr]'/                                &
     &      (5x,a17,t24,'=',f8.3,1x,a6))
 3230 format(3x,'distributed tributary',i3,' [cdt]'/                    &    
     &      (5x,a17,t24,'=',f8.3,1x,a6))
 3240 format(1x,a7,a13/                                                    &
     &       '+',7('_'),1x,12('_')/)
 3250 format(3x,'evaporation [ev]'/                                        &
     &       '+',2x,11('_')/                                            &
     &      (:/5x,'branch',i3,' =',f8.2))
 3260 format(3x,'precipitation [pr]'/                                    &
     &       '+',2x,13('_')//                                            &
     &      (5x,'branch',i3,' =',f8.2/))        
 3270 format(/1x,'external head boundary elevations'/                    &
     &       '+',8('_'),1x,4('_'),1x,8('_'),1x,10('_')/)
 3280 format(3x,'branch',i3/                                            &
     &       5x,'upstream elevation   [eluh] =',f8.3,' m')
 3290 format(3x,'branch',i3/                                            &    
     &       5x,'downstream elevation [eldh] =',f8.3,' m')
 3300 format(/1x,'water balance'/                                        &
     &       '+',5('_'),1x,7('_')/)            
 3310 format(3x,'branch',i3/                                            &
     &       5x,'spatial change  [volsbr] = ',1pe15.8e2,' m^3'/            &
     &       5x,'temporal change [voltbr] = ',1pe15.8e2,' m^3'/            &
     &       5x,'volume error             = ',1pe15.8e2,' m^3'/            &
     &       5x,'percent error            = ',1pe15.8e2,' %')
 3320 format(/1x,'energy balance'/                                        &
     &       '+',6('_'),1x,7('_')/)
 3330 format(3x,'branch',i3)
 3340 format(5x,'spatially integrated energy  [esbr] = ',1pe15.8e2,        &
     &         ' kj'/                                                    &
     &       5x,'temporally integrated energy [etbr] = ',1pe15.8e2,        &
     &         ' kj'/                                                    &
     &       5x,'energy error                        = ',1pe15.8e2,        &
     &         ' kj'/                                                    &
     &       5x,'percent error                       = ',1pe15.8e2,        &
     &         ' %')            
 3350 format(/1x,'mass balance'/                                        &
     &       '+',4('_'),1x,7('_')/)        
 3360 format(5x,a17/                                                    &
     &       7x,'spatially integrated mass  [cmbrs]= ',1pe15.8e2,1x,a2/ &
     &       7x,'temporally integrated mass [cmbrt]= ',1pe15.8e2,1x,a2/ &
     &       7x,'mass error                        = ',1pe15.8e2,1x,a2/ &
     &       7x,'percent error                     = ',1pe15.8e2,' %')
 3370 format(/1x,a8/                                                    &
     &       '+',8('_')//                                                &
     &       3x,'surface layer [kt] =',i8/                                &    
     &       3x,'elevation   [elkt] =',f8.3,' m'//                        &
     &       3x,'current upstream segment [cus]'/                        &
     &       '+',2x,7('_'),1x,8('_'),1x,7('_')//                        &
     &       (5x,'branch',i3,' =',i3))

!**** run time information formats

 4000 format(/1x,20('*'),' add layer',i3,' at julian day =',f9.3,        &
     &         1x,20('*'))
 4010 format(/1x,11('*'),' add segments ',i3,' through ',i3,            &
     &         ' at julian day =',f9.3,1x,10('*'))
 4020 format(/1x,18('*'),' subtract layer',i3,' at julian day =',f9.3,    &
     &         1x,17('*'))
 4030 format(/1x,8('*'),' subtract segments ',i3,' through ',i3,        &
     &         ' at julian day =',f9.3,1x,8('*'))
 4040 format(//17('*'),2x,a42,2x,17('*')/)
 4050 format(/1x,a18)
 4060 format(1x,a6,' =',i3,' x',i3/                                        &
     &       '     maximum active cells = ',i4/                            &
     &       '     minimum active cells = ',i4)
 4070 format(1x,a24,f8.1,' -',f8.1,' m')
 4080 format(1x,a24,f8.2,' -',f8.2,' m')
 4090 format(1x,a10)
 4100 format(1x,a24,i8)
 4110 format(1x,a24,i8,' sec')
 4120 format(1x,a24,i8,' days',f6.2,' hours')

!**** time series format's

 5000 format(a72)
 5010 format(30i3)
 5020 format(4(a17,1x,a6))
 5030 format(f10.3,2f10.2,100(1pe10.3e2))

!**** run time warning formats

 6000 format('computational warning at julian day = ',f10.3/            &
     &       '  timestep        = ',f4.0,' sec')
 6010 format('computational warning at julian day = ',f10.3/            &
     &       '  spatial change  =',1pe15.8e2,' m^3'/                    &
     &       '  temporal change =',1pe15.8e2,' m^3'/                    &
     &       '  volume error    =',1pe15.8e2,' m^3')
 6020 format('**severe** computational warning at julian day = ',f10.3/    &
     &       '  at segment ',i3/                                        &    
     &       '    water surface elevation [z]  =',f10.3,' m'/            &    
     &       '    layer thickness              =',f10.3,' m')

!**** run time error formats

 7000 format('fatal error'/                                                &
     &       '  insufficient segments in branch',i3/                    &
     &       '  julian day = ',f9.2,'  water surface layer =',i3/        &
     &       '  iut=',i5,' ds(jb)-1=',i5,' kb(i)=',i5,' kt=',i5,        &
     &       '  elws(i)=',f9.2)        
 7010 format('fatal error'/                                                &
     &       '  negative surface layer height'/                            &
     &       '  julian day = ',f9.2,'  segment =',i3,'  height = ',        &
     &       f8.2,' m')

!**** contour plot formats

 8000 format(8(i8,2x))
 8010 format(9(i8,2x))
 8020 format(8(e13.6,2x))
 8030 format(a17)
 8040 format(f12.4,5x,a9,5x,i2,5x,i4)
 8050 format(9(e13.6,2x))

!<mueller>
!    <date>2001/02/06></date>
!    <reason>new foramt satement for screen output</reason>
 9000 format('jday =',f9.3,'  dlt =',i5)  