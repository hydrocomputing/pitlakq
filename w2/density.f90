!***********************************************************************
!*                  f u n c t i o n   d e n s i t y                   **
!***********************************************************************

!$message:'  function density'
      double precision function   density (local_t,local_ss,local_ds)
      use shared_data
      implicit none

!****** type declaration

!        logical fresh_water, salt_water, susp_solids

!****** after impilict none
        double precision :: ds0, local_ds, local_t, local_ss



        ds0     = max(local_ds,0.0)
        density = ((((6.536332e-9*local_t-1.120083e-6)*local_t+1.001685e-4)            &
     &            *local_t-9.09529e-3)*local_t+6.793952e-2)*local_t+0.842594
        if (susp_solids) density = density+6.2e-4*local_ss
        if (fresh_water) density = density+ds0*((4.99e-8*local_t-3.87e-6)        &
     &                             *local_t+8.221e-4)
        if (salt_water)  density = density+ds0*((((5.3875e-9            &
     &                             *local_t-8.2467e-7)*local_t+7.6438e-5)            &
     &                             *local_t-4.0899e-3)*local_t+0.824493)            &
     &                             +((-1.6546e-6*local_t+1.0227e-4)            &
     &                             *local_t-5.72466e-3)*ds0**1.5+4.8314e-4    &
     &                             *ds0*ds0
        density = density+999.0
      end



