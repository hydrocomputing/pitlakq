!***********************************************************************
!*          s u b r o u t i n e   h e a t   e x c h a n g e           **
!***********************************************************************

!$message:'  subroutine heat_exchange'
      subroutine heat_exchange ()
      use shared_data
      implicit none



        double precision, dimension(12) :: eqt = (/-0.13d0, -0.23d0, -0.16d0, -0.02d0, 0.06d0, 0.00d0, -0.09d0,        &
     &            -0.08d0,  0.06d0,  0.22d0,  0.25d0, 0.10d0/)

        double precision :: sbc =2.0411d-7
        double precision :: tairt, x, local_h, local_d, sinal, a0, tdewt
        double precision :: long0, ha, tstar, etp,  t2k
        double precision :: fac,  ea, ts, es


        double precision :: windt !, twedt



!****** month
        if (month.eq.'  january') m = 1
        if (month.eq.' february') m = 2
        if (month.eq.'    march') m = 3
        if (month.eq.'    april') m = 4
        if (month.eq.'      may') m = 5
        if (month.eq.'     june') m = 6
        if (month.eq.'     july') m = 7
        if (month.eq.'   august') m = 8
        if (month.eq.'september') m = 9
        if (month.eq.'  october') m = 10
        if (month.eq.' november') m = 11
        if (month.eq.' december') m = 12

!****** english units

        windt = wind*2.23714d0
        tdewt = tdew*9.0d0/5.0d0+32.0d0
        tairt = tair*9.0d0/5.0d0+32.0d0
        windt = windt*log(2.0d0/0.003d0)/log(windh/0.003d0)

!****** solar radiation

        long0 = 15.0d0*int(longitude/15.0d0)
        local_d     = 0.409280d0*cos(0.017214d0*(172.0d0-jdayg))
        x     = (jday-jdayg)*24.0d0
        local_h     = 0.261799d0*(x-(longitude-long0)*0.066667d0+eqt(m)-12.0d0)
        sinal = sin(lat*.017453)*sin(local_d)+cos(lat*.017453)*cos(local_d)*cos(local_h)
        a0    = 57.2985d0*asin(sinal)
        sro   = 2.044d0*a0+0.1296d0*a0**2-0.001941d0*a0**3+7.591d-6*a0**4
        sro   = (1.0d0-0.0065d0*cloud*cloud)*sro*24.0d0
        if (a0.lt.0.0) sro = 0.0d0


!****** equilibrium temperature and heat exchange coefficient

        et    = tdewt
        fw    = 70.0d0+0.7d0*windt*windt
        ha    = 3.1872d-08*(tairt+459.67d0)**4
        tstar = (et+tdewt)*0.5d0
        beta  = 0.255d0-(0.0085d0*tstar)+(0.000204d0*tstar**2)
        cshe  = 15.7d0+(0.26d0+beta)*fw
        etp   = (sro+ha-1801.0d0)/cshe+(cshe-15.7d0)                        &
     &            *(0.26d0*tairt+beta*tdewt)/(cshe*(0.26d0+beta))
        j     = 0
        do while (abs(etp-et).gt.0.05.and.j.lt.50)
          et    = etp
          tstar = (et+tdewt)*0.5
          beta  = 0.255d0-(0.0085d0*tstar)+(2.04d-4*tstar**2)
          cshe  = 15.7+(0.26+beta)*fw
          etp   = (sro+ha-1801.0d0)/cshe+(cshe-15.7d0)*(0.26d0*tairt+beta        &
     &            *tdewt)/(cshe*(0.26d0+beta))
          j     = j+1
        end do

!****** si units

        et   = (et-32.0d0)*5.0d0/9.0d0
        sro  = sro*3.14d-8
        sron = sro*0.94d0
        cshe = cshe*5.65d-8
      return

!***********************************************************************
!*                            r a d i a t i o n                       **
!***********************************************************************

      entry radiation ()

!****** net solar radiation

        rsn = 0.94d0*rs

!****** net atmospheric radiation

        t2k = 273.2d0+tair
        fac = 1.0d0+0.0017d0*cloud**2
        ran = 1000.0d0/3600.0d0*9.37d-6*sbc*t2k**6*fac*0.97d0
      return

!***********************************************************************
!*                       s u r f a c e   t e r m s                    **
!***********************************************************************

      entry surface_terms (ts)

!****** partial water vapor pressure of the air

        if (tdew.gt.0.0) then
          ea = exp(2.3026d0*(7.5d0*tdew/(tdew+237.3d0)+0.6609d0))
        else
          ea = exp(2.3026d0*(9.5d0*tdew/(tdew+265.5d0)+0.6609d0))
        end if

!****** partial water vapor pressure at the water surface temperature

        if (ts.gt.0.0) then
          es = exp(2.3026d0*(7.5d0*ts/(ts+237.3d0)+0.6609d0))
        else
          es = exp(2.3026d0*(9.5d0*ts/(ts+265.5d0)+0.6609d0))
        end if

!****** longwave back radiation

        rb = 5.443e-8*(ts+273.2d0)**4

!****** evaporation

        fw = 9.2d0+0.46d0*wind**2
        re = fw*(es-ea)

!****** conduction

        rc = 0.47d0*fw*(ts-tair)
      end
