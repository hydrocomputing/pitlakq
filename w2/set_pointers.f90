subroutine    set_pointers
     use shared_data
     implicit none


      allocate (cprc(ncp))
      allocate (cunit1(ncp))
      allocate (cname2(ncp))
      allocate (qtrfn(ntp), ttrfn(ntp),   ctrfn(ntp))
      allocate (qinfn(nbp),  tinfn(nbp), cinfn(nbp), qotfn(nbp),     &
     &             qdtfn(nbp), tdtfn(nbp), cdtfn(nbp), prefn(nbp), tprfn(nbp), cprfn(nbp),            &
     &             euhfn(nbp),  tuhfn(nbp), cuhfn(nbp), edhfn(nbp), tdhfn(nbp), cdhfn(nbp))
      allocate (lfac(kmc,imc))
      allocate (lfpr(kmc,imc))
!<mmueller>
      allocate (limitingCause(kmc,imc))
      allocate (limitingFactor(kmc,imc))
!</mmueller>
      allocate (conv(kmp,imp))



      allocate (kb(imp), kti(imp), kb_real(imp))
      allocate (ipr(imp))
      allocate (cn(ncp))
      allocate (trcn(ncp), dtcn(ncp), prcn(ncp), uhcn(ncp), dhcn(ncp), gwcn(ncp))
      allocate (loadcn(ncp))
      allocate (incn(ncp))
      allocate (nout(nbp))
      allocate (nstr(nbp))
      allocate (cus(nbp),   ds(nbp))
      allocate (kbsw(nsp,nbp))
      allocate (kout(kmp,nbp))


      allocate (ice(imp))
      allocate (dist_tribs(nbp))
      allocate (up_flow(nbp), dn_flow(nbp), uh_internal(nbp),            &
     &          dh_internal(nbp), uh_external(nbp), dh_external(nbp))
      allocate (sel_withdrawal(nbp))
      allocate (point_sink(nsp,nbp))


      allocate (algaenosettlingss(kmc,imc), algaewithsettlingss(kmc,imc), tds(kmc,imc))

      allocate(cssk(kmc,imc,ncp), c1(kmc,imc,ncp), c2(kmc,imc,ncp))
      allocate(ssgw(kmc,imc,ncp), ssp(kmc,imc,ncp), ssgw_out(kmc,imc,ncp))
      allocate(ssload(kmc,imc,ncp))

!<mueller>
!    <date>2001/04/12</date>
!    <reason>source sinks terms for exchange
!            with pcg and phreeqc
!    </reason>
!      allocate (ssssp(kmc,imc), colssp(kmc,imc), ldomssp(kmc,imc), rdomssp(kmc,imc),        &
!     &            algssp(kmc,imc), lpomssp(kmc,imc), po4ssp(kmc,imc), nh4ssp(kmc,imc),        &
!     &            no3ssp(kmc,imc), dossp(kmc,imc), ticssp(kmc,imc), fessp(kmc,imc),            &
!     &            cbodssp(kmc,imc), trassp(kmc,imc), alssp(kmc,imc), so4ssp(kmc,imc),            &
!     &            cassp(kmc,imc), mgssp(kmc,imc), nassp(kmc,imc), kassp(kmc,imc),                &
!     &            mnssp(kmc,imc), clssp(kmc,imc), fe3ssp(kmc,imc), mn3ssp(kmc,imc),            &
!     &            sidssp(kmc,imc), ch4ssp(kmc,imc), feoh3ssp(kmc,imc),    aloh3ssp(kmc,imc),    &
!     &            cdssp(kmc,imc), pbssp(kmc,imc), znssp(kmc,imc), arsssp(kmc,imc),              &
!     &            pyritessp(kmc,imc), po4precipssp(kmc,imc), co3ssp(kmc,imc))
!
!
!      allocate (ssssgw(kmc,imc), colssgw(kmc,imc), ldomssgw(kmc,imc), rdomssgw(kmc,imc),    &
!     &            algssgw(kmc,imc), lpomssgw(kmc,imc), po4ssgw(kmc,imc), nh4ssgw(kmc,imc),    &
!     &            no3ssgw(kmc,imc), dossgw(kmc,imc), ticssgw(kmc,imc), fessgw(kmc,imc),        &
!     &            cbodssgw(kmc,imc), trassgw(kmc,imc), alssgw(kmc,imc), so4ssgw(kmc,imc),        &
!     &            cassgw(kmc,imc), mgssgw(kmc,imc), nassgw(kmc,imc), kassgw(kmc,imc),            &
!     &            mnssgw(kmc,imc), clssgw(kmc,imc), fe3ssgw(kmc,imc), mn3ssgw(kmc,imc),        &
!     &            sidssgw(kmc,imc), ch4ssgw(kmc,imc),                                            &
!     &            cdssgw(kmc,imc), pbssgw(kmc,imc), znssgw(kmc,imc), arsssgw(kmc,imc),       &
       allocate  (ssssgwout(kmc,imc), &
     &            colssgwout(kmc,imc), ldomssgwout(kmc,imc), rdomssgwout(kmc,imc), &
!    &            algssgwout(kmc,imc), lpomssgwout(kmc,imc), &
     &            po4ssgwout(kmc,imc), nh4ssgwout(kmc,imc), no3ssgwout(kmc,imc), &
     &            dossgwout(kmc,imc), ticssgwout(kmc,imc), fessgwout(kmc,imc), &
!     &            cbodssgwout(kmc,imc), &
     &            trassgwout(kmc,imc), alssgwout(kmc,imc), &
     &            so4ssgwout(kmc,imc), cassgwout(kmc,imc), mgssgwout(kmc,imc), &
     &            nassgwout(kmc,imc), kassgwout(kmc,imc), mnssgwout(kmc,imc), &
     &            clssgwout(kmc,imc), fe3ssgwout(kmc,imc), mn3ssgwout(kmc,imc), &
     &            sidssgwout(kmc,imc), ch4ssgwout(kmc,imc), cdssgwout(kmc,imc), &
     &            pbssgwout(kmc,imc), znssgwout(kmc,imc), arsssgwout(kmc,imc))
      allocate (el(kmp), h(kmp))
      allocate (hkt1(imp), hkt2(imp), bkt(imp), bhkt1(imp), bhkt2(imp), bhrkt1(imp))
      allocate (iceth(imp))
      allocate (sod(imp))
      allocate (depthm(kmp))
      allocate (wsc(ndp))
      allocate (eluh(nbp), eldh(nbp))
      allocate (qin(nbp), pr(nbp), qdtr(nbp))
      allocate (qsum(nbp))
      allocate (tin(nbp), tpr(nbp), tdtr(nbp))
      allocate (qwd(nwp))
      allocate (qtr(ntp), ttr(ntp), vol_trib_single(ntp))

      allocate (co2sat(imc))

      allocate (ratd(imp))
      allocate (curl3(imp))
      allocate (curl2(imp))
      allocate (curl1(imp))

      allocate (ratv(kmp))
      allocate (curv3(kmp))
      allocate (curv2(kmp))
      allocate (curv1(kmp))

      allocate (lpomd(kmc,imc), sedd(kmc,imc), domd(kmc,imc), sodd(kmc,imc), &
     &                            nh4d(kmc,imc), no3d(kmc,imc), cbodd(kmc,imc))
      allocate (omrm(kmc,imc), nh4rm(kmc,imc), no3rm(kmc,imc))
      allocate (armr(kmc,imc), armf(kmc,imc))
      allocate (setin(kmc,imc), setout(kmc,imc))
      allocate (a1(kmc,imc), a2(kmc,imc), a3(kmc,imc))
      allocate (agr(kmc,imc), arr(kmc,imc), amr(kmc,imc), aer(kmc,imc))
      allocate (fpss(kmp,imp), fpfe(kmp,imp))
      allocate (t1(kmp,imp),    t2(kmp,imp))
      allocate (az(kmp,imp))
      allocate (u(kmp,imp), w(kmp,imp))
      allocate (rho(kmp,imp))
      allocate (ndlt(kmp,imp))
      allocate (b(kmp,imp), bh(kmp,imp))
      allocate (qout(kmp,nbp))
      allocate (tuh(kmp,nbp),    tdh(kmp,nbp))
      allocate (cin(ncp,nbp), cdtr(ncp,nbp), cpr(ncp,nbp))
      allocate (ctr(ncp,ntp))
      allocate (qstr(nsp,nbp), estr(nsp,nbp), wstr(nsp,nbp))
      allocate (cuh(kmc,ncp,nbp), cdh(kmc,ncp,nbp))




!end in common block from main.f90

!incommon block from time_varying_data.f90


        allocate (      ctrnx(ncp,ntp),      cinnx(ncp,nbp),            &
     &                  qoutnx(kmp,nbp),     cdtrnx(ncp,nbp),            &
     &                  cprnx(ncp,nbp),      tuhnx(kmp,nbp),            &
     &                  tdhnx(kmp,nbp),      qstrnx(nsp,nbp),            &
     &                  cuhnx(kmp,ncp,nbp),  cdhnx(kmp,ncp,nbp),        &
     &                  qdtrnx(nbp),         tdtrnx(nbp),                &
     &                  prnx(nbp),           tprnx(nbp),                &
     &                  eluhnx(nbp),         eldhnx(nbp),                &
     &                  qwdnx(nwp),          qtrnx(ntp),                &
     &                  ttrnx(ntp),          qinnx(nbp),                &
     &                  tinnx(nbp))
        allocate (      ctro(ncp,ntp),       cino(ncp,nbp),                &
     &                  qouto(kmp,nbp),      cdtro(ncp,nbp),            &
     &                  tuho(kmp,nbp),       tdho(kmp,nbp),                &
     &                  qstro(nsp,nbp),      cuho(kmp,ncp,nbp),            &
     &                  cdho(kmp,ncp,nbp),   qdtro(nbp),                &
     &                  tdtro(nbp),          eluho(nbp),                &
     &                  eldho(nbp),          qwdo(nwp),                    &
     &                  qtro(ntp),           ttro(ntp),                    &
     &                  qino(nbp),           tino(nbp))
        allocate (        nxqtr1(ntp),      nxttr1(ntp),   nxctr1(ntp),    &
     &                  nxqin1(nbp),      nxtin1(nbp),   nxcin1(nbp),    &
     &                  nxqdt1(nbp),      nxtdt1(nbp),   nxcdt1(nbp),    &
     &                  nxpr1(nbp),       nxtpr1(nbp),   nxcpr1(nbp),    &
     &                  nxeuh1(nbp),      nxtuh1(nbp),   nxcuh1(nbp),    &
     &                  nxedh1(nbp),      nxtdh1(nbp),   nxcdh1(nbp),    &
     &                  nxqot1(nbp))
        allocate (        nxqtr2(ntp),      nxttr2(ntp),   nxctr2(ntp),    &
     &                  nxqin2(nbp),      nxtin2(nbp),   nxcin2(nbp),    &
     &                  nxqdt2(nbp),      nxtdt2(nbp),   nxcdt2(nbp),    &
     &                  nxpr2(nbp),       nxtpr2(nbp),   nxcpr2(nbp),    &
     &                  nxeuh2(nbp),      nxtuh2(nbp),   nxcuh2(nbp),    &
     &                  nxedh2(nbp),      nxtdh2(nbp),   nxcdh2(nbp),    &
     &                  nxqot2(nbp))

    allocate (z(imp))

! unit numbers for time_varying_data

        allocate ( trq(ntp), trt(ntp), tribconc(ntp))
        allocate ( inq(nbp), dtq(nbp), pre(nbp), uhe(nbp), dhe(nbp))
        allocate ( intemp(nbp), dtt(nbp), prt(nbp), uht(nbp), dht(nbp))
        allocate ( inc(nbp), dtc(nbp), prc(nbp), uhc(nbp), dhc(nbp))
        allocate ( otq(nbp))

        allocate ( pcgq(imp, kmp))
        allocate ( adl(imp, kmp))


                tra   => c2(:,:,1)
                ss    => c2(:,:,2)        !mueller
                col   => c2(:,:,3)
                !tds   => c2(:,:,4)        !mueller
                ldom  => c2(:,:,5)
                rdom  => c2(:,:,6)
                algae => c2(:,:,7)
                lpom  => c2(:,:,8)
                po4   => c2(:,:,9)
                nh4   => c2(:,:,10)
                no3   => c2(:,:,11)
                dox    => c2(:,:,12)
                sed   => c1(:,:,13)
                tic   => c2(:,:,14)
                alkal => c2(:,:,15)
                ph    => c1(:,:,16)   !mmueller
                co2   => c1(:,:,17)
                hco3  => c1(:,:,18)
                co3   => c1(:,:,19)
                fe    => c2(:,:,20)
                cbod  => c2(:,:,21)
                al    => c2(:,:,22)    !mueller
                so4   => c2(:,:,23)    !mueller
                ca    => c2(:,:,24)    !mueller
                mg    => c2(:,:,25)    !mueller
                na    => c2(:,:,26)    !mueller
                ka    => c2(:,:,27)    !mueller
                mn    => c2(:,:,28)    !mueller
                cl    => c2(:,:,29)    !mueller
                fe3   => c2(:,:,30)    !mueller
                mn3   => c2(:,:,31)    !mueller
                sid   => c2(:,:,32)    !mueller
                ch4   => c2(:,:,33)    !mueller
                feoh3 => c2(:,:,34)    !mueller
                aloh3 => c2(:,:,35)    !mueller
                cd    => c2(:,:,36)    !mueller
                pb    => c2(:,:,37)    !mueller
                zn    => c2(:,:,38)    !mueller
                ars   => c2(:,:,39)    !mueller
                pyrite=> c2(:,:,40)    !mueller
                po4precip => c2(:,:,41)    !mueller


                trass    => cssk(:,:,1)    !mueller
                ssss    => cssk(:,:,2)
                colss    => cssk(:,:,3)
                ldomss    => cssk(:,:,5)
                rdomss    => cssk(:,:,6)
                algss    => cssk(:,:,7)
                lpomss    => cssk(:,:,8)
                po4ss    => cssk(:,:,9)
                nh4ss   => cssk(:,:,10)
                no3ss    => cssk(:,:,11)
                doss    => cssk(:,:,12)
                ticss    => cssk(:,:,14)
                co3ss    => cssk(:,:,19)
                fess    => cssk(:,:,20)
                cbodss    => cssk(:,:,21)
                alss    => cssk(:,:,22)    !mueller
                so4ss    => cssk(:,:,23)    !mueller
                cass    => cssk(:,:,24)    !mueller
                mgss    => cssk(:,:,25)    !mueller
                nass    => cssk(:,:,26)    !mueller
                kass    => cssk(:,:,27)    !mueller
                mnss    => cssk(:,:,28)    !mueller
                clss    => cssk(:,:,29)    !mueller
                fe3ss    => cssk(:,:,30)    !mueller
                mn3ss   => cssk(:,:,31)    !mueller
                sidss   => cssk(:,:,32)    !mueller
                ch4ss   => cssk(:,:,33)    !mueller
                feoh3ss => cssk(:,:,34)    !mueller
                aloh3ss => cssk(:,:,35)    !mueller
                cdss    => cssk(:,:,36)    !mueller
                pbss    => cssk(:,:,37)    !mueller
                znss    => cssk(:,:,38)    !mueller
                arsss   => cssk(:,:,39)    !mueller
                pyritess => cssk(:,:,40)    !mueller
                po4precipss => cssk(:,:,41)    !mueller

                trassgw    => ssgw(:,:,1)
                ssssgw    => ssgw(:,:,2)
                colssgw    => ssgw(:,:,3)
                ldomssgw    => ssgw(:,:,5)
                rdomssgw    => ssgw(:,:,6)
                algssgw    => ssgw(:,:,7)
                lpomssgw    => ssgw(:,:,8)
                po4ssgw    => ssgw(:,:,9)
                nh4ssgw    => ssgw(:,:,10)
                no3ssgw    => ssgw(:,:,11)
                dossgw    => ssgw(:,:,12)
                ticssgw    => ssgw(:,:,14)
!                co3ssgw    => ssgw(:,:,19)
                fessgw    => ssgw(:,:,20)
                cbodssgw    => ssgw(:,:,21)
                alssgw    => ssgw(:,:,22)
                so4ssgw    => ssgw(:,:,23)
                cassgw    => ssgw(:,:,24)
                mgssgw    => ssgw(:,:,25)
                nassgw    => ssgw(:,:,26)
                kassgw    => ssgw(:,:,27)
                mnssgw    => ssgw(:,:,28)
                clssgw    => ssgw(:,:,29)
                fe3ssgw    => ssgw(:,:,30)
                mn3ssgw    => ssgw(:,:,31)
                sidssgw    => ssgw(:,:,32)
                ch4ssgw    => ssgw(:,:,33)
!                feoh3ssgw    => ssgw(:,:,34)
!                aloh3ssgw    => ssgw(:,:,35)
                cdssgw    => ssgw(:,:,36)
                pbssgw    => ssgw(:,:,37)
                znssgw    => ssgw(:,:,38)
                arsssgw    => ssgw(:,:,39)
!                pyritessgw    => ssgw(:,:,40)
!                po4precipssgw    => ssgw(:,:,41)


     		trassgwout    => ssgw_out(:,:,1)
                ssssgwout    => ssgw_out(:,:,2)
                colssgwout    => ssgw_out(:,:,3)
                ldomssgwout    => ssgw_out(:,:,5)
                rdomssgwout    => ssgw_out(:,:,6)
!                algssgwout    => ssgw_out(:,:,7)
!                lpomssgwout    => ssgw_out(:,:,8)
                po4ssgwout    => ssgw_out(:,:,9)
                nh4ssgwout    => ssgw_out(:,:,10)
                no3ssgwout    => ssgw_out(:,:,11)
                dossgwout    => ssgw_out(:,:,12)
                ticssgwout    => ssgw_out(:,:,14)
!                co3ssgwout    => ssgw_out(:,:,19)
                fessgwout    => ssgw_out(:,:,20)
!                cbodssgwout    => ssgw_out(:,:,21)
                alssgwout    => ssgw_out(:,:,22)
                so4ssgwout    => ssgw_out(:,:,23)
                cassgwout    => ssgw_out(:,:,24)
                mgssgwout    => ssgw_out(:,:,25)
                nassgwout    => ssgw_out(:,:,26)
                kassgwout    => ssgw_out(:,:,27)
                mnssgwout    => ssgw_out(:,:,28)
                clssgwout    => ssgw_out(:,:,29)
                fe3ssgwout    => ssgw_out(:,:,30)
                mn3ssgwout    => ssgw_out(:,:,31)
                sidssgwout    => ssgw_out(:,:,32)
                ch4ssgwout    => ssgw_out(:,:,33)
!                feoh3ssgwout    => ssgw_out(:,:,34)
!                aloh3ssgwout    => ssgw_out(:,:,35)
                cdssgwout    => ssgw_out(:,:,36)
                pbssgwout    => ssgw_out(:,:,37)
                znssgwout    => ssgw_out(:,:,38)
                arsssgwout    => ssgw_out(:,:,39)
!                pyritessgwout    => ssgw_out(:,:,40)
!                po4precipssgwout    => ssgw_out(:,:,41)

                trassp    => ssp(:,:,1)
                ssssp    => ssp(:,:,2)
                colssp    => ssp(:,:,3)
                ldomssp    => ssp(:,:,5)
                rdomssp    => ssp(:,:,6)
                algssp    => ssp(:,:,7)
                lpomssp    => ssp(:,:,8)
                po4ssp    => ssp(:,:,9)
                nh4ssp    => ssp(:,:,10)
                no3ssp    => ssp(:,:,11)
                dossp    => ssp(:,:,12)
                ticssp    => ssp(:,:,14)
                co3ssp    => ssp(:,:,19)
                fessp    => ssp(:,:,20)
                cbodssp    => ssp(:,:,21)
                alssp    => ssp(:,:,22)
                so4ssp    => ssp(:,:,23)
                cassp    => ssp(:,:,24)
                mgssp    => ssp(:,:,25)
                nassp    => ssp(:,:,26)
                kassp    => ssp(:,:,27)
                mnssp    => ssp(:,:,28)
                clssp    => ssp(:,:,29)
                fe3ssp    => ssp(:,:,30)
                mn3ssp    => ssp(:,:,31)
                sidssp    => ssp(:,:,32)
                ch4ssp    => ssp(:,:,33)
                feoh3ssp    => ssp(:,:,34)
                aloh3ssp    => ssp(:,:,35)
                cdssp    => ssp(:,:,36)
                pbssp    => ssp(:,:,37)
                znssp    => ssp(:,:,38)
                arsssp    => ssp(:,:,39)
                pyritessp    => ssp(:,:,40)
                po4precipssp    => ssp(:,:,41)

     hname =  (/'horizontal_velocity, m/s',                        &
     &          ' vertical_velocity, mm/s',                        &
     &          '   temperature, ?c      ',                            &
     &          '   limiting timestep    '/)
      cname2 = (/'    tracer, g/m^3      ',                            &   !1
     &             'suspended solids, g/m^3',                            &   !2
     &             '    coliform, g/m^3    ',                            &   !3
     &             'dissolved solids, g/m^3',                            &   !4
     &             '   labile dom, g/m^3   ',                            &   !5
     &             ' refractory dom, g/m^3 ',                            &   !6
     &             '      algae, g/m^3     ',                            &   !7
     &             '   labile pom, g/m^3   ',                           &   !8
     &             '   phosphorus, mg/m^3  ',                           &   !9
     &             '    ammonium, mg/m^3   ',                           &   !10
     &             'nitrate-nitrite, mg/m^3',                           &   !11
     &             'dissolved oxygen, g/m^3',                           &   !12
     &             '    sediment, g/m^3    ',                           &   !13
     &             'inorganic carbon, g/m^3',                           &   !14
     &             '   alkalinity, g/m^3   ',                           &   !15
     &             '           ph          ',                           &   !16
     &             ' carbon dioxide, g/m^3 ',                           &   !17
     &             '   bicarbonate, g/m^3  ',                           &   !18
     &             '    carbonate, g/m^3   ',                           &   !19
     &             '      iron, g/m^3      ',                           &   !20
     &             '      cbod, g/m^3      ',                           &   !21
     &             '    aluminium, g/m^3   ',                            &   !22 mueller
     &             '    sulfate, g/m^3     ',                            &   !23 mueller
     &             '    calcium, g/m^3     ',                            &   !24 mueller
     &             '    magnesium, g/m^3   ',                            &   !25 mueller
     &             '     sodium, g/m^3     ',                            &   !26 mueller
     &             '    potassium,  g/m^3  ',                            &   !27 mueller
     &             '     manganese, g/m^3  ',                            &   !28 mueller
     &             '      chlorid, g/m^3   ',                            &   !29 mueller
     &             '     iron iii, g/m^3   ',                            &   !30 mueller
     &             '  manganese iii, g/m^3 ',                            &   !31 mueller
     &             '     sulfide, g/m^3    ',                            &   !32 mueller
     &             '     methane, g/m^3    ',                            &   !33 mueller
     &             '  iron hydroxide, g/m^3',                            &   !34 mueller
     &             ' al. hydroxide, g/m^3  ',                            &   !35 mueller
     &             '     cadmium, g/m^3    ',                            &   !36 mueller
     &             '       lead, g/m^3     ',                            &   !37 mueller
     &             '       zinc, g/m^3     ',                            &   !38 mueller
     &             '     arsenic, g/m^3    '/)                                !39 mueller

!<mueller>
!        <date>2001/02/06</date>
!        <reason>adding more constituents which are only transported
!                reactions are done by phreeqc only</reason>
!</mueller>
        cunit1 = (/'g/m^3 ',                                             & !  1
     &             'g/m^3 ',                                             & !  2
     &             'g/m^3 ',                                             & !  3
     &             'g/m^3 ',                                             & !  4
     &             'g/m^3 ',                                             & !  5
     &             'g/m^3 ',                                             & !  6
     &             'g/m^3 ',                                             & !  7
     &             'g/m^3 ',                                             & !  8
     &             'mg/m^3',                                             & !  9
     &             'mg/m^3',                                             & ! 10
     &             'mg/m^3',                                             & ! 11
     &             'g/m^3 ',                                             & ! 12
     &             'g/m^3 ',                                             & ! 13
     &             'g/m^3 ',                                             & ! 14
     &             'g/m^3 ',                                             & ! 15
     &             '      ',                                             & ! 16
     &             'g/m^3 ',                                             'g/m^3 ', 'g/m^3 ', 'mg/m^3', 'g/m^3 ', 'g/m^3 ',        &
     &             'g/m^3 ', 'g/m^3 ', 'g/m^3 ', 'g/m^3 ', 'g/m^3 ', 'g/m^3 ', 'g/m^3 ', &
     &             'g/m^3 ', 'g/m^3 ', 'g/m^3 ', 'g/m^3 ', 'g/m^3 ', 'g/m^3 ',          &
     &             'g/m^3 ', 'g/m^3 ', 'g/m^3 ', 'g/m^3 '/)
        snp =  20
        nonzero = 1.0e-20


! mmueller
! setting all arrays to 0, 0d0, '  ', or .false.
! to prevent unpredictiable behaviour of
! uninitilized variables

 cprc = ' '
 cunit1 = ' '
 cname2 = ' '
 qtrfn = ' '
 ttrfn = ' '
 ctrfn = ' '
 qinfn = ' '
 tinfn = ' '
 cinfn = ' '
 qotfn = ' '
 qdtfn = ' '
 tdtfn = ' '
 cdtfn = ' '
 prefn = ' '
 tprfn = ' '
 cprfn = ' '
 euhfn = ' '
 tuhfn = ' '
 cuhfn = ' '
 edhfn = ' '
 tdhfn = ' '
 cdhfn = ' '
 lfac = ' '
 lfpr = ' '
 conv = ' '
 limitingCause = 0d0

 kb = 0
 kti = 0
 ipr = 0
 cn = 0
 trcn = 0
 dtcn = 0
 prcn = 0
 uhcn = 0
 dhcn = 0
 incn = 0
 nout = 0
 nstr = 0
 cus = 0
 ds = 0
 kbsw = 0
 kout = 0
 otq = 0
 trq = 0
 trt = 0
 tribconc = 0
 dtq = 0
 dtt = 0
 dtc = 0
 pre = 0
 prt = 0
 prc = 0
 uhe = 0
 uht = 0
 uhc = 0
 dhe = 0
 dht = 0
 dhc = 0
 inq = 0
 intemp = 0
 inc = 0


 ice = .false.
 dist_tribs = .false.
 up_flow = .false.
 dn_flow = .false.
 uh_internal = .false.
 dh_internal = .false.
 uh_external = .false.
 dh_external = .false.
 sel_withdrawal = .false.
 point_sink = .false.

 algaenosettlingss = 0d0
 algaewithsettlingss = 0d0
 tds = 0d0
 cssk = 0d0
 c1 = 0d0
 c2 = 0d0
 ssp = 0d0
 ssgw = 0d0
 ssgw_out = 0d0
 ssload = 0d0
 ss = 0d0
 col = 0d0
 tds = 0d0
 ldom = 0d0
 rdom = 0d0
 algae = 0d0
 lpom = 0d0
 po4 = 0d0
 nh4 = 0d0
 no3 = 0d0
 dox = 0d0
 sed = 0d0
 tic = 0d0
 alkal = 0d0
 ph = 0d0
 co2 = 0d0
 hco3 = 0d0
 co3 = 0d0
 fe = 0d0
 cbod = 0d0
 tra = 0d0
 al = 0d0
 so4 = 0d0
 ca = 0d0
 mg = 0d0
 na = 0d0
 ka = 0d0
 mn = 0d0
 cl = 0d0
 fe3 = 0d0
 mn3 = 0d0
 sid = 0d0
 ch4 = 0d0
 feoh3 = 0d0
 aloh3 = 0d0
 cd = 0d0
 pb = 0d0
 zn = 0d0
 ars = 0d0
 pyrite = 0d0
 po4precip = 0d0

 ssss = 0d0
 colss = 0d0
 ldomss = 0d0
 rdomss = 0d0
 algss = 0d0
 lpomss = 0d0
 po4ss = 0d0
 nh4ss = 0d0
 no3ss = 0d0
 doss = 0d0
 ticss = 0d0
 co3ss = 0d0
 fess = 0d0
 cbodss = 0d0
 trass = 0d0
 alss = 0d0
 so4ss = 0d0
 cass = 0d0
 mgss = 0d0
 nass = 0d0
 kass  = 0d0
 mnss = 0d0
 clss = 0d0
 fe3ss = 0d0
 mn3ss = 0d0
 sidss = 0d0
 ch4ss = 0d0
 feoh3ss = 0d0
 aloh3ss = 0d0
 cdss = 0d0
 pbss = 0d0
 znss = 0d0
 arsss = 0d0
 pyritess = 0d0
 po4precipss = 0d0

 ssssp = 0d0
 colssp = 0d0
 ldomssp = 0d0
 rdomssp = 0d0
 algssp = 0d0
 lpomssp = 0d0
 po4ssp = 0d0
 nh4ssp = 0d0
 no3ssp = 0d0
 dossp = 0d0
 ticssp = 0d0
 co3ssp = 0d0
 fessp = 0d0
 cbodssp = 0d0
 trassp = 0d0
 alssp = 0d0
 so4ssp = 0d0
 cassp = 0d0
 mgssp = 0d0
 nassp = 0d0
 kassp = 0d0
 mnssp = 0d0
 clssp = 0d0
 fe3ssp = 0d0
 mn3ssp = 0d0
 sidssp = 0d0
 ch4ssp = 0d0
 feoh3ssp = 0d0
 aloh3ssp = 0d0
 cdssp = 0d0
 pbssp = 0d0
 znssp = 0d0
 arsssp  = 0d0
 pyritessp = 0d0
 po4precipssp = 0d0

 ssssgw = 0d0
 colssgw = 0d0
 ldomssgw = 0d0
 rdomssgw = 0d0
 algssgw = 0d0
 lpomssgw = 0d0
 po4ssgw = 0d0
 nh4ssgw = 0d0
 no3ssgw = 0d0
 dossgw = 0d0
 ticssgw = 0d0
 fessgw = 0d0
 cbodssgw = 0d0
 trassgw = 0d0
 alssgw = 0d0
 so4ssgw = 0d0
 cassgw = 0d0
 mgssgw = 0d0
 nassgw = 0d0
 kassgw = 0d0
 mnssgw = 0d0
 clssgw = 0d0
 mn3ssgw = 0d0
 sidssgw = 0d0
 ch4ssgw = 0d0
 fe3ssgw = 0d0
 cdssgw = 0d0
 pbssgw = 0d0
 znssgw = 0d0
 arsssgw = 0d0
! ssssgwout = 0.0d0
 colssgwout = 0.0d0
 ldomssgwout = 0.0d0
 rdomssgwout = 0.0d0
! algssgwout = 0.0d0
! lpomssgwout = 0.0d0
 po4ssgwout = 0.0d0
 nh4ssgwout = 0.0d0
 no3ssgwout = 0.0d0
 dossgwout = 0.0d0
 ticssgwout = 0.0d0
 fessgwout = 0.0d0
! cbodssgwout = 0.0d0
 trassgwout = 0.0d0
 alssgwout = 0.0d0
 so4ssgwout = 0.0d0
 cassgwout = 0.0d0
 mgssgwout = 0.0d0
 nassgwout = 0.0d0
 kassgwout = 0.0d0
 mnssgwout = 0.0d0
 clssgwout = 0.0d0
 fe3ssgwout = 0.0d0
 mn3ssgwout = 0.0d0
 sidssgwout = 0.0d0
 ch4ssgwout = 0.0d0
 cdssgwout = 0.0d0
 pbssgwout = 0.0d0
 znssgwout = 0.0d0
 arsssgwout = 0.0d0
 pcgq = 0d0
 el = 0d0
 h = 0d0
 hkt1 = 0d0
 hkt2 = 0d0
 bkt = 0d0
 bhkt1 = 0d0
 bhkt2 = 0d0
 bhrkt1 = 0d0
 iceth = 0d0
 sod = 0d0
 depthm = 0d0
 wsc = 0d0
 eluh = 0d0
 eldh = 0d0
 qin = 0d0
 pr = 0d0
 qdtr = 0d0
 qsum = 0d0
 tin = 0d0
 tpr = 0d0
 tdtr = 0d0
 qwd = 0d0
 qtr = 0d0
 ttr = 0d0
 lpomd = 0d0
 sedd = 0d0
 domd = 0d0
 sodd = 0d0
 nh4d = 0d0
 no3d = 0d0
 cbodd = 0d0
 omrm = 0d0
 nh4rm = 0d0
 no3rm = 0d0
 armr = 0d0
 armf = 0d0
 setin = 0d0
 setout = 0d0
 a1 = 0d0
 a2 = 0d0
 a3 = 0d0
 agr = 0d0
 arr = 0d0
 amr = 0d0
 aer = 0d0
 fpss = 0d0
 fpfe = 0d0
 t1 = 0d0
 t2 = 0d0
 az = 0d0
 u = 0d0
 w  = 0d0
 rho = 0d0
 ndlt = 0d0
 b = 0d0
 bh = 0d0
 qout  = 0d0
 tuh = 0d0
 tdh = 0d0
 cin = 0d0
 cdtr = 0d0
 cpr = 0d0
 ctr = 0d0
 qstr = 0d0
 estr = 0d0
 wstr = 0d0
 cuh = 0d0
 cdh = 0d0
 ctrnx = 0d0
 cinnx = 0d0
 qoutnx = 0d0
 cdtrnx = 0d0
 cprnx = 0d0
 tuhnx = 0d0
 tdhnx = 0d0
 qstrnx = 0d0
 cuhnx = 0d0
 cdhnx = 0d0
 qdtrnx = 0d0
 tdtrnx = 0d0
 prnx = 0d0
 tprnx = 0d0
 eluhnx = 0d0
 eldhnx = 0d0
 qwdnx = 0d0
 qtrnx = 0d0
 ttrnx = 0d0
 qinnx = 0d0
 tinnx = 0d0
 ctro = 0d0
 cino = 0d0
 qouto = 0d0
 cdtro = 0d0
 tuho = 0d0
 tdho = 0d0
 qstro = 0d0
 cuho = 0d0
 cdho  = 0d0
 qdtro = 0d0
 tdtro = 0d0
 eluho = 0d0
 eldho = 0d0
 qwdo = 0d0
 qtro = 0d0
 ttro = 0d0
 qino = 0d0
 tino = 0d0
 nxqtr1 = 0d0
 nxttr1 = 0d0
 nxctr1 = 0d0
 nxqin1 = 0d0
 nxtin1 = 0d0
 nxcin1 = 0d0
 nxqdt1 = 0d0
 nxtdt1 = 0d0
 nxcdt1 = 0d0
 nxpr1 = 0d0
 nxtpr1 = 0d0
 nxcpr1 = 0d0
 nxeuh1 = 0d0
 nxtuh1 = 0d0
 nxcuh1 = 0d0
 nxedh1 = 0d0
 nxtdh1 = 0d0
 nxcdh1 = 0d0
 nxqot1 = 0d0
 nxqtr2 = 0d0
 nxttr2 = 0d0
 nxctr2 = 0d0
 nxqin2 = 0d0
 nxtin2 = 0d0
 nxcin2 = 0d0
 nxqdt2 = 0d0
 nxtdt2 = 0d0
 nxcdt2 = 0d0
 nxpr2 = 0d0
 nxtpr2 = 0d0
 nxcpr2 = 0d0
 nxeuh2 = 0d0
 nxtuh2 = 0d0
 nxcuh2 = 0d0
 nxedh2 = 0d0
 nxtdh2 = 0d0
 nxcdh2 = 0d0
 nxqot2 = 0d0
 z = 0d0
 limitingFactor = 0.0d0

 ! more to zeorded
 call set_all_zero

 end subroutine set_pointers
