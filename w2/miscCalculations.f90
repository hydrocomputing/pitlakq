    subroutine misccalculations()
        use shared_data
        implicit none
        double precision:: density

        include 'formats_main.inc'

        vactive = 0.0d0
        surfacearea = 0.0d0
        outer: do jb=1,nbp
           iu = cus(jb)
           id = ds(jb)
           inner1: do i=iu,id
                inner2: do k=kt+1,kb(i)
                    vactive(k,i) = bh(k,i) * dlx(i)
                    hactive(k,i) = h(k)
                enddo inner2
                vactive(kt,i) = bhkt2(i) * dlx(i)
                hactive(kt,i) = hkt2(i)
                surfacearea = surfacearea + b(kt,i) * dlx(i)
            enddo inner1
         enddo outer

!***********************************************************************
!*        task 2.5: layer - segment additions and subtractions        **
!***********************************************************************

!$message:'    layer/segment additions and subtractions'
!****** water surface minimum thickness

        zmin = -1000.0
        do jb=1,nbp
          do i=cus(jb),ds(jb)
            zmin = max(zmin,z(i))
          end do
        end do
        add_layer = zmin.lt.-0.80*h(kt-1).and.kt.ne.2
        sub_layer = zmin.gt.0.60*h(kt)


!****** add layers

        do while (add_layer)
          if (snapshot) write (snp,4000) kt-1,jday

!******** variable initialization

          kt = kt-1
          do jb=1,nbp
            iu = cus(jb)
            id = ds(jb)
            do i=iu-1,id+1
              z(i)     = h(kt)+z(i)
              u(kt,i)  = u(kt+1,i)
              w(kt,i)  = w(kt+1,i)*(bb(kt+1,i)/bb(kt,i))
     !mmueller
              if (b(kt,i).le.thin_cell_limit) then
                  u(kt,i) = u(kt,i)/thin_cell_reduction
                  w(kt,i) = w(kt,i)/thin_cell_reduction
              end if
     !mmueller
              hkt1(i)  = h(kt)-z(i)
              bhkt1(i) = bhkt1(i)-bh(kt+1,i)
              bkt(i)   = bhkt1(i)/hkt1(i)
              t1(kt,i) = t1(kt+1,i)
              do jc=1,nac
                c1(kt,i,cn(jc))   = c1(kt+1,i,cn(jc))
                cssk(kt,i,cn(jc)) = cssk(kt+1,i,cn(jc))
              end do
              if (constituents) then
                rho(kt,i) = density (t2(kt,i),ss(kt,i),tds(kt,i))
              else
                rho(kt,i) = density (t2(kt,i),0.0,0.0)
              end if
            end do

            do i=iu-1,id
              bhrkt1(i) = (bhkt1(i)+bhkt1(i+1))*0.5
            end do
            if (up_head(jb)) then
              bhsum           = bhrkt1(iu-1)+bhr(kt+1,iu-1)
              quh1(kt,jb)     = quh1(kt+1,jb)*bhrkt1(iu-1)/bhsum
              quh1(kt+1,jb)   = quh1(kt+1,jb)*bhr(kt+1,iu-1)/bhsum
              tssuh1(kt,jb)   = tssuh1(kt+1,jb)*bhrkt1(iu-1)/bhsum
              tssuh1(kt+1,jb) = tssuh1(kt+1,jb)*bhr(kt+1,iu-1)/bhsum
              do jc=1,nac
                cssuh1(kt,cn(jc),jb)   = cssuh1(kt+1,cn(jc),jb)            &
     &                                   *bhrkt1(iu-1)/bhsum
                cssuh1(kt+1,cn(jc),jb) = cssuh1(kt+1,cn(jc),jb)            &
     &                                   *bhr(kt+1,iu-1)/bhsum
              end do
            end if
            if (dn_head(jb)) then
              bhsum           = bhrkt1(id)+bhr(kt+1,id)
              qdh1(kt,jb)     = qdh1(kt+1,jb)*bhrkt1(id)/bhsum
              qdh1(kt+1,jb)   = qdh1(kt+1,jb)*bhr(kt+1,id)/bhsum
              tssdh1(kt,jb)   = tssdh1(kt+1,jb)*bhrkt1(id)/bhsum
              tssdh1(kt+1,jb) = tssdh1(kt+1,jb)*bhr(kt+1,id)/bhsum
              do jc=1,nac
                cssdh1(kt,cn(jc),jb)   = cssdh1(kt+1,cn(jc),jb)            &
     &                                   *bhrkt1(id)/bhsum
                cssdh1(kt+1,cn(jc),jb) = cssdh1(kt+1,cn(jc),jb)            &
     &                                   *bhr(kt+1,id)/bhsum
              end do
            end if
            do i=iu,id-1
              dx(kt,i) = dxi
            end do
            iut = iu
            idt = id-1
            if (up_head(jb)) iut = iu-1
            if (dn_head(jb)) idt = id
            do i=iut,idt
              az(kt,i)  = azmin
              saz(kt,i) = azmin
            end do

!********** upstream active segment

            iut = us(jb)
            do i=us(jb),ds(jb)
              if (kb(i)-kt.lt.nl(jb)-1) iut = i+1
            end do

!********** segment addition

            if (iut.ne.iu) then
              if (snapshot) write (snp,4010) iut,iu-1,jday
              do i=iut-1,iu-1
                z(i)     = z(iu)
                kti(i)   = kti(iu)
                hkt1(i)  = h(kt)-z(iu)
                bhkt1(i) = b(kti(i),i)*(el(kt)-el(kti(i)+1)-z(i))
                bkt(i)   = bhkt1(i)/hkt1(i)
              end do
              do i=iut-1,iu-1
                bhrkt1(i) = (bhkt1(i)+bhkt1(i+1))*0.5
              end do
              do i=iut,iu-1
                ice(i)   = ice(iu)
                iceth(i) = iceth(iu)
                do k=kt,kb(i)
                  dx(k,i) = dxi
                  t1(k,i) = t1(k,iu)
                  t2(k,i) = t1(k,iu)


                  u(k,i)  = u(k,iu-1)
          !mmueller
                  if (b(k,i).le.thin_cell_limit) then
                      u(k,i) = u(k,i)/thin_cell_reduction
                  end if
          !mmueller
                  su(k,i) = u(k,iu-1)
                  do jc=1,nac
                    bht = bh(k,i)
                    if (k.eq.kt) bht = bhkt1(i)
                    c1(k,i,cn(jc))   = c1(k,iu,cn(jc))
                    c2(k,i,cn(jc))   = c1(k,iu,cn(jc))
                    cmbrt(cn(jc),jb) = cmbrt(cn(jc),jb)+c1(k,iu,cn(jc))    &
     &                                 *dlx(i)*bht
                  end do
                end do

                do k=kt,kb(i)-1
                  az(k,i)  = az(k,iu)
                  saz(k,i) = az(k,iu)
                end do
              end do
              do k=kb(iut),kb(iu)
                u(k,iu-1)  = 0.0d0
                su(k,iu-1) = 0.0d0
                tadl(k,iu) = 0.0d0
                if (constituents) then
                  do jac=1,nac
                    cadl(k,iu,cn(jac)) = 0.0d0
                  end do
                end if
              end do
              if (shift_demand) then
                idiff = iut-us(jb)
                do i=iut,id
                  sod(i) = sods(i-idiff)
                end do
              end if
              iu      = iut
              cus(jb) = iu
              if (uh_external(jb)) kb(iu-1) = kb(iu)
              if (uh_internal(jb)) kb(iu-1) = min(kb(uhs(jb)),kb(iu))
              if (up_head(jb)) then
                do k=kt,kb(iu-1)-1
                  az(kt,iu-1)  = azmin
                  saz(kt,iu-1) = azmin
                end do
              end if
            end if

!********** total active cells and single layers

            do i=iu,id
              ntac         = ntac+1
              one_layer(i) = kt.eq.kb(i)
            end do
            ntacmx = max(ntac,ntacmx)
          end do

!******** additional layers

          zmin = -1000.0
          do jb=1,nbp
            do i=cus(jb),ds(jb)
              zmin = max(zmin,z(i))
            end do
          end do
          add_layer = zmin.lt.-0.80*h(kt-1).and.kt.ne.2
        end do

!****** subtract layers

        do while (sub_layer)
          if (snapshot) write (snp,4020) kt,jday

!******** variable initialization

          kt = kt+1
          do jb=1,nbp
            iu = cus(jb)
            id = ds(jb)
            do i=iu-1,id+1
              a1(kt-1,i) = 0.0d0
            end do
            do i=iu-1,id+1
              z(i)     = z(i)-h(kt-1)
              hkt1(i)  = h(kt)-z(i)
              bhkt1(i) = bhkt1(i)+bh(kt,i)
              bkt(i)   = bhkt1(i)/hkt1(i)
              t1(kt,i) = (t1(kt-1,i)*(bhkt1(i)-bh(kt,i))                &
     &                   +t1(kt,i)*bh(kt,i))/bhkt1(i)
              do jc=1,nac
                jac              = cn(jc)
                c1(kt,i,jac)     = (c1(kt-1,i,jac)*(bhkt1(i)-bh(kt,i))    &
     &                             +c1(kt,i,jac)*bh(kt,i))/bhkt1(i)
                cssb(kt,i,jac)   = cssb(kt-1,i,jac)+cssb(kt,i,jac)
                cssk(kt,i,jac)   =(cssk(kt-1,i,jac)*(bhkt1(i)-bh(kt,i)) &
     &                             +cssk(kt,i,jac)*bh(kt,i))/bhkt1(i)
                cssb(kt-1,i,jac) = 0.0d0
                cssk(kt-1,i,jac) = 0.0d0
              end do
            end do
            do i=iu-1,id
              bhrkt1(i) = (bhkt1(i)+bhkt1(i+1))*0.5
            end do
            if (up_head(jb)) then
              quh1(kt,jb)   = quh1(kt,jb)+quh1(kt-1,jb)
              tssuh1(kt,jb) = tssuh1(kt-1,jb)+tssuh1(kt,jb)
              do jc=1,nac
                cssuh1(kt,cn(jc),jb) = cssuh1(kt-1,cn(jc),jb)            &
     &                                 +cssuh1(kt,cn(jc),jb)
              end do
            end if
            if (dn_head(jb)) then
              qdh1(kt,jb)   = qdh1(kt,jb)+qdh1(kt-1,jb)
              tssdh1(kt,jb) = tssdh1(kt-1,jb)+tssdh1(kt,jb)
              do jc=1,nac
                cssdh1(kt,cn(jc),jb) = cssdh1(kt-1,cn(jc),jb)            &
     &                                 +cssdh1(kt,cn(jc),jb)
              end do
            end if

!********** upstream active segment

            iut = us(jb)
            do i=us(jb),ds(jb)
              if (kb(i)-kt.lt.nl(jb)-1) iut = i+1
              one_layer(i) = kt.eq.kb(i)
            end do
            if (iut.gt.ds(jb)-1) then
              open  (err,file='err.opt',status='unknown')
              write (err,7000) jb,jday,kt,iut,ds(jb)-1,kb(i),kt, elws(i)
              write (err,*) 'cell error 2'
              write (*,*) 'cell error 2'
              error = 1
              return
            end if

!********** segment subtraction

            if (iut.ne.iu) then
              if (snapshot) write (snp,4030) iu,iut-1,jday
              do i=iu,iut-1
                do jc=1,nac
                  vol              = bhkt1(i)*dlx(i)
                  cmbrt(cn(jc),jb) = cmbrt(cn(jc),jb)-c1(kt,i,cn(jc))    &
     &                               *vol+(cssb(kt,i,cn(jc))            &
     &                               +cssk(kt,i,cn(jc))*vol)*dlt
                  do k=kt+1,kb(i)
                    vol              = bh(k,i)*dlx(i)
                    cmbrt(cn(jc),jb) = cmbrt(cn(jc),jb)-c1(k,i,cn(jc))    &
     &                                 *vol+(cssb(k,i,cn(jc))            &
     &                                 +cssk(k,i,cn(jc))*vol)*dlt
                  end do
                end do
              end do
              do i=iu-1,iut-1
                f(i)     = 0.0d0
                z(i)     = 0.0d0
                iceth(i) = 0.0d0
                bhrho(i) = 0.0d0
                ice(i)   = .false.
                do k=kt,kb(i)
                  az(k,i)   = 0.0d0
                  saz(k,i)  = 0.0d0
                  dx(k,i)   = 0.0d0
                  u(k,i)    = 0.0d0
                  t1(k,i)   = 0.0d0
                  tadl(k,i) = 0.0d0
                  qss(k,i)  = 0.0d0
                  do jc=1,nac
                    c1(k,i,cn(jc))   = 0.0d0
                    c2(k,i,cn(jc))   = 0.0d0
                    c1s(k,i,cn(jc))  = 0.0d0
                    cadl(k,i,cn(jc)) = 0.0d0
                    cssb(k,i,cn(jc)) = 0.0d0
                    cssk(k,i,cn(jc)) = 0.0d0
                  end do
                end do
              end do
              if (shift_demand) then
                idiff = iut-us(jb)
                do i=iut,id
                  sod(i) = sods(i-idiff)
                end do
              end if
              iu           = iut
              cus(jb)      = iu
              z(iu-1)      = z(iu)
              kti(iu-1)    = kti(iu)
              hkt1(iu-1)   = hkt1(iu)
              bhkt1(iu-1)  = b(kti(iu),iu-1)*el(kt)-(el(kti(i)+1)-z(iu))
              bkt(iu-1)    = bhkt1(iu-1)/hkt1(iu-1)
              bhrkt1(iu-1) = (bhkt1(iu-1)+bhkt1(iu))*0.5
              if (uh_external(jb)) kb(iu-1) = kb(iu)
              if (uh_internal(jb)) kb(iu-1) = min(kb(uhs(jb)),kb(iu))
            end if

!********** total active cells

            do i=iu,id
              ntac = ntac-1
            end do
            ntacmn = min(ntac,ntacmn)
          end do

!******** additional layer subtractions

          zmin = -1000.0
          do jb=1,nbp
            do i=cus(jb),ds(jb)
              zmin = max(zmin,z(i))
            end do
          end do
          sub_layer = zmin.gt.0.60*h(kt)
        end do

!***********************************************************************
!*                        task 2.6: balances                          **
!***********************************************************************

!$message:'    balances'
        if (volume_balance) then
          do jb=1,nbp
            volsbr(jb) = volsbr(jb)+dlvol(jb)
!<mueller>
!    <date>2001/02/06></date>
!    <reason>adding gw volume to total volume</reason>
!</mueller>
            voltbr(jb) = volev(jb)+volpr(jb)+voltr(jb)+voldt(jb)        &
     &                   +volwd(jb)+voluh(jb)+voldh(jb)+volin(jb)        &
     &                   +volout(jb)+volgwin(jb)+volgwout(jb)+volload(jb) !mueller
            if (abs(volsbr(jb)-voltbr(jb)).gt.vtol) then
              if (volume_warning) then
                warning_open = .true.
                write (wrn,6010) jday,volsbr(jb),voltbr(jb),volsbr(jb)    &
     &                           -voltbr(jb)
                volume_warning = .false.
              end if
            end if
          end do
        end if
        if (energy_balance) then
          do jb=1,nbp
            etbr(jb) = eibr(jb)+tssev(jb)+tsspr(jb)+tsstr(jb)            &
     &                 +tssdt(jb)+tsswd(jb)+tssuh(jb)+tssdh(jb)            &
     &                 +tssin(jb)+tssout(jb)+tsss(jb)+tssb(jb)            &
     &                 +tssice(jb)+tssgwin(jb)+tssgwout(jb)+tssload(jb)
            esbr(jb) = 0.0d0
            do i=cus(jb),ds(jb)
              esbr(jb) = esbr(jb)+t1(kt,i)*dlx(i)*bhkt1(i)
              do k=kt+1,kb(i)
                esbr(jb) = esbr(jb)+t1(k,i)*dlx(i)*bh(k,i)
              end do
            end do
          end do
        end if
        if (mass_balance) then
          do jb=1,nbp
            do jc=1,nac
              jac = cn(jc)
              if (transport(jac)) then
                cmbrs(jac,jb) = 0.0d0
                do i=cus(jb),ds(jb)
                  cmbrs(jac,jb) = cmbrs(jac,jb)+c1(kt,i,jac)*dlx(i)        &
     &                            *bhkt1(i)
                  cmbrt(jac,jb) = cmbrt(jac,jb)+(cssb(kt,i,jac)            &
     &                            +cssk(kt,i,jac)*bhkt1(i)*dlx(i))*dlt
                  do k=kt+1,kb(i)
                    cmbrs(jac,jb) = cmbrs(jac,jb)+c1(k,i,jac)*dlx(i)    &
     &                              *bh(k,i)
                    cmbrt(jac,jb) = cmbrt(jac,jb)+(cssb(k,i,jac)        &
     &                              +cssk(k,i,jac)*bh(k,i)*dlx(i))*dlt
                  end do
                end do
              end if
            end do
          end do
        end if


!***********************************************************************
!*           task 2.7: variable updates for next timestep             **
!***********************************************************************


!$message:'    variable updates'
        do jb=1,nbp
          iu = cus(jb)
          id = ds(jb)
          do i=iu-1,id+1
            sz(i)     = z(i)
            skti(i)   = kti(i)
            hkt2(i)   = hkt1(i)
            avhkt(i)  = (hkt2(i)+h(kt+1))*0.5
            bhkt2(i)  = bhkt1(i)
            bhrkt2(i) = bhrkt1(i)
            if (hkt2(i).le.0.0) then
              open  (err,file='err.opt',status='unknown')
              write (err,7010) jday,i,hkt2(i)
              write(*,*) 'error'
              error = 0
              return
            end if
            do k=kt,max(kb(iu),kb(i))
              t2(k,i)  = t1(k,i)
              su(k,i)  = u(k,i)
              sw(k,i)  = w(k,i)
              saz(k,i) = az(k,i)
              qss(k,i) = 0.0d0
              tss(k,i) = 0.0d0
            end do
          end do
          if (uh_internal(jb)) then
            do k=kt,kb(iu-1)
              quh2(k,jb)   = quh1(k,jb)*dlt
              tssuh2(k,jb) = tssuh1(k,jb)*dlt
            end do
          end if
          if (dh_internal(jb)) then
            do k=kt,kb(id+1)
              qdh2(k,jb)   = qdh1(k,jb)*dlt
              tssdh2(k,jb) = tssdh1(k,jb)*dlt
            end do
          end if
          do jc=1,nac
            do i=iu-1,id+1
              do k=kt,kb(i)
                c1s(k,i,cn(jc))  = c1(k,i,cn(jc))
                c2(k,i,cn(jc))   = max(c1(k,i,cn(jc)),0.0)
                cssb(k,i,cn(jc)) = 0.0d0
              end do
            end do
            if (uh_internal(jb)) then
              do k=kt,kb(iu-1)
                cssuh2(k,cn(jc),jb) = cssuh1(k,cn(jc),jb)*dlt
              end do
            end if
            if (dh_internal(jb)) then
              do k=kt,kb(id+1)
                cssdh2(k,cn(jc),jb) = cssdh1(k,cn(jc),jb)*dlt
              end do
            end if
          end do
        end do
        elkt = el(kt)-z(ds(1))
!****** time variable updates
        nit     = nit+1
        eltm    = eltm+dlt
        jday    = eltm/86400.0
        eltmjd  = jday-tmstrt
        end_run = jday.ge.tmend
        dlts    = dlt
        dlt     = int(max(dltmin,dltf(dltdp)*curmax))
        dlt     = min(dlt,dlts+1.0*dlts)
        dlt     = min(dlt,dlttvd)
        dltav   = (eltm-tmstrt*86400.0)/nit
        if (jday.ge.wscd(wscdp+1)) wscdp = wscdp+1
        if (jday.ge.dltd(dltdp+1)) dltdp = dltdp+1
        if (dlt.gt.dltmax(dltdp))  dlt   = dltmax(dltdp)
        if (dlts.lt.mindlt.and.dlts.ne.dlttvds) then
          mindlt = dlts
          jdmin  = jday
        end if
        curmax = dltmax(dltdp)/dltf(dltdp)
        if (int(jday).eq.jdaynx) then
          jdayg  = jdayg+1
          jdaynx = jdaynx+1
        end if
        if (jdayg.gt.300) winter = .true.
        if (jdayg.lt.40)  winter = .false.
        write (gdch,'(i3)') gday
        call gregorian_date (year)
        if (constituents) then
          update_kinetics = .false.
          if (mod(nit,frequk).eq.0) update_kinetics = .true.
        end if


    end subroutine
