    subroutine cleanup() 
        use shared_data
        implicit none
!        double precision:: density

        include 'formats_main.inc'
        


!***********************************************************************
!*                      task 3: end simulation                        **
!***********************************************************************

      if (snapshot) then
        call date_time (cdate,ctime)                                    !fortran
        write (snp,*)                                                   !fortran
        write (snp,4040) 'normal termination at '//ctime//' on '//cdate !fortran
        write (snp,*)
        write (snp,4050) 'runtime statistics'
        write (snp,4060) '  grid',imp,kmp,ntacmx,ntacmn
        write (snp,4070) '    segment lengths      =',dlxmin,dlxmax
        write (snp,4080) '    layer heights        =',hmin,hmax
        write (snp,4090) '  timestep'
        write (snp,4100) '    total iterations     =',nit
        write (snp,4100) '    # of violations      =',nv
        write (snp,4110) '    average timestep     =',int(dltav)
        write (snp,4120) '    simulation time      =',int(eltmjd),        &
     &                   (eltmjd-int(eltmjd))*24.0
        write (snp,'(a3)')  esc//'e'
        close (snp)
        
      end if
      if (time_series)  close (tsr)
      if (vector)       close (vpl)
      if (profile)      close (prf)
      if (spreadsheet)  close (spr)
      if (contour)      close (cpl)
      if (.not.warning_open) then
        close (wrn,status='delete')
      else
        close (wrn)
      end if
      close (err,status='delete')
      close(met)
      if (withdrawals) then
            close (wdq)
          end if
          if (tributaries) then
            do jt=1,ntr
              close (trq(jt))
              close (trt(jt))

              if (constituents) then
                close (tribconc(jt))
              end if
            end do
          end if
          do jb=1,nbp
            if (up_flow(jb)) then
               close (inq(jb))
              close (intemp(jb))
              if (constituents) then
                close (inc(jb))
              end if
            end if
            if (dn_flow(jb)) then
              close (otq(jb))
            end if
            if (precipitation) then
              close (pre(jb))
              close (prt(jb))
              if (constituents) then
                 close(prc(jb))
              end if
            end if
            if (dist_tribs(jb)) then
              close (dtq(jb))
              close (dtt(jb))
              if (constituents) then
                close (dtc(jb))
              end if
            end if
            if (uh_external(jb)) then
              close (uhe(jb))
              close (uht(jb))
              if (constituents) then
                close (uhc(jb))
              end if
            end if
            if (dh_external(jb)) then
              close (dhe(jb))
              close (dht(jb))
              if (constituents) then
                close (dhc(jb))
              end if
            end if

          end do
      open_files = .true.

	  deallocate(cunit3)
	  deallocate(acc, inacc, tracc, dtacc, pracc)
      deallocate(dtrc, swc) 
      deallocate(cunit2)
      deallocate(cname1)
      deallocate(segment)
      deallocate(trc)
!      deallocate(rsifn(nbp),  vprfn(nbp), tsrfn(nbp), prffn(nbp), &
!     &               vplfn(nbp), cplfn(nbp), snpfn(nbp),    &
!     &             lprfn(nbp),  bthfn(nbp), sprfn(nbp))
      deallocate(sink)
      deallocate(conv1)  

!	  deallocate (cn0)
      deallocate (skti, kbmin)
      deallocate (iprf, ispr)    
      deallocate (ipri)
      deallocate (iwd, kwd, jbwd)
      deallocate (jbtr, itr, kttr, kbtr) 
      deallocate (ktqin, kbqin)
      deallocate (jbuh, jbdh)
      deallocate (uhs, dhs, us, nl)



      deallocate (dn_head,up_head)
      deallocate (ice_in) 
      deallocate (allow_ice,    one_layer)
      deallocate (iso_conc, vert_conc, long_conc)                    
      deallocate (transport)
      deallocate (place_qtr, specify_qtr)


!**** dimension statements

      
      deallocate (icesw)
      deallocate (bhrkt2, avhkt,  dlx, dlxr, elws)      
      deallocate (dlxrho)
      deallocate (ev, qdt, qpr)
      deallocate (q, qc, qssum)
      deallocate (sods)
      deallocate (rn, phi0)
      deallocate (avh)
      !<mueller>
!    <date>2001/02/06></date>
!    <reason>void</reason>    
      deallocate (w2z)
!</mueller>
      deallocate (depthb)
      deallocate (tvp)
      deallocate (sf1v)
      deallocate (twd) 
      deallocate (eltrt, eltrb)
      deallocate (c2i, avcout)
      deallocate (qprbr, evbr)    
      deallocate (snpd, vpld, prfd, cpld, rsod, tsrd, dltd, &
     &                          wscd, sprd, scrd)
      deallocate (snpf, vplf, prff, cplf, rsof, tsrf,  &
     &                          dltf, scrf, sprf)
      deallocate (dltmax)  

      deallocate (w2x)
      
      deallocate (sf2v, sf3v, sf4v, sf5v, sf6v,            &
     &          sf7v, sf8v, sf9v, sf10v)
      deallocate (xdummy)


      deallocate (vactive)
      deallocate (hactive)
      deallocate (balance_error)

      deallocate (qgw)  
      deallocate (qgwin) 
      deallocate (qgwout)   
      deallocate (tgw)
      deallocate (tlake)   
      deallocate (currentGwOutVolume) 
      !<date>2013/08/20</date>
      deallocate (qload) 
      

      deallocate (admx, admz, dz, dx, &
     &             dm, p,    &
     &             hpg, hdg, sb, st,    dzq)
      deallocate (su, sw, saz)
      deallocate (tss, qss)
      deallocate (bb, br, bhr, hseg) 
      deallocate (sf1l)
      deallocate (cwd)
      deallocate (quh1, quh2, qdh1, qdh2)
      deallocate (quh1_cum_plus, quh2_cum_plus, qdh1_cum_plus, qdh2_cum_plus)
      deallocate (quh1_cum_minus, quh2_cum_minus, qdh1_cum_minus, qdh2_cum_minus)
      deallocate (tssuh1 , tssuh2 , tssdh1 , tssdh2 )
      deallocate (qinf)    
      deallocate (qtrf)
      deallocate (akbr)
      deallocate (fetchu, fetchd)
      deallocate (cvp)
      deallocate (sf2l, sf3l, sf4l, sf5l, sf6l,        &
     &          sf7l, sf8l, sf9l, sf10l, &
     &            sf11l, sf12l, sf13l)
      deallocate (cssuh1 , cssuh2 , cssdh1 , cssdh2) 
      deallocate (c1s,  cssb)

!      deallocate (volgwin, volgwout, tssgwin, tssgwout)  
!      deallocate (tssev,  tsspr,  tsstr,        &
!                tssdt,  tsswd,  tssuh,    &
!     &          tssdh,  tssin,  tssout, tsss, &
!     &            tssb,   tssice,    &
!     &          esbr,   etbr,   eibr)
!      deallocate (volsbr, voltbr, volev, volpr,    &
!            voltr, voldt, volwd,  voluh,  voldh, &
!    &        volin,  volout, dlvol, volibr)
      deallocate (bhrho, gma, bta, a, c, d, v, f)
      deallocate (sz)
      deallocate (gmat,   dt,     vt)
      deallocate (btat,    ct,  at)
      deallocate (tadl,   tadv)
      deallocate (ad1l,   ad2l,   ad3l,        &
     &            ad1v,   ad2v,   ad3v)
      deallocate (dx1,    dx2,    dx3)
      deallocate (cmbrs,  cmbrt)
      deallocate (cadl,  cadv)
!      deallocate (csstr, cssdt, csswd, csspr,     &
!                  cssin, cssout, cssuh, cssdh,    &
!                  cssphc, cssw2, cssgwin, cssgwout, &
!                  cssatm, csssed)

	  deallocate (cprc)
      deallocate (cunit1)
      deallocate (cname2) 
      deallocate (qtrfn, ttrfn,   ctrfn)
      deallocate (qinfn,  tinfn, cinfn, qotfn,     &    
     &             qdtfn, tdtfn, cdtfn, prefn, tprfn, cprfn,            &
     &             euhfn,  tuhfn, cuhfn, edhfn, tdhfn, cdhfn)
      deallocate (lfac)
      deallocate (lfpr)
      deallocate (limitingCause)
      deallocate (limitingFactor)
      deallocate (conv)
      deallocate (kb, kti)  
      deallocate (ipr)
      deallocate (cn) 
      deallocate (trcn, dtcn, prcn, uhcn, dhcn, gwcn, loadcn)
      deallocate (incn)
      deallocate (nout)
      deallocate (nstr)
      deallocate (cus,   ds)
      deallocate (kbsw) 
      deallocate (kout)  

        
      deallocate (ice)
      deallocate (dist_tribs)
      deallocate (up_flow, dn_flow, uh_internal,            &
     &          dh_internal, uh_external, dh_external)
      deallocate (sel_withdrawal)
      deallocate (point_sink)

      deallocate (algaenosettlingss,    algaewithsettlingss)

      deallocate (cssk, c1, c2)
      deallocate (ssgw, ssp)
!      deallocate (ssssp, colssp, ldomssp, rdomssp,        &
!     &            algssp, lpomssp, po4ssp, nh4ssp,        &
!     &            no3ssp, dossp, ticssp, fessp,            &
!     &            cbodssp, trassp, alssp, so4ssp,            &
!     &            cassp, mgssp, nassp, kassp,                &
!     &            mnssp, clssp, fe3ssp, mn3ssp,            &
!     &            sidssp, ch4ssp, feoh3ssp,    aloh3ssp,    &
!     &            cdssp, pbssp, znssp, arsssp,              &
!     &            pyritessp, po4precipssp, co3ssp) 
      

!      deallocate (ssssgw, colssgw, ldomssgw, rdomssgw,    &
!     &            algssgw, lpomssgw, po4ssgw, nh4ssgw,    &
!     &            no3ssgw, dossgw, ticssgw, fessgw,        &
!     &            cbodssgw, trassgw, alssgw, so4ssgw,        &
!     &            cassgw, mgssgw, nassgw, kassgw,            &
!     &            mnssgw, clssgw, fe3ssgw, mn3ssgw,        &
!     &            sidssgw, ch4ssgw,                                            &
!     &            cdssgw, pbssgw, znssgw, arsssgw)

     deallocate (ssssgwout, colssgwout, ldomssgwout, rdomssgwout, &
!     &            algssgwout, lpomssgwout, &
     &            po4ssgwout, nh4ssgwout, no3ssgwout, dossgwout, &
     &            ticssgwout, fessgwout, &
!     &           cbodssgwout, &
     &            trassgwout, alssgwout, so4ssgwout, cassgwout, &
     &            mgssgwout, nassgwout, kassgwout, mnssgwout, &
     &            clssgwout, fe3ssgwout, mn3ssgwout, sidssgwout, &
     &            ch4ssgwout, cdssgwout, pbssgwout, znssgwout, arsssgwout)
      deallocate (el, h)
      deallocate (hkt1, hkt2, bkt, bhkt1, bhkt2, bhrkt1)
      deallocate (iceth)
      deallocate (sod)
      deallocate (depthm)
      deallocate (wsc)
      deallocate (eluh, eldh)
      deallocate (qin, pr, qdtr)
      deallocate (qsum)
      deallocate (tin, tpr, tdtr)
      deallocate (qwd)
      deallocate (qtr, ttr)

      deallocate (co2sat)

      deallocate (ratd)
      deallocate (curl3)
      deallocate (curl2)
      deallocate (curl1)

      deallocate (ratv)
      deallocate (curv3)
      deallocate (curv2)
      deallocate (curv1)
      
      deallocate (lpomd, sedd, domd, sodd, &
     &                            nh4d, no3d, cbodd)
      deallocate (omrm, nh4rm, no3rm)
      deallocate (armr, armf)
      deallocate (setin, setout)
      deallocate (a1, a2, a3)
      deallocate (agr, arr, amr, aer)
      deallocate (fpss, fpfe)
      deallocate (t1,    t2)
      deallocate (az)
      deallocate (u, w) 
      deallocate (rho)
      deallocate (ndlt)
      deallocate (b, bh)
      deallocate (qout) 
      deallocate (tuh,    tdh)
      deallocate (cin, cdtr, cpr)
      deallocate (ctr)
      deallocate (qstr, estr, wstr)
      deallocate (cuh, cdh)

    
      
      
!end in common block from main.f90

!incommon block from time_varying_data.f90


        deallocate (      ctrnx,      cinnx,            &
     &                  qoutnx,     cdtrnx,            &
     &                  cprnx,      tuhnx,            &
     &                  tdhnx,      qstrnx,            &
     &                  cuhnx,  cdhnx,        &
     &                  qdtrnx,         tdtrnx,                &
     &                  prnx,           tprnx,                &
     &                  eluhnx,         eldhnx,                &
     &                  qwdnx,          qtrnx,                &    
     &                  ttrnx,          qinnx,                &
     &                  tinnx)
        deallocate (      ctro,       cino,                &
     &                  qouto,      cdtro,            &
     &                  tuho,       tdho,                &
     &                  qstro,      cuho,            &
     &                  cdho,   qdtro,                &
     &                  tdtro,          eluho,                &
     &                  eldho,          qwdo,                    &
     &                  qtro,           ttro,                    &
     &                  qino,           tino)                
        deallocate (        nxqtr1,      nxttr1,   nxctr1,    &
     &                  nxqin1,      nxtin1,   nxcin1,    &
     &                  nxqdt1,      nxtdt1,   nxcdt1,    &
     &                  nxpr1,       nxtpr1,   nxcpr1,    &
     &                  nxeuh1,      nxtuh1,   nxcuh1,    &
     &                  nxedh1,      nxtdh1,   nxcdh1,    &
     &                  nxqot1)
        deallocate (        nxqtr2,      nxttr2,   nxctr2,    &
     &                  nxqin2,      nxtin2,   nxcin2,    &
     &                  nxqdt2,      nxtdt2,   nxcdt2,    &
     &                  nxpr2,       nxtpr2,   nxcpr2,    &
     &                  nxeuh2,      nxtuh2,   nxcuh2,    &
     &                  nxedh2,      nxtdh2,   nxcdh2,    &
     &                  nxqot2)

    deallocate (z)

! unit numbers for time_varying_data

        deallocate ( trq, trt, tribconc)
        deallocate ( inq, dtq, pre, uhe, dhe)
        deallocate ( intemp, dtt, prt, uht, dht)
        deallocate ( inc, dtc, prc, uhc, dhc)
        deallocate ( otq)

        deallocate ( pcgq)
        deallocate ( adl)
    end subroutine
   