!***********************************************************************
!*   s u b r o u t i n e   s e l e c t i v e   w i t h d r a w a l    **
!***********************************************************************

!$message:'  subroutine selective_withdrawal'
subroutine selective_withdrawal
        use shared_data


! not in common block


!****** dimension declaration

        double precision, dimension (kmp):: vnorm



!****** data declaration

        double precision :: local_g = 9.81d0, elstr, ratio, coef, rhoft, hswt, dlrhot, rhofb, hswb
        double precision :: dlrhob, vsum, dlrhomax
        integer :: local_k, local_js, ktop, kbot, kstr

!****** variable initialization

        do local_k=1,kmp
          qout(local_k,jb) = 0.0d0
          vnorm(local_k)   = 0.0d0
        end do

!****** outflows

        do local_js=1,nstr(jb)

!******** initial withdrawal limits

          ktop = kt
          kbot = min(kbsw(local_js,jb),kb(id))
          if (kbot.lt.kt+1) kbot = kt+1

!******** structure layer

          local_k = kt
          do while (el(local_k).ge.estr(local_js,jb))
            local_k = local_k+1
          end do
          kstr  = max(local_k-1,kt)
          kstr  = min(kstr,kb(id))
          elstr = estr(local_js,jb)
          elkt  = el(kt)-z(id)
          if (estr(local_js,jb).le.el(kb(id)+1)) then
            kstr  = kt
            elstr = el(kb(id))
          end if
          if (estr(local_js,jb).gt.elkt) elstr = elkt
          if (kbsw(local_js,jb).lt.kstr) then
            kstr  = kt
            elstr = elkt-(h(kt)-z(id))/2.0
          end if

!******** boundary interference

          ratio = (elstr-el(kbot))/(elkt-el(kbot))
          coef  = 1.0
          if (ratio.lt.0.10.or.ratio.gt.0.90) coef = 2.0

!******** withdrawal zone above structure

          do local_k=kstr-1,kt,-1

!********** density frequency

            ht    = el(local_k)-elstr
            rhoft = max(sqrt((abs(rho(local_k,id)-rho(kstr,id)))/(ht            &
     &              *rho(kstr,id)+nonzero)*local_g),nonzero)

!********** thickness

            if (point_sink(local_js,jb)) then
              hswt = (coef*qstr(local_js,jb)/rhoft)**0.333333
            else
              hswt = sqrt(2.0*coef*qstr(local_js,jb)/(wstr(local_js,jb)*rhoft))
            end if
            if (ht.ge.hswt) then
              ktop = local_k
              go to 10000
            end if
          end do
10000     continue

!******** reference density

          if ((elstr+hswt).lt.elkt) then
            dlrhot = abs(rho(kstr,id)-rho(ktop,id))
          else if (elkt.eq.elstr) then
            dlrhot = nonzero
          else
            dlrhot = abs(rho(kstr,id)-rho(kt,id))*hswt/(elkt-elstr)
          end if
          dlrhot = max(dlrhot,nonzero)

!******** withdrawal zone below structure

          do local_k=kstr+1,kbot

!********** density frequency

            hb    = elstr-el(local_k)
            rhofb = max(sqrt((abs(rho(local_k,id)-rho(kstr,id)))/(hb            &
     &              *rho(kstr,id)+nonzero)*local_g),nonzero)

!********** thickness

            if (point_sink(local_js,jb)) then
              hswb = (coef*qstr(local_js,jb)/rhofb)**0.333333
            else
              hswb = sqrt(2.0*coef*qstr(local_js,jb)/(wstr(local_js,jb)*rhofb))
            end if
            if (hb.ge.hswb) then
              kbot = local_k
              go to 10010
            end if
          end do
10010     continue

!******** reference density

          if ((elstr-hswb).gt.el(kbot+1)) then
            dlrhob = abs(rho(kstr,id)-rho(kbot,id))
          else
            dlrhob = abs(rho(kstr,id)-rho(kbot,id))*hswb                &
     &               /(elstr-el(kbot+1))
          end if
          dlrhob = max(dlrhob,nonzero)

!******** velocity profile

          vsum     = 0.0d0
          dlrhomax = max(dlrhot,dlrhob,1.0e-10)
          do local_k=ktop,kbot
            vnorm(local_k) = abs(1.0-((rho(local_k,id)-rho(kstr,id))/dlrhomax)**2)
            vsum     = vsum+vnorm(local_k)
          end do

!******** outflows

          do local_k=ktop,kbot
            qout(local_k,jb) = qout(local_k,jb)+(vnorm(local_k)/vsum)*qstr(local_js,jb)
          end do
        end do

!****** boundary velocities and total outflow

        qsum(jb) = 0.0d0
        do local_k=kt,kb(id)
          if (qout(local_k,jb).eq.0.0) u(local_k,id) = 0.0d0
          qsum(jb) = qsum(jb)+qout(local_k,jb)
        end do
      end

