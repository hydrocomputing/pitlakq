   subroutine initvariables()
        use shared_data

        implicit none
        double precision :: density
               

        include 'formats_main.inc'

        iso_temp     = int(t2i).ge.0
        vert_temp    = int(t2i).eq.-1
        long_temp    = int(t2i).lt.-1
        open_vpr     = vert_temp.and..not.restart_in
        open_lpr     = long_temp.and..not.restart_in
        do jc=1,ncp
          iso_conc(jc)  = int(c2i(jc)).ge.0
          vert_conc(jc) = int(c2i(jc)).eq.-1
          long_conc(jc) = int(c2i(jc)).lt.-1
          if (vert_conc(jc).and..not.restart_in) open_vpr = .true.
          if (long_conc(jc).and..not.restart_in) open_lpr = .true.
        end do
        !open(debug, file='debug.opt',status='unknown')
!***********************************************************************
!*                         task 1.4: variables                        **
!***********************************************************************

!***********************************************************************
!*                      task 1.4.1: zero variables                    **
!***********************************************************************

!$message:'    variable initialization'
!<mueller>
!    <date>2001/02/06></date>
!    <reason>setting intitial gw to zero ?</resaon>
    nxqgw1 = 0 !
      do i=1,imc
        do k=1,kmc
        ssssgw(k,i)   = 0.0d0
        colssgw(k,i)  = 0.0d0
        ldomssgw(k,i) = 0.0d0
        rdomssgw(k,i) = 0.0d0
        algssgw(k,i)  = 0.0d0
        lpomssgw(k,i) = 0.0d0
        po4ssgw(k,i)  = 0.0d0
        nh4ssgw(k,i)  = 0.0d0
        no3ssgw(k,i)  = 0.0d0
        dossgw(k,i)   = 0.0d0
        ticssgw(k,i)  = 0.0d0
        fessgw(k,i)   = 0.0d0
        cbodssgw(k,i) = 0.0d0
        trassgw(k,i)  = 0.0d0
        alssgw(k,i)   = 0.0d0
        so4ssgw(k,i)  = 0.0d0
        cassgw(k,i)   = 0.0d0
        mgssgw(k,i)   = 0.0d0
        nassgw(k,i)   = 0.0d0
        kassgw(k,i)   = 0.0d0
        mnssgw(k,i)   = 0.0d0
        clssgw(k,i)   = 0.0d0
        fe3ssgw(k,i)  = 0.0d0
        mn3ssgw(k,i)   = 0.0d0
        sidssgw(k,i)   = 0.0d0
        ch4ssgw(k,i)   = 0.0d0
        cdssgw(k,i)   = 0.0d0
        pbssgw(k,i)  = 0.0d0
        znssgw(k,i)   = 0.0d0
        arsssgw(k,i)   = 0.0d0
        ssssp(k,i)     = 0.0d0
        colssp(k,i)  = 0.0d0
        ldomssp(k,i) = 0.0d0
        rdomssp(k,i) = 0.0d0
        algssp(k,i)  = 0.0d0
        lpomssp(k,i) = 0.0d0
        po4ssp(k,i)  = 0.0d0
        nh4ssp(k,i)  = 0.0d0
        no3ssp(k,i)  = 0.0d0
        dossp(k,i)     = 0.0d0
        ticssp(k,i)  = 0.0d0
        co3ssp(k,i)  = 0.0d0
        fessp(k,i)     = 0.0d0
        cbodssp(k,i) = 0.0d0
        trassp(k,i)  = 0.0d0
        alssp(k,i)     = 0.0d0
        so4ssp(k,i)  = 0.0d0
        cassp(k,i)     = 0.0d0
        mgssp(k,i)     = 0.0d0
        nassp(k,i)     = 0.0d0
        kassp(k,i)     = 0.0d0
        mnssp(k,i)     = 0.0d0
        clssp(k,i)     = 0.0d0
        fe3ssp(k,i)  = 0.0d0
        mn3ssp(k,i)  = 0.0d0
        sidssp(k,i)  = 0.0d0
        ch4ssp(k,i)  = 0.0d0
        feoh3ssp(k,i)= 0.0d0
        aloh3ssp(k,i)= 0.0d0
        cdssp(k,i)     = 0.0d0
        pbssp(k,i)  = 0.0d0
        znssp(k,i)  = 0.0d0
        arsssp(k,i)  = 0.0d0
        pyritessp(k,i)= 0.0d0
        po4precipssp(k,i)= 0.0d0

        algaenosettlingss(k,i)= 0.0d0
        algaewithsettlingss(k,i)= 0.0d0
    end do
   end do


! setting forcing function variables to zero
a = 0.0d0
c = 0.0d0
d = 0.0d0
f = 0.0d0
!</mueller>
      do i=1,imp
        do k=1,kmp
          conv(k,i)  = blank
          conv1(k,i) = blank1
        end do
      end do
      do i=1,imc
        do k=1,kmc
          lfpr(k,i) = blank
        end do
      end do
      do i=1,imp
        icesw(i) = 1.0
        do jb=1,nbp
          fetchu(i,jb) = 0.0d0
          fetchd(i,jb) = 0.0d0
        end do
        if (.not.restart_in) then
          do k=1,kmp
            u(k,i) = 0.0d0
            w(k,i) = 0.0d0
          end do
        end if
        do k=1,kmp
          tss(k,i) = 0.0d0
          qss(k,i) = 0.0d0
          if (constituents) then
            do jc=1,ncp
              if (.not.restart_in) then
                cssb(k,i,jc) = 0.0d0
                cssk(k,i,jc) = 0.0d0
              end if
            end do
          end if
        end do
      end do
      do jb=1,nbp
        if (restart) then
        else
          volev(jb)  = 0.0d0
          volpr(jb)  = 0.0d0
          voltr(jb)  = 0.0d0
          !write(*,*) 'voltr in intVariables', voltr, shape(voltr)
          voldt(jb)  = 0.0d0
          volwd(jb)  = 0.0d0
          voluh(jb)  = 0.0d0
          voldh(jb)  = 0.0d0
          volin(jb)  = 0.0d0
          volout(jb) = 0.0d0
          volsbr(jb) = 0.0d0
          volibr(jb) = 0.0d0
!<mueller>
!    <date>2001/02/06, 2005/08/16</date>
!    <reason>initilising volgwin and volgwout</reason>
        volgwin(jb)  = 0.0d0
        volgwout(jb) = 0.0d0
!</mueller>
          tsss(jb)   = 0.0d0
          tssb(jb)   = 0.0d0
          tssev(jb)  = 0.0d0
          tsspr(jb)  = 0.0d0
          tsstr(jb)  = 0.0d0
          tssdt(jb)  = 0.0d0
          tsswd(jb)  = 0.0d0
          tssuh(jb)  = 0.0d0
          tssdh(jb)  = 0.0d0
          tssin(jb)  = 0.0d0
          tssout(jb) = 0.0d0
          tssice(jb) = 0.0d0
          esbr(jb)   = 0.0d0
          etbr(jb)   = 0.0d0
          eibr(jb)   = 0.0d0
!<mueller>
!    <date>2005/08/16</date>
!    <reason>initilising tssgwin and tssgwout</reason>
        tssgwin(jb)  = 0.0d0
        tssgwout(jb) = 0.0d0
        !<date>2013/08/20</date>
        tssload(jb) = 0.0d0
!</mueller>
        end if
        do k=1,kmp
          akbr(k,jb) = 0.0d0
        end do
        do jc=1,ncp
          cmbrt(jc,jb) = 0.0d0
        end do
      end do
    dlmr     = 0.0d0
      dlvr     = 0.0d0
      kbpr     = 0
      nac      = 0
      nacin    = 0
      nactr    = 0
      nacdt    = 0
      nacpr    = 0
      nsprf    = 0
      ntac     = 0
      kbmax    = 0
      hmax     = 0
      dlxmax   = 0
      hmin     = 1.0d10
      dlxmin   = 1.0d10
      title(7) = ' '
      if (.not.restart_in) then
        kloc = 1
        iloc = 1
      end if

!***********************************************************************
!*                  task 1.4.2: miscellaneous variables               **
!***********************************************************************
!**** logical controls

      open_files      = .true.
      volume_warning  = .true.
      update_kinetics = .true.
      end_run         = .false.
      warning_open    = .false.
      tributaries     = ntr.gt.0
      withdrawals     = nwd.gt.0
      place_qin       = pqc.eq.1
      evaporation     = evc.eq.1
      volume_balance  = vbc.eq.1
      energy_balance  = ebc.eq.1
      precipitation   = precip.eq.1
      screen_output   = scrc.eq.'t'
      snapshot        = snpc.eq.'t'
      contour         = cplc.eq.'t'
      vector          = vplc.eq.'t'
      profile         = prfc.eq.'t'
      spreadsheet     = sprc.eq.'t'
      ice_calc        = icec.eq.1
      time_series     = tsrc.eq.'t'
      restart_out     = rsoc.eq.'t'
      restart_out     = .true.
      interp_tribs    = tric.eq.1
      interp_head     = hdic.eq.1
      interp_withdrwl = wdic.eq.1
      interp_met      = metic.eq.1
      interp_inflow   = infic.eq.1
      interp_dtribs   = dtric.eq.1
      interp_outflow  = outic.eq.1
      limiting_dlt    = hprc(4).eq.'t'
      no_inflow       = qinc.eq.0
      no_heat         = heatc.eq.0
      no_wind         = windc.eq.0
      no_outflow      = qoutc.eq.0
      laserjet_ii     = trim(ljpc).eq.'ii'
      laserjet_iii    = trim(ljpc).eq.'iii'
      laserjet_iv     = trim(ljpc).eq.'iv'
      upwind          = sltr.eq.0
      ultimate        = sltr.eq.2
      term_by_term    = slheat.eq.1
      detailed_ice    = (slice).eq.1
      detailed_ice    = ice_calc.and.detailed_ice
      mass_balance    = constituents.and.mbc.eq.1
      susp_solids     = constituents.and.acc(2).eq.1
      oxygen_demand   = constituents.and.acc(12).eq.1
      limiting_factor = constituents.and.acc(7).eq.1                &
     &                  .and.limc.eq.1
      fresh_water     = constituents.and.trim(wtype).eq.'fresh'
      salt_water      = constituents.and.trim(wtype).eq.'salt'
      shift_demand    = constituents.and.acc(12).eq.1                &
     &                  .and.sdc.eq.1
      interpolate     = interp_inflow.or.interp_tribs.or.interp_dtribs  &
     &                  .or.interp_outflow.or.interp_withdrwl            &
     &                  .or.interp_head.or.interp_met
      leap_year       = mod(year,4).eq.0
      if (restart_in) then
        winter = .false.
        if (jday.gt.300.0.or.jday.lt.40.0) winter = .true.
      else
        winter = .false.
        if (tmstrt.gt.300.0.or.tmstrt.lt.40.0) winter = .true.
      end if
      do jt=1,ntp
        place_qtr(jt)   = trc(jt).eq.1
        specify_qtr(jt) = trc(jt).eq.2
      end do
      do jc=1,ncp
        transport(jc) = .true.
        if (jc.eq.13.or.(jc.gt.15.and.jc.lt.20)) transport(jc) = .false.
      end do
      do jc=5,13
        if (acc(jc).eq.1) update_rates = .true.
      end do
      do jb=1,nbp
        up_flow(jb)        = uhs(jb).eq.0
        dn_flow(jb)        = dhs(jb).eq.0
        up_head(jb)        = uhs(jb).ne.0
        dn_head(jb)        = dhs(jb).ne.0
        uh_internal(jb)    = uhs(jb).gt.0
        dh_internal(jb)    = dhs(jb).gt.0
        uh_external(jb)    = uhs(jb).eq.-1
        dh_external(jb)    = dhs(jb).eq.-1
        dist_tribs(jb)     = dtrc(jb).eq.'t'
        sel_withdrawal(jb) = swc(jb).eq.'t'
        do js=1,nstr(jb)
          point_sink(js,jb) = trim(sink(js,jb)).eq.'point'
        end do
        if (uh_external(jb).or.dh_external(jb)) head_boundary = .true.
      end do
!**** convert rates from per-day to per-second

      ae     = ae/86400.0
      am     = am/86400.0
      ar     = ar/86400.0
      ag     = ag/86400.0
      as     = as/86400.0
      fes    = fes/86400.0
      sss    = sss/86400.0
      poms   = poms/86400.0
      sdk    = sdk/86400.0
      coldk  = coldk/86400.0
      lrddk  = lrddk/86400.0
      nh4dk  = nh4dk/86400.0
      no3dk  = no3dk/86400.0
      ldomdk = ldomdk/86400.0
      rdomdk = rdomdk/86400.0
      lpomdk = lpomdk/86400.0
      kbod   = kbod/86400.0
      if (constituents) then
        do i=1,imp
          sod(i)  = sod(i)/86400.0*fsod
          sods(i) = sod(i)
        end do
      end if

!**** saltwater units

      if (salt_water) then
        cname1(4) = 'salinity         '
        cname2(4) = '    salinity, kg/m^3    '
        cunit1(4) = 'kg/m^3'
        cunit2(4) = 'kg/m^3'
        cunit3(4) = 'kg'
      end if

!**** time and printout control variables
      if (.not.restart_in) then
        jday   = tmstrt
        jdayg  = jday
        eltm   = tmstrt*86400.0
        dlt    = dltmax(1)
        dlts   = dlt
        mindlt = dlt
        nit    = 0
        nv     = 0
        dltdp  = 1
        wscdp  = 1
        snpdp  = 1
        tsrdp  = 1
        vpldp  = 1
        prfdp  = 1
        sprdp  = 1
        cpldp  = 1
        rsodp  = 1
        scrdp  = 1
        nxtmsn = snpd(1)
        nxtmts = tsrd(1)
        nxtmpr = prfd(1)
        nxtmsp = sprd(1)
        nxtmcp = cpld(1)
        nxtmvp = vpld(1)
        nxtmrs = rsod(1)
        nxtmsc = scrd(1)
      end if
      do j=nwsc+1,ndp
        wscd(j) = tmend+1.0
      end do
      do j=ndt+1,ndp
        dltd(j) = tmend+1.0
      end do
       do j=nsnp+1,ndp
        snpd(j) = tmend+1.0
      end do
      do j=ntsr+1,ndp
        tsrd(j) = tmend+1.0
      end do
      do j=nprf+1,ndp
        prfd(j) = tmend+1.0
      end do
       do j=nspr+1,ndp
        sprd(j) = tmend+1.0
      end do
      do j=nvpl+1,ndp
        vpld(j) = tmend+1.0
      end do
      do j=ncpl+1,ndp
        cpld(j) = tmend+1.0
      end do
      do j=nrso+1,ndp
        rsod(j) = tmend+1.0
      end do
      do j=nscr+1,ndp
        scrd(j) = tmend+1.0
      end do
      jdaynx = jdayg+1
      nxtvd = 0.0d0 !jday
      curmax = dltmax(dltdp)/dltf(dltdp)

!**** active constituents


      if (constituents) then
        do jc=1,ncp
          if (acc(jc).eq.1) then
            nac     = nac+1
            cn(nac) = jc
          end if
          if (inacc(jc).eq.1) then
            nacin       = nacin+1
            incn(nacin) = jc
          end if
          if (tracc(jc).eq.1) then
            nactr       = nactr+1
            trcn(nactr) = jc
          end if
          if (dtacc(jc).eq.1) then
            nacdt       = nacdt+1
            dtcn(nacdt) = jc
          end if
          if (pracc(jc).eq.1) then
            nacpr       = nacpr+1
            prcn(nacpr) = jc
          end if
          !34: feoh3ss
          !35: aloh3ss
          !40: pyritess
          !41: po4precipss
          ! minerals are not transported to or from the groudwater
          if (acc(jc) .eq. 1 &
                      .and. jc .ne. 34 .and. jc .ne. 35 &
                      .and. jc .ne. 40 .and. jc .ne. 41 &
                      .and. jc .le. ncp - ncp_additional_minerals) then
            nacgw     = nacgw+1
            gwcn(nacgw) = jc
          end if
          if (acc(jc) .eq. 1) then
            nacload = nacload + 1
            loadcn(nacload) = jc
          end if
        end do
      end if
      if (constituents) then
        do jc=1,ncp
          if (acc(jc).eq.1) then
            nac0      = nac0+1
!            cn0(nac0) = jc
          end if
        end do
      end if
      deg = char(248)//'c'
      esc = char(027)
      call date_time (cdate,ctime)
      title(7) = 'model run at '//ctime//' on '//cdate                           !fortran
      if (restart_in) title(7) = 'model restarted at '//ctime//            &        !fortran
     &                           ' on '//cdate                                   !fortran
 
!***********************************************************************
!*                        task 1.4.3: geometry                        **
!***********************************************************************
!**** layer elevations and mimimum/maximum layer heights


      el(kmp) = elbot
      do k=kmp-1,1,-1
        el(k) = el(k+1)+h(k)
        hmin  = min(h(k),hmin)
        hmax  = max(h(k),hmax)
      end do

!**** water surface and bottom layers
      do jb=1,nbp
        do i=us(jb)-1,ds(jb)+1
          if (.not.restart_in) then
            kt     = 2
            kti(i) = 2
            do while (el(kti(i)).gt.elws(i))
              kti(i) = kti(i)+1
            end do
            z(i)    = el(kti(i))-elws(i)
            ktmax   = max(2,kti(i))
            kt      = max(ktmax,kt)
            kti(i)  = max(2,kti(i)-1)
            skti(i) = kti(i)
            sz(i)   = z(i)
          end if
          k = 2
          do while (b(k,i).gt.0.0)
            kb(i) = k
            k     = k+1
          end do
          kbmax = max(kbmax,kb(i))
        end do
        kb(us(jb)-1) = kb(us(jb))
        kb(ds(jb)+1) = kb(ds(jb))
      end do
      



      do jb=1,nbp
        iu = us(jb)
        id = ds(jb)


!<mueller>
!    <date>2001/02/06></date>
!    <reason>initializing gw inflow with zero</reason>
    if (gw_coupling) then
        do i = iu, id
            do k =kt, kb(i)
                qgw(k,i) = 0.0d0  
            end do 
        end do 
    end if 
!</mueller>

    if (loading) then
        do i = iu, id
            do k =kt, kb(i)
                qload(k,i) = 0.0d0  
            end do 
        end do 
    end if 
    
!****** upstream active segment and single layer

        iut = iu
        do i=iu,id
          if (kb(i)-kt.lt.nl(jb)-1) iut = i+1
          one_layer(i) = kt.eq.kb(i)
        end do
        if (iut.gt.ds(jb)-1) then
          write(*,*) 'i', i
          write(*,*) 'b', b
          write(*,*)  'elws', elws
          write(*,*) 'dlx', dlx
          write(*,*) 'h', h
          open  (err,file='err.opt',status='unknown')
          write (err,7000) jb,jday,kt,iut,ds(jb)-1,kb(i),kt, elws(i)
          write (err,*) 'downstream cell dry'
          write (*,*) 'downstream cell dry'
          error = 0
          return
        end if

        cus(jb) = iut

!****** boundary bottom layers


        if (uh_external(jb)) kb(iut-1) = kb(iut)
        if (dh_external(jb)) kb(id+1)  = kb(id)
        if (uh_internal(jb)) kb(iut-1) = min(kb(uhs(jb)),kb(iut))
        if (dh_internal(jb)) kb(id+1)  = min(kb(dhs(jb)),kb(id))

!****** boundary segment lengths

        dlx(iu-1) = dlx(iu)
        dlx(id+1) = dlx(id)

!****** minimum bottom layers and average segment lengths 


        do i=iu-1,id
          kbmin(i) = min(kb(i),kb(i+1))
          dlxr(i)  = (dlx(i)+dlx(i+1))*0.5
        end do
        dlxr(id+1) = dlx(id)

!****** minimum/maximum segment lengths

        do i=iu,id
          dlxmin = min(dlxmin,dlx(i))
          dlxmax = max(dlxmax,dlx(i))
        end do

!****** boundary widths

        do k=1,kb(iu)
          b(k,iu-1) = b(k,iu)
          if (uh_internal(jb)) b(k,iu-1) = b(k,uhs(jb))
        end do
        do k=1,kb(id)
          b(k,id+1) = b(k,id)
          if (dh_internal(jb)) b(k,id+1) = b(k,dhs(jb))
        end do
        do i=iu-1,id+1
          b(1,i) = b(2,i)
          do k=kb(i)+1,kmp
            b(k,i) = b(kb(i),i)
          end do
        end do

!****** areas and bottom widths

        do i=iu-1,id+1
          do k=1,kmp-1
            bh(k,i) = b(k,i)*h(k)
            bb(k,i) = (b(k,i)+b(k+1,i))*0.5
          end do
          bh(kb(i)+1,i) = bh(kb(i),i)
        end do

!****** derived geometry

        do i=iu-1,id+1
          hkt2(i)  = h(kt)-z(i)
          avhkt(i) = (hkt2(i)+h(kt+1))*0.5
          bhkt2(i) = b(kti(i),i)*(el(kt)-el(kti(i)+1)-z(i))
          do k=kti(i)+1,kt
            bhkt2(i) = bhkt2(i)+bh(k,i)
          end do
          bkt(i) = bhkt2(i)/hkt2(i)
        end do
        idt = id+1
        if (jb.eq.nbp) idt = id
        do i=iu-1,idt
          bhrkt2(i) = (bhkt2(i)+bhkt2(i+1))*0.5
          do k=1,kmp-1
            br(k,i)  = (b(k,i)+b(k,i+1))*0.5
            bhr(k,i) = (bh(k,i)+bh(k,i+1))*0.5
          end do
        end do
        do k=1,kmp-1
          avh(k) = (h(k)+h(k+1))*0.5
        end do

!****** branch numbers corresponding to tributaries, withdrawals, and head

        if (tributaries) then
          do jt=1,ntr
            if (itr(jt).ge.us(jb).and.itr(jt).le.ds(jb)) then
              jbtr(jt) = jb
            end if
          end do
        end if
        if (withdrawals) then
          do jw=1,nwd
            if (iwd(jw).ge.us(jb).and.iwd(jw).le.ds(jb)) then
              jbwd(jw) = jb
            end if
          end do
        end if
        if (uh_internal(jb)) then
          jbuh(jb)     = 0
          branch_found = .false.
          do while (.not.branch_found)
            jbuh(jb) = jbuh(jb)+1
            do i=us(jbuh(jb)),ds(jbuh(jb))
              if (i.eq.uhs(jb)) branch_found = .true.
            end do
          end do
        end if
        if (dh_internal(jb)) then
          jbdh(jb)     = 0
          branch_found = .false.
          do while (.not.branch_found)
            jbdh(jb) = jbdh(jb)+1
            do i=us(jbdh(jb)),ds(jbdh(jb))
              if (i.eq.dhs(jb)) branch_found = .true.
            end do
          end do
        end if

!****** branch layer area

        do k=kmp-1,2,-1
          do i=iu,id
            if (k.le.kb(i)) akbr(k,jb) = akbr(k,jb)+b(k,i)*dlx(i)
          end do
        end do

!****** layer bottom and middle depths

        depthb(kt)   = hkt2(i)
        depthm(kt)   = hkt2(i)*0.5
        depthb(kt+1) = depthb(kt)+h(kt+1)
        depthm(kt+1) = depthm(kt)+h(kt+1)*0.5
        do k=kt+2,kmp
          depthb(k) = depthb(k-1)+h(k)
          depthm(k) = depthm(k-1)+(h(k-1)+h(k))*0.5
        end do

!****** total active cells

        do i=cus(jb),id
          do k=kt,kb(i)
            ntac = ntac+1
          end do
        end do
        ntacmx = ntac
        ntacmn = ntac

!****** wind fetch lengths

        do i=iu,id
          fetchd(i,jb) = fetchd(i-1,jb)+dlx(i)
        end do
        do i=id,iu,-1
          fetchu(i,jb) = fetchu(i+1,jb)+dlx(i)
        end do
      end do

!**** segment heights

      do i=1,imp
        do k=kb(i),2,-1
          hseg(k,i) = hseg(k+1,i)+h(k)
        end do
      end do


!**** ending segment and bottom layer for snapshots

      iepr = min(imp-2,nisnp)
      do i=1,iepr
        ipr(i) = ipri(i)
      end do
      do i=1,iepr
        kbpr = max(kb(ipr(i)),kbpr)
      end do

!**** transport interpolation multipliers

      do i=2,imp-1
        do k=2,kmp-1

!******** positive flows

          dlxt = dlx(i-1) 
          if (k.gt.kb(i-1)) dlxt = dlx(i)
          dlxm         = min(dlx(i+1),dlx(i))
          sf1l(k,i)    = (dlx(i+1)+dlx(i))*0.5
          sf2l(k,i,1)  = dlx(i)/(dlx(i)+dlx(i+1))
          sf3l(k,i,1)  = dlx(i)**2
          sf4l(k,i,1)  = dlx(i+1)/(dlx(i)+dlx(i+1))
          sf5l(k,i,1)  = 0.25*(dlxt+2.0*dlx(i)+dlx(i+1))*(dlxt+dlx(i))
          sf6l(k,i,1)  = -0.25*(dlx(i)+dlx(i+1))*(dlxt+dlx(i))
          sf7l(k,i,1)  = 0.25*(dlx(i)+dlx(i+1))*(dlxt+2.0*dlx(i)        &
     &                   +dlx(i+1))
          sf8l(k,i,1)  = 0.5*(dlx(i)-dlx(i+1))*dlxm
          sf9l(k,i,1)  = 0.5*(dlxt+2.0*dlx(i)-dlx(i+1))*dlxm
          sf10l(k,i,1) = 0.5*(dlxt+3.0*dlx(i))*dlxm
          sf11l(k,i,1) = sf8l(k,i,1)/sf5l(k,i,1)/sf1l(k,i)
          sf12l(k,i,1) = sf9l(k,i,1)/sf6l(k,i,1)/sf1l(k,i)
          sf13l(k,i,1) = sf10l(k,i,1)/sf7l(k,i,1)/sf1l(k,i)
          htop         = h(k-1)
          hmid         = h(k)
          hbot         = h(k+1)
          hmin         = min(hbot,hmid)
          sf1v(k)      = (hbot+hmid)*0.5
          sf2v(k,1)    = hmid**2
          sf3v(k,1)    = hmid/(hmid+hbot)
          sf4v(k,1)    = hbot/(hmid+hbot)
          sf5v(k,1)    = 0.25*(htop+2.0*hmid+hbot)*(htop+hmid)
          sf6v(k,1)    = -0.25*(hmid+hbot)*(htop+hmid)
          sf7v(k,1)    = 0.25*(hmid+hbot)*(htop+2.0*hmid+hbot)
          sf8v(k,1)    = 0.5*(hmid-hbot)*hmin
          sf9v(k,1)    = 0.5*(htop+2.0*hmid-hbot)*hmin
          sf10v(k,1)   = 0.5*(htop+3.0*hmid)*hmin

!******** negative flows

          if (i.lt.imp-1) then
            dlxt = dlx(i+2)
            if (k.gt.kb(i+2)) dlxt = dlx(i+1)
            dlxm         = min(dlx(i),dlx(i+1))
            sf1l(k,i)    = (dlx(i+1)+dlx(i))*0.5
            sf2l(k,i,2)  = dlx(i+1)/(dlx(i)+dlx(i+1))
            sf3l(k,i,2)  = dlx(i+1)**2
            sf4l(k,i,2)  = dlx(i)/(dlx(i)+dlx(i+1))
            sf5l(k,i,2)  = 0.25*(dlx(i)+2.0*dlx(i+1)+dlxt)*(dlx(i)        &
     &                     +dlx(i+1))
            sf6l(k,i,2)  = -0.25*(dlx(i+1)+dlxt)*(dlx(i)+dlx(i+1))
            sf7l(k,i,2)  = 0.25*(dlx(i)+2.0*dlx(i+1)+dlxt)*(dlx(i+1)    &
     &                     +dlxt)
            sf8l(k,i,2)  = -0.5*(3.0*dlx(i+1)+dlxt)*dlxm
            sf9l(k,i,2)  = 0.5*(dlx(i)-2.0*dlx(i+1)-dlxt)*dlxm
            sf10l(k,i,2) = 0.5*(dlx(i)-dlx(i+1))*dlxm
            sf11l(k,i,2) = sf8l(k,i,2)/sf5l(k,i,2)/sf1l(k,i)
            sf12l(k,i,2) = sf9l(k,i,2)/sf6l(k,i,2)/sf1l(k,i)
            sf13l(k,i,2) = sf10l(k,i,2)/sf7l(k,i,2)/sf1l(k,i)
          end if
          htop = h(k)
          hmid = h(k+1)
          if (k.lt.kb(i)) then
            hbot = h(k+2)
            if (k.eq.kb(i)-1) hbot = h(k+1)
            hmin       = min(htop,hmid)
            sf1v(k)    = (hmid+htop)*0.5
            sf2v(k,2)  = hmid**2
            sf3v(k,2)  = hmid/(htop+hmid)
            sf4v(k,2)  = htop/(htop+hmid)
            sf5v(k,2)  = 0.25*(htop+2.0*hmid+hbot)*(htop+hmid)
            sf6v(k,2)  = -0.25*(hmid+hbot)*(htop+hmid)
            sf7v(k,2)  = 0.25*(htop+2.0*hmid+hbot)*(hmid+hbot)
            sf8v(k,2)  = -0.5*(3.0*hmid+hbot)*hmin
            sf9v(k,2)  = 0.5*(htop-2.0*hmid-hbot)*hmin
            sf10v(k,2) = 0.5*(htop-hmid)*hmin
          end if
        end do
      end do
!***********************************************************************
!*                    task 1.4.4: initial conditions                  **
!***********************************************************************
!$message:'    initial conditions'

!**** vertical profiles


!      if (open_vpr) then

!****** temperature

!        open (vpr,file=vprfn,status='old')
!        read (vpr,*)
!        if (vert_temp) read (vpr,1030) (tvp(k),k=kt,kbmax)
!        if (constituents) then
!          do jc=1,ncp
!            if (vert_conc(jc)) read (vpr,1030) (cvp(k,jc),k=kt,kbmax)
!          end do
!        end if
!      end if

!**** longitudinal/vertical initial profiles

!      if ((open_lpr).and.(.not.restart_in)) then
!            if (long_temp) then
!                status = nf_open(lprfn, nf_nowrite, ncid)
!            if (status .ne. nf_noerr) call handle_err (status)
!
!                call netcdf_get_double_values(ncid, 'temperature', tempid, t1, kmp, imp)
!        
!            end if
            if (iso_temp)  t1 = t2i
            t2=t1
!            
!
!
!
!            call netcdf_get_double_values(ncid, 'const', c2id, c2, kmp, imp)
!            status = nf_inq_varid(ncid, 'iceth', icethid)    
!            if (status .ne. nf_noerr) call handle_err (status)    
!            status = nf_get_var_double(ncid, icethid, iceth)
!            if (status .ne. nf_noerr) call handle_err (status)
!            status = nf_close(ncid)
!            if (status .ne. nf_noerr) call handle_err (status)
            c1  = c2
            c1s = c1
       ! open (lpr,file=lprfn,status='old')
       ! read (lpr,*)
      !end if
      do jb=1,nbp
        iu = cus(jb)
        id = ds(jb)
        if (.not.restart_in) then

!******** temperature

       !   do i=iu,id
       !     if (long_temp) read (lpr,1190) (t1(k,i),k=kt,kb(i))
       !     do k=kt,kb(i)
       !       if (iso_temp)  t1(k,i) = t2i
       !       if (vert_temp) t1(k,i) = tvp(k)
       !       t2(k,i) = t1(k,i)
       !     end do
       !   end do

     


!******** constituents

 !         do jc=1,nac0
 !           do i=iu,id
 !             if (long_conc(cn0(jc))) then
 !               read (lpr,1190) (c2(k,i,cn0(jc)),k=kt,kb(i))
 !             end if
 !             do k=kt,kb(i)
 !               if (iso_conc(cn0(jc)))  c2(k,i,cn0(jc)) = c2i(cn0(jc))
 !               if (vert_conc(cn0(jc))) c2(k,i,cn0(jc)) = cvp(k,cn0(jc))
 !               c1(k,i,cn0(jc))  = c2(k,i,cn0(jc))
 !               c1s(k,i,cn0(jc)) = c1(k,i,cn0(jc))
 !             end do
 !           end do
 !         end do

!******** intial volume

          if (volume_balance) then
            do i=iu,id
              volibr(jb) =volibr(jb)+dlx(i)*bhkt2(i)
              do k=kt+1,kb(i)
                volibr(jb) = volibr(jb)+dlx(i)*bh(k,i)
              end do
            end do
          end if


!******** energy

          if (energy_balance) then
            do i=iu,id
              eibr(jb) = eibr(jb)+t2(kt,i)*dlx(i)*bhkt2(i)
              do k=kt+1,kb(i)
                eibr(jb) = eibr(jb)+t2(k,i)*dlx(i)*bh(k,i)
              end do
            end do
          end if

!******** constituent mass

          do jc=1,nac
            jac = cn(jc)
            do i=cus(jb),ds(jb)
              cmbrt(jac,jb) = cmbrt(jac,jb)+c2(kt,i,jac)*dlx(i)*bhkt2(i)
              do k=kt+1,kb(i)
                cmbrt(jac,jb) = cmbrt(jac,jb)+c2(k,i,jac)*dlx(i)*bh(k,i)
              end do
            end do
          end do

!******** ice cover

          if (ice_calc) then
            do i=iu,id
              if (icethi >= 0) then
                 iceth(i) = icethi
              endif
              ice(i)   = iceth(i).gt.0.0
            end do
          end if

!******** vertical eddy viscosity

          iut = iu
          idt = id-1
          if (up_head(jb)) iut = iu-1
          if (dn_head(jb)) idt = id
          do i=iut,idt
            do k=kt,kbmin(i)-1
              az(k,i)  = azmin
              saz(k,i) = azmin
            end do
          end do
        end if

!****** density

        do i=iu,id
          do k=kt,kb(i)
            if (constituents) then
              rho(k,i) = density (t2(k,i),ss(k,i),tds(k,i))
            else
              rho(k,i) = density (t2(k,i),0.0,0.0)
            end if
          end do
        end do

!****** horizontal diffusivities

        do i=iu,id-1
          do k=kt,kbmin(i)
            dx(k,i) = dxi
          end do
        end do

!****** saved velocities and eddy viscosities

        do i=iu-1,id+1
          do k=kt,max(kb(iu),kb(i))
            su(k,i)  = u(k,i)
            sw(k,i)  = w(k,i)
            saz(k,i) = az(k,i)
          end do
        end do
      end do

!**** density related constants

      rhowcp = rhow*cp
      rhoicp = rhoi*cp
      rhorl1 = rhoi*rl1
      do i=2,imp-1
        dlxrho(i) = 0.5/(dlxr(i)*rhow)
      end do

      !if (open_vpr) close (vpr)
      !if (open_lpr) close (lpr)
      call gregorian_date (year)


!***********************************************************************
!*                          task 1.5: outputs                         **
!***********************************************************************
!$message:'    outputs'
!**** open output files (contains ms powerstation fortran specific i/o)
!
!      if (restart_in) then
!        if (snapshot)    open (snp,file=snpfn, access='append')                         !fortran
!        if (time_series) open (tsr,file=tsrfn, access='append')                         !fortran
!        if (vector)      open (vpl,file=vplfn, access='append')                         !fortran
!        if (profile)     open (prf,file=prffn, access='append')                         !fortran
!        if (spreadsheet) open (spr,file=sprfn, access='append')                         !fortran
!        if (contour)     open (cpl,file=cplfn, access='append')                         !fortran
!        if (time_series) then
!          rewind (tsr)
!          do j=1,7
!            read (tsr,5000,end=10000)
!          end do
!          read (tsr,5010) nac,(cn(jc),jc=1,nac)
!          read (tsr,5020,end=10000) (cname1(cn(jc)),cunit2(cn(jc)),        &
!     &                               jc=1,nac)
!          read (tsr,5030,end=10000)  jdayts
!          do while (jdayts.lt.jday)
!            read (tsr,5030,end=10000) jdayts
!          end do
!        end if
10000   continue
!        if (profile) then
!          rewind (prf)
!          read (prf,2500)  title
!          read (prf,2510) (dum,jc=1,ncp+1)
!          read (prf,2520) (dum1,dum1,jc=1,ncp+1)
!          read (prf,2530)  constituents,n,(n,jc=1,nac+1)
!          read (prf,2540)  n,n,(n,i=1,niprf)
!          read (prf,2550) (dummy,i=1,kmp)
!          do while (jdaypr.lt.jday)
!            do jc=1,nac
!              if (cprc(cn(jc)).eq.'t') then
!                do jprf=1,niprf
!                  read (prf,2560,end=10003) n,n,(dummy,k=1,n)
!                end do
!              end if
!            end do
!            do jprf=1,niprf
!              read (prf,2560,end=10003) n,n,(dummy,k=1,n)
!            end do
!            read (prf,2590,end=10003) jdaypr
!          end do
!          backspace (prf)
!          dummy = dummy+n
!        end if
!10003   continue
!        if (spreadsheet) then
!          rewind (spr)
!          read (spr,2610) (segment(j),j=1,nispr)
!          read (spr,2580,end=10005) jdaysp
!          do while (jdaysp.lt.jday)
!            read (spr,2580,end=10005) jdaysp
!          end do
!          backspace (spr)
!        end if
!10005   continue
!      else
!        if (snapshot)    open (snp,file=snpfn,status='unknown')
!        if (time_series) open (tsr,file=tsrfn,status='unknown')
!        if (vector)      open (vpl,file=vplfn,status='unknown')
!        if (profile)     open (prf,file=prffn,status='unknown')
!        if (spreadsheet) open (spr,file=sprfn,status='unknown')
!        if (contour)     open (cpl,file=cplfn,status='unknown')
!      end if
!
!
!      open (wrn,file='w2.wrn',status='unknown')
!      open (err,file='w2.err',status='unknown')
!
!**** output files
!
!      if (snapshot) then
!        if (laserjet_ii) then
!          write (snp,'(''+'',a80)') esc//'e'//esc//'(s16.66h'//esc//    &
!     &                             '(10u'//esc//'&a8l'//esc//'&l7e'
!        else if (laserjet_iii) then
!          write (snp,'(''+'',a80)') esc//'e'//esc//'&l6.0c'//esc//        &
!     &                             '(s0p16.67h8.5v0s0b0t'//esc//        &
!     &                             '(10u'//esc//'&a8l'//esc//'&l7e'
!        else if (laserjet_iv) then
!          write (snp,'(a80)') esc//'e'//esc//'&l6.0c7e'//esc//            &
!     &                        '(s0p16.67h8.5v0s0b0t'//esc//'(10u'        &
!     &                        //esc//'&a8l'
!        end if
!      end if
!      if (.not.restart_in) then
!        if (profile) then
!          write (prf,2500)  title
!          write (prf,2510) (cprc(jc),jc=1,ncp),'t'
!          write (prf,2520) (cname1(jc),cunit2(jc),jc=1,ncp),            &
!     &                     'temperature     ',char(248)//'c    '        
!          write (prf,2530)  constituents,nac+1,(cn(jc),jc=1,nac),22
!          write (prf,2540)  prfdp,kt,(kb(iprf(i)),i=1,niprf)
!          write (prf,2550)  h
!          do jc=1,nac
!            if (cprc(cn(jc)).eq.'t') then
!              do jprf=1,niprf
!                i   = iprf(jprf)
!                nrs = kb(i)-kt+1
!                write (prf,2560) cn(jc),nrs,(c2(k,i,cn(jc)),k=kt,kb(i))
!              end do
!            end if
!          end do
!          do jprf=1,niprf
!            i   = iprf(jprf)
!            nrs = kb(i)-kt+1
!            write (prf,2560) 22,nrs,(t2(k,i),k=kt,kb(i))
!          end do
!        end if
!        if (spreadsheet) then
!          do j=1,nispr
!            if (ispr(j).lt.10) then
!              write (seg1,'(i1)') ispr(j)
!              segment(j) = 'seg_'//seg1
!            else
!              write (seg2,'(i2)') ispr(j)
!              segment(j) = 'seg_'//seg2
!            end if
!          end do
!          write (spr,2600) (segment(j),j=1,nispr)
!        end if
!        if (time_series) then
!          write (tsr,5000)  title
!          write (tsr,5010)  nac,(cn(jc),jc=1,nac)
!          write (tsr,5020) (cname1(cn(jc)),cunit2(cn(jc)),jc=1,nac)
!        end if
!        if (contour) then
!          write (cpl,5000) title
!          write (cpl,8000) nbp
!          write (cpl,8000) imp,kmp
!          do jb=1,nbp
!            write(cpl,8010) us(jb),ds(jb)
!            write(cpl,8010) (kb(i),i=us(jb),ds(jb))
!          end do
!          write (cpl,8020) dlx
!          write (cpl,8020) h
!          write (cpl,8000) nac
!          write (cpl,8030) (cname1(cn(jc)),jc=1,nac)
!        end if
!        if (vector) then
!          write (vpl,*) title
!          write (vpl,*) h,kb,us,ds,dlx
!        end if
!      end if
        nxmet1 = 0.0d0
        nxqwd1 = 0.0d0
        nxqtr1 = 0.0d0
        nxttr1 = 0.0d0
        nxctr1 = 0.0d0
        nxqin1 = 0.0d0
        nxtin1 = 0.0d0
        nxcin1 = 0.0d0
        nxqot1 = 0.0d0
        nxqdt1 = 0.0d0
        nxtdt1 = 0.0d0
        nxcdt1 = 0.0d0
        nxpr1 = 0.0d0
        nxtpr1 = 0.0d0
        nxcpr1 = 0.0d0
        nxeuh1 = 0.0d0
        nxtuh1 = 0.0d0
        nxcuh1 = 0.0d0
        nxedh1 = 0.0d0
        nxtdh1 = 0.0d0
        nxcdh1 = 0.0d0

        oldkt = kt
        !write(*,*) 'voltr in intVariables end', voltr, shape(voltr)
    end subroutine