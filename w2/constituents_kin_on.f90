    subroutine constituentcalculations(kinetics, add_ssp, add_ssgw)
        use shared_data
        implicit none
        logical, intent(in) :: kinetics
        logical, intent(in) :: add_ssp
        logical, intent(in) :: add_ssgw
        !f2py intent(in) kinetics
	!f2py intent(in) :: add_ssp
        !f2py intent(in) :: add_ssgw
        

    update_kinetics = kinetics
!<mueller>
!    <date>2001/02/19</date>
!    <reason>setting source sinks to zero
!            because otherwise ss would become 
!            larger and larger for constituents
!            without native w2 subroutine.
!            ss from old timestep would be added.
!    </reason>
       cssk = 0.0d0
!<mueller>


if (constituents) then
        palt = (1.-((el(kt)-z(ds(1)))/1000.0)/44.3)**5.25
        outer: do jb=1,nbp 
          if (update_kinetics) then

              if (update_rates) then
                call rate_multipliers
                call decay_constants
              end if

!<mueller>
!    <date>2001/02/06</date>
!    <reason>calling subrotine for suspended soilids
!            ss change depending on total concentration
!    </reason>
            do jac=1,nac
              jc = cn(jac)
                if (jc.eq.2) call suspended_solids
!</mueller>
                if (jc.eq.3)  call coliform
                if (jc.eq.5)  call labile_dom
                if (jc.eq.6)  call refractory_dom
                if (jc.eq.7)  call phytoplankton
                if (jc.eq.8)  call labile_pom
                if (jc.eq.9)  call phosphorus
                if (jc.eq.10) call ammonium
                if (jc.eq.11) call nitrate
                if (jc.eq.12) call dissolved_oxygen
                if (jc.eq.13) call sediment
                if (jc.eq.14) call inorganic_carbon
!<mueller>
!    <date>2001/02/06</date>
!    <reason>switching off ph and iron if phreeqc runs
!     <reason>
!              if (.not. phreeqc_coupling) then
!
!                if (jc.eq.16) call ph_co2
!                if (jc.eq.20) call iron
!               endif
!</mueller>
                if (jc.eq.21) call biochemical_o2_demand
              end do


!<mueller>
!    <date>2001/02/15></date>
!    <reason>adding sink source terms from phreeqc -> p
!             and pcg -> gw</reason>


       end if   !update kinetics
     enddo outer

       
    if (add_ssp) then
       do jb=1,nbp
          iu = cus(jb)
          id = ds(jb)
          do i=iu,id
            do k=kt,kb(i)
               ssss(k,i)  = ssss(k,i)     +    ssssp(k,i)  /(dlt)  
               colss(k,i) = colss(k,i)    +    colssp(k,i) /(dlt)
               ldomss(k,i)= ldomss(k,i)   +    ldomssp(k,i)/(dlt)
               rdomss(k,i)= rdomss(k,i)   +    rdomssp(k,i)/(dlt)
               algss(k,i) = algss(k,i)    +    algssp(k,i) /(dlt)
               lpomss(k,i)= lpomss(k,i)   +    lpomssp(k,i)/(dlt)
               po4ss(k,i) = po4ss(k,i)    +    po4ssp(k,i) /(dlt)
               nh4ss(k,i) = nh4ss(k,i)    +    nh4ssp(k,i) /(dlt)
               no3ss(k,i) = no3ss(k,i)    +    no3ssp(k,i) /(dlt)
               doss(k,i)  = doss(k,i)     +    dossp(k,i)  /(dlt)
               ticss(k,i) = ticss(k,i)    +    ticssp(k,i) /(dlt)
               co3ss(k,i) = co3ss(k,i)    +    co3ssp(k,i) /(dlt)
               fess(k,i)  = fess(k,i)     +    fessp(k,i)    /(dlt)
               cbodss(k,i)= cbodss(k,i)   +    cbodssp(k,i)/(dlt)
               trass(k,i) = trass(k,i)    +    trassp(k,i)  /(dlt)
               alss(k,i)  = alss(k,i)     +    alssp(k,i)  /(dlt)
               so4ss(k,i) = so4ss(k,i)    + so4ssp(k,i)  /(dlt)
               cass(k,i)  = cass(k,i)     + cassp(k,i)   /(dlt)
               mgss(k,i)  = mgss(k,i)     + mgssp(k,i)   /(dlt)
               nass(k,i)  = nass(k,i)     + nassp(k,i)   /(dlt)
               !naTotalSum = naTotalSum + (nass(k,i) * vactive(k,i))
               kass(k,i)  = kass(k,i)     + kassp(k,i)   /(dlt)
               mnss(k,i)  = mnss(k,i)     + mnssp(k,i)   /(dlt)
               clss(k,i)  = clss(k,i)     + clssp(k,i)   /(dlt)
               fe3ss(k,i) = fe3ss(k,i)    + fe3ssp(k,i)  /(dlt)
               mn3ss(k,i) = mn3ss(k,i)    + mn3ssp(k,i)  /(dlt)
               sidss(k,i)  = sidss(k,i)   + sidssp(k,i)  /(dlt)
               ch4ss(k,i) = ch4ss(k,i)    + ch4ssp(k,i)  /(dlt)
               cdss(k,i)  = cdss(k,i)     + cdssp(k,i)   /(dlt)
               pbss(k,i)  = pbss(k,i)     + pbssp(k,i)   /(dlt)
               znss(k,i)  = znss(k,i)     + znssp(k,i)   /(dlt)
               arsss(k,i) = arsss(k,i)    + arsssp(k,i)  /(dlt)
               feoh3ss(k,i) = feoh3ss(k,i) + feoh3ssp(k,i)/(dlt)
               aloh3ss(k,i) = aloh3ss(k,i) + aloh3ssp(k,i) /(dlt)  
               pyritess(k,i) = pyritess(k,i) + pyritessp(k,i)/(dlt)
               po4precipss(k,i) = po4precipss(k,i) + po4precipssp(k,i)/(dlt)
               do jac=1,nac
                   jc = cn(jac)
                   cssphc(jc,jb) = cssphc(jc,jb) + ssp(k,i,jc) * dlt
               end do
            enddo
          enddo
        enddo
        !write(*,*) 'naTotalSum', naTotalSum/1d6
        !write(*,*) 'sodaTotalSum', 2.3051679440447503d0* naTotalSum/1d6
        end if !add_ssp

        if (add_ssgw) then
        ! moving down qgw if cells fall dry
        ! during recent time step
        do jb=1,nbp
          iu = cus(jb)
          id = ds(jb)
          do i=iu,id
            do k=moved, kt-1
                if (vactive(k,i) > 0.0 ) then
                    qgw(kt,i) = qgw(kt,i) + qgw(k,i)*vactive(kt,i)/vactive(k,i)
                    trassgw(kt,i)   = trassgw(kt,i)  + trassgw(k,i)
                    ssssgw(kt,i)   = ssssgw(kt,i)  + ssssgw(k,i)
                    colssgw(kt,i)  = colssgw(kt,i) + colssgw(k,i)
                    ldomssgw(kt,i) = ldomssgw(kt,i)+ ldomssgw(k,i)
                    rdomssgw(kt,i) = rdomssgw(kt,i)+ rdomssgw(k,i)
                    lpomssgw(kt,i) = lpomssgw(kt,i)+ lpomssgw(k,i)
                    po4ssgw(kt,i)  = po4ssgw(kt,i) + po4ssgw(k,i)
                    nh4ssgw(kt,i)  = nh4ssgw(kt,i) + nh4ssgw(k,i)
                    no3ssgw(kt,i)  = no3ssgw(kt,i) + no3ssgw(k,i)
                    dossgw(kt,i)   = dossgw(kt,i)  + dossgw(k,i)
                    ticssgw(kt,i)  = ticssgw(kt,i) + ticssgw(k,i)
                    fessgw(kt,i)   = fessgw(kt,i)  + fessgw(k,i)
                    cbodssgw(kt,i) = cbodssgw(kt,i)+ cbodssgw(k,i)
                    trassgw(kt,i)  = trassgw(kt,i) + trassgw(k,i)
                    alssgw(kt,i)   = alssgw(kt,i)  + alssgw(k,i)
                    so4ssgw(kt,i)  = so4ssgw(kt,i) + so4ssgw(k,i)
                    cassgw(kt,i)   = cassgw(kt,i)  + cassgw(k,i)
                    mgssgw(kt,i)   = mgssgw(kt,i)  + mgssgw(k,i)
                    nassgw(kt,i)   = nassgw(kt,i)  + nassgw(k,i)
                    kassgw(kt,i)   = kassgw(kt,i)  + kassgw(k,i)
                    mnssgw(kt,i)   = mnssgw(kt,i)  + mnssgw(k,i)
       !write(*,*) 'before', clssgw(kt,i)
                   clssgw(kt,i)   = clssgw(kt,i)  + clssgw(k,i)
       !write(*,*) 'after', clssgw(kt,i)
                    fe3ssgw(kt,i) = fe3ssgw(kt,i)  + fe3ssgw(k,i)
                    mn3ssgw(kt,i) = mn3ssgw(kt,i)  + mn3ssgw(k,i)
                    sidssgw(kt,i) = sidssgw(kt,i)  + sidssgw(k,i)
                    ch4ssgw(kt,i) = ch4ssgw(kt,i)  + ch4ssgw(k,i)
                endif
                !moved  = moved +1
              enddo
            enddo
          enddo
          ! end moving
          overbranches: do jb=1,nbp
            iu = cus(jb)
            id = ds(jb)
            overcolumns: do i=iu,id
             overlayers: do k=kt,kb(i)
              vactivecheck: if (vactive(k,i) > 0.0) then
               qgwincheck: if (qgwin(k,i) > 0.0) then
               trass(k,i)  = trass(k,i)     + trassgw(k,i)  /vactive(k,i) 
               ssss(k,i)  = ssss(k,i)       + ssssgw(k,i)  /vactive(k,i)  
               colss(k,i) = colss(k,i)      + colssgw(k,i) /vactive(k,i)  
               ldomss(k,i)= ldomss(k,i)     + ldomssgw(k,i)/vactive(k,i)  
               rdomss(k,i)= rdomss(k,i)     + rdomssgw(k,i)/vactive(k,i)  
!               algss(k,i) = algss(k,i)      + algssgw(k,i)/vactive(k,i)  
               lpomss(k,i)= lpomss(k,i)     + lpomssgw(k,i)/vactive(k,i)  
               po4ss(k,i) = po4ss(k,i)      + po4ssgw(k,i) /vactive(k,i)  
               nh4ss(k,i) = nh4ss(k,i)      + nh4ssgw(k,i) /vactive(k,i)  
               no3ss(k,i) = no3ss(k,i)      + no3ssgw(k,i) /vactive(k,i)  
               doss(k,i)  = doss(k,i)       + dossgw(k,i)  /vactive(k,i)  
               ticss(k,i) = ticss(k,i)      + ticssgw(k,i) /vactive(k,i)  
               fess(k,i)  = fess(k,i)       + fessgw(k,i)  /vactive(k,i)  
               cbodss(k,i)= cbodss(k,i)     + cbodssgw(k,i)/vactive(k,i)  
               trass(k,i) = trass(k,i)      + trassgw(k,i) /vactive(k,i)  
               alss(k,i)  = alss(k,i)       + alssgw(k,i)  /vactive(k,i)  
               so4ss(k,i) = so4ss(k,i)      + so4ssgw(k,i) /vactive(k,i)  
               cass(k,i)  = cass(k,i)       + cassgw(k,i)  /vactive(k,i)  
               mgss(k,i)  = mgss(k,i)       + mgssgw(k,i)  /vactive(k,i)  
               nass(k,i)  = nass(k,i)       + nassgw(k,i)  /vactive(k,i) 
               kass(k,i)  = kass(k,i)       + kassgw(k,i)  /vactive(k,i)  
               mnss(k,i)  = mnss(k,i)       + mnssgw(k,i)  /vactive(k,i) 
               clss(k,i)  = clss(k,i)       + clssgw(k,i)  /vactive(k,i) 
               fe3ss(k,i) = fe3ss(k,i)      + fe3ssgw(k,i) /vactive(k,i)  
               mn3ss(k,i) = mn3ss(k,i)      + mn3ssgw(k,i) /vactive(k,i)  
               sidss(k,i) = sidss(k,i)      + sidssgw(k,i) /vactive(k,i)  
               ch4ss(k,i) = ch4ss(k,i)      + ch4ssgw(k,i) /vactive(k,i)  
               cdss(k,i)  = cdss(k,i)       + cdssgw(k,i)  /vactive(k,i) 
               pbss(k,i)  = pbss(k,i)       + pbssgw(k,i)  /vactive(k,i) 
               znss(k,i)  = znss(k,i)       + znssgw(k,i)  /vactive(k,i) 
               arsss(k,i) = arsss(k,i)      + arsssgw(k,i) /vactive(k,i) 
               do jac=1,nac
                   jc = cn(jac)
                   cssgwin(jc,jb) = cssgwin(jc,jb) + ssgw(k,i,jc) * dlt
               end do
             end if qgwincheck
             qgwoutcheck: if (qgwout(k,i) < 0.0) then
!              ssss(k,i)  = ssss(k,i)      + ss(k,i)  *qgwout(k,i)/vactive(k,i)  
               colss(k,i) = colss(k,i)      + col(k,i)  *qgwout(k,i)/vactive(k,i)  
               ldomss(k,i)= ldomss(k,i)   + ldom(k,i)   *qgwout(k,i)/vactive(k,i)  
               rdomss(k,i)= rdomss(k,i)   + rdom(k,i)   *qgwout(k,i)/vactive(k,i)  
!               algss(k,i) = algss(k,i)      + algae(k,i) *qgwout(k,i)/vactive(k,i)  
!               lpomss(k,i)= lpomss(k,i)   + lpom(k,i)  *qgwout(k,i)/vactive(k,i)  
               po4ss(k,i) = po4ss(k,i)      + po4(k,i)  *qgwout(k,i)/vactive(k,i)  
               nh4ss(k,i) = nh4ss(k,i)      + nh4(k,i)  *qgwout(k,i)/vactive(k,i)  
               no3ss(k,i) = no3ss(k,i)      + no3(k,i)  *qgwout(k,i)/vactive(k,i)  
               doss(k,i)  = doss(k,i)      + dox(k,i)    *qgwout(k,i)/vactive(k,i)  
               ticss(k,i) = ticss(k,i)      + tic(k,i)  *qgwout(k,i)/vactive(k,i)  
               fess(k,i)  = fess(k,i)      + fe(k,i)    *qgwout(k,i)/vactive(k,i)  
!               cbodss(k,i)= cbodss(k,i)   + cbod(k,i)  *qgwout(k,i)/vactive(k,i)  
               trass(k,i) = trass(k,i)      + tra(k,i)  *qgwout(k,i)/vactive(k,i)  
               alss(k,i)  = alss(k,i)      + al(k,i)    *qgwout(k,i)/vactive(k,i)  
               so4ss(k,i) = so4ss(k,i)      + so4(k,i)  *qgwout(k,i)/vactive(k,i)  
               cass(k,i)  = cass(k,i)      + ca(k,i)    *qgwout(k,i)/vactive(k,i)  
               mgss(k,i)  = mgss(k,i)      + mg(k,i)    *qgwout(k,i)/vactive(k,i)  
               nass(k,i)  = nass(k,i)      + na(k,i)    *qgwout(k,i)/vactive(k,i)  
               kass(k,i)  = kass(k,i)      + ka(k,i)    *qgwout(k,i)/vactive(k,i)  
               mnss(k,i)  = mnss(k,i)      + mn(k,i)    *qgwout(k,i)/vactive(k,i)  
               clss(k,i)  = clss(k,i)      + cl(k,i)    *qgwout(k,i)/vactive(k,i) 
               fe3ss(k,i) = fe3ss(k,i)      + fe3(k,i)  *qgwout(k,i)/vactive(k,i)  
               mn3ss(k,i) = mn3ss(k,i)      + mn3(k,i)  *qgwout(k,i)/vactive(k,i)  
               sidss(k,i) = sidss(k,i)      + sid(k,i)  *qgwout(k,i)/vactive(k,i)  
               ch4ss(k,i) = ch4ss(k,i)      + ch4(k,i)  *qgwout(k,i)/vactive(k,i) 
               cdss(k,i)  = cdss(k,i)      + cd(k,i)    *qgwout(k,i)/vactive(k,i) 
               pbss(k,i)  = pbss(k,i)      + pb(k,i)    *qgwout(k,i)/vactive(k,i) 
               znss(k,i)  = znss(k,i)      + zn(k,i)    *qgwout(k,i)/vactive(k,i) 
               arsss(k,i)  = arsss(k,i)      + ars(k,i)    *qgwout(k,i)/vactive(k,i) 
               
!               ssssgwout(k,i)   = ss(k,i)   * qgwout(k,i) * dlt    
               colssgwout(k,i)  = col(k,i)  * qgwout(k,i) * dlt  
               ldomssgwout(k,i) = ldom(k,i) * qgwout(k,i) * dlt 
               rdomssgwout(k,i) = rdom(k,i) * qgwout(k,i) * dlt  
!               algssgwout(k,i)  = alg(k,i)  * qgwout(k,i) * dlt   
!               lpomssgwout(k,i) = lpom(k,i) * qgwout(k,i) * dlt 
               po4ssgwout(k,i)  = po4(k,i)  * qgwout(k,i) * dlt  
               nh4ssgwout(k,i)  = nh4(k,i)  * qgwout(k,i) * dlt   
               no3ssgwout(k,i)  = no3(k,i)  * qgwout(k,i) * dlt   
               dossgwout(k,i)   = dox(k,i)   * qgwout(k,i) * dlt 
               ticssgwout(k,i)  = tic(k,i)  * qgwout(k,i) * dlt   
               fessgwout(k,i)   = fe(k,i)   * qgwout(k,i) * dlt   
!               cbodssgwout(k,i) = cbod(k,i) * qgwout(k,i) * dlt   
               trassgwout(k,i)  = tra(k,i)  * qgwout(k,i) * dlt  
               alssgwout(k,i)   = al(k,i)   * qgwout(k,i) * dlt   
               so4ssgwout(k,i)  = so4(k,i)  * qgwout(k,i) * dlt  
               cassgwout(k,i)   = ca(k,i)   * qgwout(k,i) * dlt  
               mgssgwout(k,i)   = mg(k,i)   * qgwout(k,i) * dlt 
               nassgwout(k,i)   = na(k,i)   * qgwout(k,i) * dlt  
               kassgwout(k,i)   = ka(k,i)   * qgwout(k,i) * dlt   
               mnssgwout(k,i)   = mn(k,i)   * qgwout(k,i) * dlt  
               clssgwout(k,i)   = cl(k,i)   * qgwout(k,i) * dlt  
               fe3ssgwout(k,i)  = fe3(k,i)  * qgwout(k,i) * dlt 
               mn3ssgwout(k,i)  = mn3(k,i)  * qgwout(k,i) * dlt  
               sidssgwout(k,i)  = sid(k,i)  * qgwout(k,i) * dlt  
               ch4ssgwout(k,i)  = ch4(k,i)  * qgwout(k,i) * dlt  
               cdssgwout(k,i)   = cd(k,i)   * qgwout(k,i) * dlt  
               pbssgwout(k,i)   = pb(k,i)   * qgwout(k,i) * dlt  
               znssgwout(k,i)   = zn(k,i)   * qgwout(k,i) * dlt  
               arsssgwout(k,i)  = ars(k,i)  * qgwout(k,i) * dlt  
               do jac=1,nac
                   jc = cn(jac)
                   cssgwout(jc,jb) = cssgwout(jc,jb) + ssgw_out(k,i,jc)
               end do
              endif qgwoutcheck
             endif vactivecheck
            enddo overlayers
 
           if (oldkt < kt) then
                !write(*,*) 'moved old', i, oldkt, kt
!               ssssgwout(kt,i) = ssssgwout(kt,i) + ssssgwout(oldkt,i)
               colssgwout(kt,i) = colssgwout(kt,i) + colssgwout(oldkt,i)
               ldomssgwout(kt,i) = ldomssgwout(kt,i) + ldomssgwout(oldkt,i)
               rdomssgwout(kt,i) = rdomssgwout(kt,i) + rdomssgwout(oldkt,i)
!               algssgwout(kt,i) = algssgwout(kt,i) + algssgwout(oldkt,i)
!               lpomssgwout(kt,i) = lpomssgwout(kt,i) + lpomssgwout(oldkt,i)
               po4ssgwout(kt,i) = po4ssgwout(kt,i) + po4ssgwout(oldkt,i)
               nh4ssgwout(kt,i) = nh4ssgwout(kt,i) + nh4ssgwout(oldkt,i)
               no3ssgwout(kt,i) = no3ssgwout(kt,i) + no3ssgwout(oldkt,i)
               dossgwout(kt,i) = dossgwout(kt,i) + dossgwout(oldkt,i)
               ticssgwout(kt,i) = ticssgwout(kt,i) + ticssgwout(oldkt,i)
               fessgwout(kt,i) = fessgwout(kt,i) + fessgwout(oldkt,i)
!               cbodssgwout(kt,i) = cbodssgwout(kt,i) + cbodssgwout(oldkt,i)
               trassgwout(kt,i) = trassgwout(kt,i) + trassgwout(oldkt,i)
               alssgwout(kt,i) = alssgwout(kt,i) + alssgwout(oldkt,i)
               so4ssgwout(kt,i) = so4ssgwout(kt,i) + so4ssgwout(oldkt,i)
               cassgwout(kt,i) = cassgwout(kt,i) + cassgwout(oldkt,i)
               mgssgwout(kt,i) = mgssgwout(kt,i) + mgssgwout(oldkt,i)
               nassgwout(kt,i) = nassgwout(kt,i) + nassgwout(oldkt,i)
               kassgwout(kt,i) = kassgwout(kt,i) + kassgwout(oldkt,i)
               mnssgwout(kt,i) = mnssgwout(kt,i) + mnssgwout(oldkt,i)
               clssgwout(kt,i) = clssgwout(kt,i) + clssgwout(oldkt,i)
               fe3ssgwout(kt,i) = fe3ssgwout(kt,i) + fe3ssgwout(oldkt,i)
               mn3ssgwout(kt,i) = mn3ssgwout(kt,i) + mn3ssgwout(oldkt,i)
               sidssgwout(kt,i) = sidssgwout(kt,i) + sidssgwout(oldkt,i)
               ch4ssgwout(kt,i) = ch4ssgwout(kt,i) + ch4ssgwout(oldkt,i)
               cdssgwout(kt,i) = cdssgwout(kt,i) + cdssgwout(oldkt,i)
               pbssgwout(kt,i) = pbssgwout(kt,i) + pbssgwout(oldkt,i)
               znssgwout(kt,i) = znssgwout(kt,i) + znssgwout(oldkt,i)
               arsssgwout(kt,i) = arsssgwout(kt,i) + arsssgwout(oldkt,i)
             end if
          enddo overcolumns
          oldkt = kt
        enddo overbranches
        end if !add_ssgw
            do jac=1,nac
              jc = cn(jac)

!************ external sources/sinks
           do jb=1,nbp 
             iu = cus(jb)
             id = ds(jb)
              if (tributaries) then
                do jt=1,ntr
                  if (jb.eq.jbtr(jt)) then
                    i = itr(jt)
                    if (i.lt.cus(jb)) i = cus(jb)
                    do k=kttr(jt),kbtr(jt)
                      if (qtr(jt).lt.0.0) then
                        cssb(k,i,jc) = cssb(k,i,jc)+c1(k,i,jc)*qtr(jt)*qtrf(k,jt)
                        csstr(jc,jb) = csstr(jc,jb)+c1(k,i,jc)*qtr(jt)*qtrf(k,jt)
                        trib_specie_balance(jt,jc) = trib_specie_balance(jt,jc)+c1(k,i,jc)*qtr(jt)*qtrf(k,jt)*dlt
                      else
                        cssb(k,i,jc) = cssb(k,i,jc)+ctr(jc,jt)*qtr(jt)*qtrf(k,jt)
                        csstr(jc,jb) = csstr(jc,jb)+ctr(jc,jt)*qtr(jt)*qtrf(k,jt)
                        trib_specie_balance(jt,jc) = trib_specie_balance(jt,jc)+ctr(jc,jt)*qtr(jt)*qtrf(k,jt)*dlt
                      end if
                    end do
                  end if
                end do
              end if
              if (dist_tribs(jb)) then
                do i=iu,id
                  cssb(kt,i,jc) = cssb(kt,i,jc)+cdtr(jc,jb)*qdt(i)
                  cssdt(jc,jb) = cssdt(jc,jb)+cdtr(jc,jb)*qdt(i)
                end do
              end if
              if (withdrawals) then
                do jw=1,nwd
                  if (jb.eq.jbwd(jw)) then
                    i            = max(cus(jbwd(jw)),iwd(jw))
                    k            = max(kt,kwd(jw))
                    cssb(k,i,jc) = cssb(k,i,jc)-c1s(k,i,jc)*qwd(jw)
                    csswd(jc,jb) = csswd(jc,jb)-c1s(k,i,jc)*qwd(jw)
                  end if
                end do
              end if
              if (precipitation) then
                do i=iu,id
                  cssb(kt,i,jc) = cssb(kt,i,jc)+cpr(jc,jb)*qpr(i)
                  csspr(jc,jb) = csspr(jc,jb)+cpr(jc,jb)*qpr(i)
                end do
              end if
              if (up_flow(jb)) then
                do k=kt,kb(iu)
                  cssb(k,iu,jc) = cssb(k,iu,jc)+qinf(k,jb)*qin(jb)*cin(jc,jb)
                  cssin(jc,jb) = cssin(jc,jb)+qinf(k,jb)*qin(jb)*cin(jc,jb)
                end do
              end if
              if (dn_flow(jb)) then
                do k=kt,kb(id)
                  cssb(k,id,jc) = cssb(k,id,jc)-qout(k,jb)*c1s(k,id,jc)
                  cssout(jc,jb) = cssout(jc,jb)-qout(k,jb)*c1s(k,id,jc)
                end do
              end if
              if (up_head(jb)) then
                iut = iu
                if (quh1(kt,jb).ge.0.0) iut = iu-1
                cssuh1(kt,jc,jb) = c1s(kt,iut,jc)*quh1(kt,jb)
                cssb(kt,iu,jc)   = cssb(kt,iu,jc)+cssuh1(kt,jc,jb)
                cssuh(jc,jb) = cssuh(jc,jb)+cssuh1(kt,jc,jb)
                do k=kt+1,kb(iu)
                  iut = iu
                  if (quh1(k,jb).ge.0.0) iut = iu-1
                  cssuh1(k,jc,jb) = c1s(k,iut,jc)*quh1(k,jb)
                  cssb(k,iu,jc)   = cssb(k,iu,jc)+cssuh1(k,jc,jb)
                  cssuh(jc,jb) = cssuh(jc,jb)+cssuh1(k,jc,jb)
                end do
                if (uh_internal(jb)) then
                  i = uhs(jb)
                  do k=kt,kb(iu)
                    cssb(k,i,jc) = cssb(k,i,jc)-cssuh2(k,jc,jb)/dlt
                    cssuh(jc,jb) = cssuh(jc,jb)-cssuh2(k,jc,jb)/dlt
                  end do
                end if
              end if
              if (dn_head(jb)) then
                idt = id+1
                if (qdh1(kt,jb).ge.0.0) idt = id
                cssdh1(kt,jc,jb) = c1s(kt,idt,jc)*qdh1(kt,jb)
                cssb(kt,id,jc)   = cssb(kt,id,jc)-cssdh1(kt,jc,jb)
                cssdh(jc,jb) = cssdh(jc,jb)-cssdh1(kt,jc,jb)
                do k=kt+1,kb(id+1)
                  idt = id+1
                  if (qdh1(k,jb).ge.0.0) idt = id
                  cssdh1(k,jc,jb) = c1s(k,idt,jc)*qdh1(k,jb)
                  cssb(k,id,jc)   = cssb(k,id,jc)-cssdh1(k,jc,jb)
                  cssdh(jc,jb) = cssdh(jc,jb)-cssdh1(k,jc,jb)
                end do
                if (dh_internal(jb)) then
                  i = dhs(jb)
                  do k=kt,kb(id+1)
                    cssb(k,i,jc) = cssb(k,i,jc)+cssdh2(k,jc,jb)/dlt
                    cssdh(jc,jb) = cssdh(jc,jb)+cssdh2(k,jc,jb)/dlt
                  end do
                end if
              end if
            end do
          end do

!******** transport constituents

!$message:'      transport'

!<mmueller>
!ultimate interpolation multipliers horizontal
        if (ultimate) then
           do i=iu,id
             ratd(i)  =  dlxr(i-1)/dlxr(i)
             curl3(i) =  2.0*dlx(i)**2/(dlxr(i)+dlxr(i-1))/dlxr(i)
             curl2(i) = -2.0*dlx(i)**2/(dlxr(i)*dlxr(i-1))
             curl1(i) =  2.0*dlx(i)**2/(dlxr(i)+dlxr(i-1))/dlxr(i-1)
           end do
!ultimate interpolation multipliers vertical
           do k=2,kmp-1
             ratv(k)  =  avh(k-1)/avh(k)
             curv3(k) =  2.0*h(k)**2/(avh(k-1)+avh(k))/avh(k)
             curv2(k) = -2.0*h(k)**2/(avh(k-1)*avh(k))
             curv1(k) =  2.0*h(k)**2/(avh(k-1)+avh(k))/avh(k-1)
           end do
        end if
!</mmueller>

          do jb=1,nbp
            iu = cus(jb)
            id = ds(jb)
            do jac=1,nac
              jc = cn(jac)
              if (transport(jc)) then

!************** horizontal advection and diffusion terms

                do i=iu,id-1
                  do k=kt,kb(i)
                    if (u(k,i).ge.0.0) then
                      c1l = c1s(k,i-1,jc)
                      c2l = c1s(k,i,jc)
                      c3l = c1s(k,i+1,jc)
                      if (u(k,i-1).le.0.0) c1l = c1s(k,i,jc)
                    else
                      c1l = c1s(k,i,jc)
                      c2l = c1s(k,i+1,jc)
                      c3l = c1s(k,i+2,jc)
                      if (u(k,i+2).ge.0.0) c3l = c1s(k,i+1,jc)
                    end if
                    cadl(k,i,jc) =  (dx1(k,i)-u(k,i)*ad1l(k,i))*c1l        &
     &                             +(dx2(k,i)-u(k,i)*ad2l(k,i))*c2l        &
     &                             +(dx3(k,i)-u(k,i)*ad3l(k,i))*c3l
                  end do
                end do

!<mmueller>
!ultimate multipliers horizontal
        if (ultimate) then
          do i=iu,id-1
            do k=kt,kb(i)
              cour = u(k,i)*dlt/dlxr(i)
              if (u(k,i) >= 0.0) then
                c1l = c1s(k,i-1,jc)
                c2l = c1s(k,i,jc)
                c3l = c1s(k,i+1,jc)
                if (u(k,i-1) <= 0.0 .or. k > kb(i-1)) c1l = c1s(k,i,jc)
                cart      =  c3l
                calf      =  c1l
                rats      =  ratd(i)
                curs3     =  curl3(i)
                curs2     =  curl2(i)
                curs1     =  curl1(i)
                dx1(k,i)  =  dx(k,i)*sf11l(k,i,1)
                dx2(k,i)  =  dx(k,i)*sf12l(k,i,1)
                dx3(k,i)  =  dx(k,i)*sf13l(k,i,1)
                alfa      =  2.0*(dx(k,i)*dlt/(sf1l(k,i)*sf1l(k,i))-(1.0-cour*cour)/6.0)*sf3l(k,i,1)
                ad1l(k,i) = (alfa-cour*sf8l(k,i,1)*0.5)/sf5l(k,i,1)
                ad2l(k,i) =  sf4l(k,i,1)+(alfa-cour*sf9l(k,i,1)*0.5)/sf6l(k,i,1)
                ad3l(k,i) =  sf2l(k,i,1)+(alfa-cour*sf10l(k,i,1)*0.5)/sf7l(k,i,1)
              else
                c1l = c1s(k,i,jc)
                c2l = c1s(k,i+1,jc)
                c3l = c1s(k,i+2,jc)
                if (u(k,i+2) >= 0.0 .or. k > kb(i+2) .or. i == id-1) c1l = c1s(k,i+1,jc)
                cart      =  c1l
                calf      =  c3l
                rats      =  ratd(i+1)
                curs3     =  curl3(i+1)
                curs2     =  curl2(i+1)
                      curs1     =  curl1(i+1)
                dx1(k,i)  =  dx(k,i)*sf11l(k,i,2)
                      dx2(k,i)  =  dx(k,i)*sf12l(k,i,2)
                dx3(k,i)  =  dx(k,i)*sf13l(k,i,2)
                alfa      =  2.0*(dx(k,i)*dlt/(sf1l(k,i)*sf1l(k,i))-(1.0-cour*cour)/6.0)*sf3l(k,i,2)
                ad1l(k,i) =  sf2l(k,i,2)+(alfa-cour*sf8l(k,i,2)*0.5)/sf5l(k,i,2)
                ad2l(k,i) =  sf4l(k,i,2)+(alfa-cour*sf9l(k,i,2)*0.5)/sf6l(k,i,2)
                      ad3l(k,i) = (alfa-cour*sf10l(k,i,2)*0.5)/sf7l(k,i,2)
              end if
              cadl(k,i,jc) = (dx1(k,i)-u(k,i)*ad1l(k,i))*c1l+(dx2(k,i)-u(k,i)*ad2l(k,i))*c2l+(dx3(k,i)-u(k,i)*ad3l(k,i))*c3l
              ratdi = 1.0/rats
              delc  = rats*c3l+(ratdi-rats)*c2l-ratdi*c1l
              delc  = sign(1.0d0,u(k,i))*delc
              adelc = abs(delc)
              acurv = abs(curs3*c3l+curs2*c2l+curs1*c1l)
              if (acurv <= 0.6*adelc) then
                flux = ad1l(k,i)*c1l+ad2l(k,i)*c2l+ad3l(k,i)*c3l
              else if (acurv >= adelc) then
                flux = c2l
              else if (abs(cour) > 0.0) then
                ftemp = ad1l(k,i)*c1l+ad2l(k,i)*c2l+ad3l(k,i)*c3l
                cref = calf+(c2l-calf)/abs(cour)
                if (delc > 0.0) then
                  cmax1 = min(cref,cart)
                  if (cref < c2l) cmax1 = cart
                  flux = 0.5*(c2l+cmax1)
                  if (ftemp <= cmax1 .and. ftemp >= c2l) flux = ftemp
                else
                  cmin1 = max(cref,cart)
                  if (cref > c2l) cmin1 = cart
                  if (ftemp >= cmin1 .and. ftemp <= c2l) then
                          flux = ftemp
                  else if (ftemp > 0.0) then
                    flux = 0.5*(c2l+cmin1)
                  else
                    flux = 0.0
                  end if
                end if
              else
                flux = 0.0
              end if
              cadl(k,i,jc) = (dx1(k,i)*c1l+dx2(k,i)*c2l+dx3(k,i)*c3l)-u(k,i)*flux
            end do
          end do
        end if
!</mmueller>

!************** vertical advection terms

                do i=iu,id
                  do k=kt,kb(i)-1
                    if (w(k,i).ge.0.0) then
                      c1v = c1s(k-1,i,jc)
                      c2v = c1s(k,i,jc)
                      c3v = c1s(k+1,i,jc)
                      if (k.le.kt+1) c1v = c1s(kt,i,jc)
                    else
                      c1v = c1s(k,i,jc)
                      c2v = c1s(k+1,i,jc)
                      c3v = c1s(k+2,i,jc)
                      if (k.eq.kb(i)-1) c3v = c1s(k+1,i,jc)
                    end if
                    cadv(k,i,jc) = -w(k,i)*(ad1v(k,i)*c1v+ad2v(k,i)*c2v    &
     &                             +ad3v(k,i)*c3v)
                  end do
                end do
              end if
            end do
          end do

!<mmueller>
!ultimate multipliers vertical
        if (ultimate) then
          do i=iu,id
            do k=kt,kb(i)-1
              if (w(k,i) >= 0.0) then
                c1v   = c1s(k-1,i,jc)
                c2v   = c1s(k,i,jc)
                c3v   = c1s(k+1,i,jc)
                cart  = c3v
                calf  = c1v
                calf  = c1v
                rats  = ratv(k)
                curs3 = curv3(k)
                curs2 = curv2(k)
                curs1 = curv1(k)
                if (k <= kt+1) then
                  c1v   =  c1s(kt,i,jc)
                  ht    =  hkt1(i)
                  hm    =  h(k)
                  hb    =  h(k+1)
                  calf  =  c1v
                  rats  =  avhkt(i)/avh(k)
                  curs3 =  2.0*hm*hm/(avhkt(i)+avh(k))/avh(k)
                  curs2 = -2.0*hm*hm/(avhkt(i)*avh(k))
                  curs1 =  2.0*hm*hm/(avhkt(i)+avh(k))/avhkt(i)
                  if (k == kt) then
                    hm    =  hkt1(i)
                    rats  =  1.0
                    curs3 =  1.0
                    curs2 = -2.0
                    curs1 =  1.0
                  end if
                  hmin = min(hb,hm)
                  sf1v(k)    = (hb+hm)*0.5
                  sf2v(k,1)  =  hm**2
                  sf3v(k,1)  =  hm/(hm+hb)
                  sf4v(k,1)  =  hb/(hm+hb)
                  sf5v(k,1)  =  0.25*(ht+2.0*hm+hb)*(ht+hm)
                  sf6v(k,1)  = -0.25*(hm+hb)*(ht+hm)
                  sf7v(k,1)  =  0.25*(hm+hb)*(ht+2.0*hm+hb)
                  sf8v(k,1)  =  0.5*(hm-hb)*hmin
                  sf9v(k,1)  =  0.5*(ht+2.0*hm-hb)*hmin
                  sf10v(k,1) =  0.5*(ht+3.0*hm)*hmin
                end if
                cour      =  w(k,i)*dlt/sf1v(k)
                alfa      =  2.0*(dzq(k,i)*dlt/(sf1v(k)*sf1v(k))-(1.0-cour*cour)/6.0)*sf2v(k,1)
                ad1v(k,i) = (alfa-cour*sf8v(k,1)*0.5)/sf5v(k,1)
                ad2v(k,i) =  sf4v(k,1)+(alfa-cour*sf9v(k,1)*0.5)/sf6v(k,1)
                ad3v(k,i) =  sf3v(k,1)+(alfa-cour*sf10v(k,1)*0.5)/sf7v(k,1)
              else
                c1v = c1s(k,i,jc)
                c2v = c1s(k+1,i,jc)
                c3v = c1s(k+2,i,jc)
                if (k == kb(i)-1) c3v = c1s(k+1,i,jc)
                cart  = c1v
                calf  = c3v
                curs3 = curv3(k+1)
                curs2 = curv2(k+1)
                curs1 = curv1(k+1)
                rats  = avh(k)/avh(k+1)
                if (k == kt) then
                  ht            =  hkt1(i)
                  hm            =  h(kt+1)
                  hb            =  h(kt+2)
                  hmin          =  min(ht,hm)
                  rats          =  avhkt(i)/avh(k)
                  curs3         =  2.0*hm*hm/(avhkt(i)+avh(k))/avh(k)
                  curs2         = -2.0*hm*hm/(avhkt(i)*avh(k))
                  curs1         =  2.0*hm*hm/(avhkt(i)+avh(k))/avhkt(i)
                  sf1v(k)    = (hm+ht)*0.5
                  sf2v(k,2)  =  hm**2
                  sf3v(k,2)  =  hm/(ht+hm)
                  sf4v(k,2)  =  ht/(ht+hm)
                  sf5v(k,2)  =  0.25*(ht+2.0*hm+hb)*(ht+hm)
                  sf6v(k,2)  = -0.25*(hm+hb)*(ht+hm)
                  sf7v(k,2)  =  0.25*(ht+2.0*hm+hb)*(hm+hb)
                  sf8v(k,2)  = -0.5*(3.0*hm+hb)*hmin
                  sf9v(k,2)  =  0.5*(ht-2.0*hm-hb)*hmin
                  sf10v(k,2) =  0.5*(ht-hm)*hmin
                end if
                cour      =  w(k,i)*dlt/sf1v(k)
                alfa      =  2.0*(dzq(k,i)*dlt/(sf1v(k)*sf1v(k))-(1.0-cour*cour)/6.0)*sf2v(k,2)
                ad1v(k,i) =  sf3v(k,2)+(alfa-cour*sf8v(k,2)*0.5)/sf5v(k,2)
                ad2v(k,i) =  sf4v(k,2)+(alfa-cour*sf9v(k,2)*0.5)/sf6v(k,2)
                ad3v(k,i) = (alfa-cour*sf10v(k,2)*0.5)/sf7v(k,2)
              end if
              cadv(k,i, jc) = -w(k,i)*(ad1v(k,i)*c1v+ad2v(k,i)*c2v+ad3v(k,i)*c3v)
              ratvi = 1.0/rats
              delc  = rats*c3v+(ratvi-rats)*c2v-ratvi*c1v
              delc  = sign(1.0d0,w(k,i))*delc
              adelc = abs(delc)
              acurv = abs(curs3*c3v+curs2*c2v+curs1*c1v)
              if (acurv <= 0.6*adelc) then
                flux = ad1v(k,i)*c1v+ad2v(k,i)*c2v+ad3v(k,i)*c3v
              else if (acurv >= adelc) then
                flux = c2v
              else if (abs(cour) > 0.0) then
                ftemp = ad1v(k,i)*c1v+ad2v(k,i)*c2v+ad3v(k,i)*c3v
                cref  = calf+(c2v-calf)/abs(cour)
                if (delc > 0.0) then
                  cmax1 = cart
                  if (cref >= c2v) cmax1 = min(cref,cart)
                  flux = 0.5*(c2v+cmax1)
                  if (ftemp <= cmax1 .and. ftemp >= c2v) flux = ftemp
                else
                  cmin1 = max(cref,cart)
                  if (cref > c2v) cmin1 = cart
                  if (ftemp >= cmin1 .and. ftemp <= c2v) then
                    flux = ftemp
                  else if (ftemp > 0.0) then
                    flux = 0.5*(c2v+cmin1)
                  else
                    flux = 0.0
                  end if
                end if
              else
                flux = 0.0
              end if
              cadv(k,i, jc) = -w(k,i)*flux
            end do
          end do
        end if
!</mmueller>

!******** constituent transport
          do jb=1,nbp
            iu = cus(jb)
            id = ds(jb)
            do jac=1,nac
              jc = cn(jac)
              if (transport(jc)) then
                do i=iu,id
                  c1(kt,i,jc) = ((c1s(kt,i,jc)*bhkt2(i)/dlt                &
     &                          +(cadl(kt,i,jc)*bhrkt1(i)                &
     &                          -cadl(kt,i-1,jc)*bhrkt1(i-1))/dlx(i)    &
     &                          +(1.0-theta)*cadv(kt,i,jc)*bb(kt,i))    &
     &                          /bhkt1(i)+cssb(kt,i,jc)/(dlx(i)            &
     &                          *bhkt1(i))+cssk(kt,i,jc))*dlt
                  do k=kt+1,kb(i)
                    c1(k,i,jc) = ((c1s(k,i,jc)*bh(k,i)/dlt                &
     &                           +(cadl(k,i,jc)*bhr(k,i)-cadl(k,i-1,jc)    &
     &                           *bhr(k,i-1))/dlx(i)+(1.0-theta)        &
     &                           *(cadv(k,i,jc)*bb(k,i)-cadv(k-1,i,jc)    &    
     &                           *bb(k-1,i)))/bh(k,i)+cssb(k,i,jc)        &
     &                           /(dlx(i)*bh(k,i))+cssk(k,i,jc))*dlt
                  end do

!**************** time-weighted vertical advection and implicit diffusion

                  if (.not.one_layer(i)) then
                    dt(kb(i)) = c1(kb(i),i,jc)
                    do k=kt,kb(i)-1
                      dt(k) = c1(k,i,jc)
                    end do
                    gmat(kt) = dt(kt)
                    do k=kt+1,kb(i)
                      gmat(k) = dt(k)-at(k,i)/btat(k-1,i)*gmat(k-1)
                    end do
                    c1(kb(i),i,jc) = gmat(kb(i))/btat(kb(i),i)
                    do k=kb(i)-1,kt,-1
                      c1(k,i,jc) = (gmat(k)-ct(k,i)*c1(k+1,i,jc))        &
     &                             /btat(k,i)
                    end do
                  end if
                end do
              end if
            end do
          end do
        end if

        if (mass_balance) then
          do jb=1,nbp
            do jc=1,nac
              jac = cn(jc)
              if (transport(jac)) then
                cmbrs(jac,jb) = 0.0d0
                do i=cus(jb),ds(jb)
                  cmbrs(jac,jb) = cmbrs(jac,jb)+c1(kt,i,jac)*dlx(i)        &
     &                            *bhkt1(i)
                  cmbrt(jac,jb) = cmbrt(jac,jb)+(cssb(kt,i,jac)            &
     &                            +cssk(kt,i,jac)*bhkt1(i)*dlx(i))*dlt
                  do k=kt+1,kb(i)
                    cmbrs(jac,jb) = cmbrs(jac,jb)+c1(k,i,jac)*dlx(i)    &
     &                              *bh(k,i)
                    cmbrt(jac,jb) = cmbrt(jac,jb)+(cssb(k,i,jac)        &
     &                              +cssk(k,i,jac)*bh(k,i)*dlx(i))*dlt
                  end do
                end do
              end if
            end do
          end do
        end if

!<debug>
            if (mass_balance) then
              do jb=1,nbp
                do jc=1,nac
                  jac = cn(jc)
                  if (transport(jac)) then
                    if (cmbrs(jac,jb).ne.0.0) then 
                      dlmr = (cmbrt(jac,jb)-cmbrs(jac,jb))                &
     &                       /(cmbrs(jac,jb)+nonzero)
                    end if

!                       if (abs(dlmr) > 10) then                     !
!                          write(*,*) cname1(jac)                      !
!                          write(*,*) 'cmbrt', cmbrt(jac,jb)           !
!                          write(*,*) 'cmbrs', cmbrs(jac,jb)           !
!                          write(*,*) 'procent error', dlmr*100.0      !
!                       end if                                         !


                  end if
                end do
              end do
            end if

           
!</debug>


      do i=1,imc
        do k=1,kmc
!        ssssgw(k,i)   = 0.0d0
!        colssgw(k,i)  = 0.0d0
!        ldomssgw(k,i) = 0.0d0
!        rdomssgw(k,i) = 0.0d0
!        algssgw(k,i)  = 0.0d0
!        lpomssgw(k,i) = 0.0d0
!        po4ssgw(k,i)  = 0.0d0
!        nh4ssgw(k,i)  = 0.0d0
!        no3ssgw(k,i)  = 0.0d0
!        dossgw(k,i)   = 0.0d0
!        ticssgw(k,i)  = 0.0d0
!        fessgw(k,i)   = 0.0d0
!        cbodssgw(k,i) = 0.0d0
!        trassgw(k,i)  = 0.0d0
!        alssgw(k,i)   = 0.0d0
!        so4ssgw(k,i)  = 0.0d0
!        cassgw(k,i)   = 0.0d0
!        mgssgw(k,i)   = 0.0d0
!        nassgw(k,i)   = 0.0d0
!        kassgw(k,i)   = 0.0d0
!        mnssgw(k,i)   = 0.0d0
!        clssgw(k,i)   = 0.0d0
!        fe3ssgw(k,i)  = 0.0d0
!        mn3ssgw(k,i)  = 0.0d0
!        sidssgw(k,i)   = 0.0d0
!        ch4ssgw(k,i)  = 0.0d0
!        cdssgw(k,i)  = 0.0d0
!        pbssgw(k,i)  = 0.0d0
!        znssgw(k,i)  = 0.0d0
!        arsssgw(k,i)  = 0.0d0

        ssssp(k,i)     = 0.0d0
        colssp(k,i)  = 0.0d0
        ldomssp(k,i) = 0.0d0
        rdomssp(k,i) = 0.0d0
        algssp(k,i)  = 0.0d0
        lpomssp(k,i) = 0.0d0
        po4ssp(k,i)  = 0.0d0
        nh4ssp(k,i)  = 0.0d0
        no3ssp(k,i)  = 0.0d0
        dossp(k,i)     = 0.0d0
        ticssp(k,i)  = 0.0d0
        fessp(k,i)     = 0.0d0
        cbodssp(k,i) = 0.0d0
        trassp(k,i)  = 0.0d0
        alssp(k,i)     = 0.0d0
        so4ssp(k,i)  = 0.0d0
        cassp(k,i)     = 0.0d0
        mgssp(k,i)     = 0.0d0
        nassp(k,i)     = 0.0d0
        kassp(k,i)     = 0.0d0
        mnssp(k,i)     = 0.0d0
        clssp(k,i)     = 0.0d0
        fe3ssp(k,i)  = 0.0d0
        mn3ssp(k,i)  = 0.0d0
        sidssp(k,i)  = 0.0d0
        ch4ssp(k,i)  = 0.0d0
        feoh3ssp(k,i)= 0.0d0
        aloh3ssp(k,i)= 0.0d0
        cdssp(k,i)   = 0.0d0
        pbssp(k,i)   = 0.0d0
        znssp(k,i)   = 0.0d0
        arsssp(k,i)  = 0.0d0
        pyritessp(k,i)= 0.0d0
        po4precipssp(k,i)= 0.0d0
        co3ssp(k,i) = 0.0d0

        algaenosettlingss(k,i)= 0.0d0
        algaewithsettlingss(k,i)= 0.0d0
    end do
   end do
cssk = 0.0d0

    end subroutine 
