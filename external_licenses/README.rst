External Licenses
=================

PITLAKQ depends on a several external libraries. As it also distributed in binary
from that contains all these software components the licenses of them
are included here.

These are the libraries included. Their licenses are included in the directory
of the corresponding name:

* hdf5
* matplotlib 
* mx_date_time
* netcdf4
* numpy
* openpyxl
* python
* wxPython
* xlsxwriter
* yaml
* zodb

