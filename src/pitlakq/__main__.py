"""
Run as main program with `python -m pitlakq`
"""

if __name__ == '__main__':


    from pitlakq.metamodel.running.run_pitlakq import main

    main()